This document will explain some param to launch MyOrchestra for dev

Programs Arguments

[DEBUG] : Show debug trace from the application
[NO_LOGIN] : Skip the login window
    If the twice environments variables {USER_NAME} OR {USER_PSWD} are not define OR empty
        USER_NAME="SaulGoodMan"
        USER_PSWD="NamDoogLaus"
    Else their value will be use for login

Environments Variables

{USER_NAME} : Name of the user
    If DEFINE and NOT NULL it will be auto-write in login window
{USER_PSWD} : Password of the user
    If DEFINE and NOT NULL it will be auto-write in login window

{DB_NAME} : Name of the data base
    If UNDEFINE or NULL the application is launch without DB connection
{DB_PORT} : Port of the data base
    If UNDEFINE or NULL and DB_NAME too, the application is launch without DB connection
        DB_PORT="http://localhost:3306/"
    Else if UNDEFINE or NULL the application will use DEFAULT port
        DB_PORT="http://localhost:3306/"