package fr.orchestra.utils.system;


public interface ConfSys {

    int WINDOWS = 1;
    int LINUX = 0;

    int OS_NUM = System.getProperty("os.name").matches("[wW]indows.*") ? WINDOWS : LINUX;
    char FILE_SEPARATOR = System.getProperty("file.separator").charAt(0);

    String JAVA_VERSION_STRING = System.getProperty("java.version");
    int JAVA_VERSION_NUM = Integer.parseInt(JAVA_VERSION_STRING.substring(0, JAVA_VERSION_STRING.indexOf('.')));
}
