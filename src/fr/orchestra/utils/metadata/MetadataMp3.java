package fr.orchestra.utils.metadata;

import fr.orchestra.plugin.templates.MetadataGetter;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MetadataMp3 implements MetadataGetter {

    private final static String SUFFIX = "xmpDM:";

    private Metadata metadata = new Metadata();

    @Override
    public void initMetadataExtractor(File file) {
        try {
            InputStream input = new FileInputStream(file);
            ContentHandler handler = new DefaultHandler();
            Parser parser = new Mp3Parser();
            ParseContext parseCtx = new ParseContext();
            parser.parse(input, handler, metadata, parseCtx);
            input.close();

        } catch (SAXException | IOException | TikaException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getTitle() {
        return metadata.get("title");
    }

    @Override
    public String getArtist() {
        return metadata.get(SUFFIX + "artist");
    }

    @Override
    public String getGenre() {
        return metadata.get(SUFFIX + "genre");
    }

    @Override
    public String getAlbum() {
        return metadata.get(SUFFIX + "album");
    }

    public String getYear() {
        return metadata.get(SUFFIX + "year");
    }

    public String getReleaseDate() {
        return metadata.get(SUFFIX + "releaseDate");
    }

    public String getDuration() {
        return metadata.get(SUFFIX + "duration");
    }

    public double durationDoubleValue(String duration) {
        return duration == null ? 0.0 : Double.parseDouble(duration);
    }

    @Override
    public double getDoubleValueDurationMilliSec() {
        return durationDoubleValue(getDuration());
    }

    public String getSpecificMetaData(MetadataType metadataType) {
        return metadata.get(metadataType.getType());
    }

    public enum MetadataType {
        TITLE("title", true),
        ARTIST("artist"),
        GENRE("genre"),
        ALBUM("album"),
        YEAR("year"),
        DURATION("duration");

        String type;

        MetadataType(String type) {
            this.type = SUFFIX + type;
        }

        MetadataType(String type, boolean noSuffix) {
            if (noSuffix) this.type = type;
            else this.type = SUFFIX + type;
        }

        public String getType() {
            return type;
        }
    }
}