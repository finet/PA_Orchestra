package fr.orchestra.utils.metadata;

import fr.orchestra.plugin.templates.MetadataGetter;
import fr.orchestra.utils.format.TrackFormat;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class MetadataFactory {

    private final static MetadataFactory INSTANCE = new MetadataFactory();
    private Map<String[], MetadataGetter> mapMetadataReader = new HashMap<>();

    public static MetadataFactory getInstance() {
        return INSTANCE;
    }

    public void addMetadataReader(String[] format, MetadataGetter metadataInterface) {
        mapMetadataReader.put(format, metadataInterface);
    }

    public MetadataGetter getMetadataReader(File file) {

        String regex = TrackFormat.getInstance().getFormatRegex(file);
        if (regex == null) return null;

        for (Map.Entry<String[], MetadataGetter> kv : mapMetadataReader.entrySet()) {
            for (String format : kv.getKey()) {
                if (("test." + format).matches(regex)) return kv.getValue();
            }
        }
        return null;
    }
}
