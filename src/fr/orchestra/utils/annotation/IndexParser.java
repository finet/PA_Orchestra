package fr.orchestra.utils.annotation;

import fr.orchestra.view.IndexResources;
import javafx.scene.image.Image;
import org.opengis.parameter.InvalidParameterNameException;

import javax.management.modelmbean.InvalidTargetObjectTypeException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;

public class IndexParser {

    private Class[] annotationsUsed;

    public IndexParser(Class[] annotations) {
        this.annotationsUsed = annotations;
    }

    public void FindAnnotationField(Class[] classes) throws InvalidTargetObjectTypeException {

        for (Class clazz : classes) {
            Field[] fields = clazz.getDeclaredFields();
            ResourceFile resourceFile;

            for (Field field : fields) {
                Annotation[] annotations = field.getAnnotations();

                for (Annotation annotation : annotations) {

                    for (Class annotationSearch : annotationsUsed) {
                        if (annotationSearch.getTypeName().equals(annotation.annotationType().getName())) {

                            field.setAccessible(true);
                            resourceFile = field.getAnnotation(ResourceFile.class);
                            if (!Modifier.isStatic(field.getModifiers())) {
                                throw new InvalidTargetObjectTypeException("The field" + field.getName()
                                        + " in the class " + clazz.getName()
                                        + " of the package " + clazz.getPackage().getName()
                                        + " must be static");
                            } else {
                                URL tmp = IndexResources.getINSTANCE().getFileResource(resourceFile.value());

                                if (tmp == null) {
                                    throw new InvalidParameterNameException("The field \"" + field.getName() + "\" resources are not found in " + IndexResources.class.getPackage()
                                            + "\n\tFrom " + field.getDeclaringClass().getName(), "");
                                }

                                if (field.getType().getName().equals(URL.class.getName())) {
                                    try {
                                        field.set(null, tmp);
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    }
                                } else if (field.getType().getName().equals(InputStream.class.getName())) {
                                    try {
                                        try {
                                            field.set(null, tmp.openStream());
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    }
                                } else if (field.getType().getName().equals(String.class.getName())) {
                                    try {
                                        field.set(null, tmp.toExternalForm());
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    }
                                } else if (field.getType().getName().equals(Image.class.getName())) {
                                    try {
                                        field.set(null, new Image(tmp.toExternalForm()));
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    throw new InvalidTargetObjectTypeException("The field" + field.getName()
                                            + " in the class " + clazz.getName()
                                            + " of the package " + clazz.getPackage().getName()
                                            + "\nmust be a " + URL.class.getName()
                                            + " or " + InputStream.class.getName()
                                            + " or " + String.class.getName()
                                            + " or " + Image.class.getName());
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
