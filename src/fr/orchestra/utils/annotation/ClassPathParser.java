package fr.orchestra.utils.annotation;

import fr.orchestra.plugin.templates.MetadataGetter;
import fr.orchestra.plugin.templates.TrackPlayer;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarFile;

public class ClassPathParser {

    /*private static String classPath = ClassPathParser.class.getProtectionDomain().getCodeSource().getLocation().getPath();
    private static Package pckg = Package.getPackage(classPath);*/
    private static List<Class> classes = new ArrayList<>();

    public static Class[] getClasses(String pckgName) throws ClassNotFoundException {

        // Get a File object for the package
        File directory = null;
        String path = null;
        URL resource = null;

        try {
            ClassLoader cld = Thread.currentThread().getContextClassLoader();
            if (cld == null) {
                throw new ClassNotFoundException("Can't get class loader.");
            }
            path = pckgName.replace('.', '/');

            resource = cld.getResource(path);
        } catch (NullPointerException x) {
            throw new ClassNotFoundException(pckgName + " (" + directory + ") does not appear to be a valid package");
        }

        if (resource == null) {
            throw new ClassNotFoundException("No resource for " + path);
        }

        if (!resource.toString().startsWith("jar:")) {
            directory = new File(resource.getFile());
            if (!directory.exists()) {
                throw new ClassNotFoundException(pckgName + " does not appear to be a valid package");
            } else findClass(directory, pckgName);
        } else {
            findClassJar(resource, pckgName);
        }


        Class[] classesA = new Class[classes.size()];
        classes.toArray(classesA);
        return classesA;
    }

    private static void findClass(File directory, String pckgname) {
        String[] files = directory.list();

        for (File subFile : Objects.requireNonNull(directory.listFiles())) {

            if (subFile.isFile() && subFile.getName().endsWith(".class")) {
                try {
                    classes.add(Class.forName(pckgname + '.' + subFile.getName().substring(0, subFile.getName().length() - 6)));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (subFile.isDirectory()) {
                findClass(subFile, pckgname + "." + subFile.getName());
            }
        }
    }

    private static void findClassJar(URL resource, String pckgName) {

        JarFile jar = null;
        try {
            jar = new JarFile(resource.getPath().substring(5, resource.getPath().lastIndexOf('!')).replaceAll("\\\\", "/"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert jar != null;
        Enumeration enumeration = jar.entries();
        URLClassLoader loader = new URLClassLoader(new URL[]{resource});

        MetadataGetter metadataGetter = null;
        TrackPlayer trackPlayer = null;


        while (enumeration.hasMoreElements()) {

            String tmp = enumeration.nextElement().toString();
            String pckg = pckgName.replaceAll("\\.", "/");

            if (tmp.matches("^" + pckg + "/.*\\.class")) {

                try {
                    //Class.forName()
                    Class clazz = Class.forName(tmp.replaceAll("[/\\\\]", ".").substring(0, tmp.lastIndexOf('.')), false, loader);
                    classes.add(clazz);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
