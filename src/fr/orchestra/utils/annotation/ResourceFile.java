package fr.orchestra.utils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This Annotation must flag a static variable who will contain a resource.
 *
 * @param value The local path of the resource to be load and
 *              the root is the package where the classIndexResources is located.
 *              The separated char is a '.', example: pictures.logo.
 *              WARNING: Many resources collocated (in the same package)
 *              cannot have the same name even if they have different format type.
 * @return Object who can take 4 type :
 * - java.net.URL
 * - java.io.InputStream
 * - java.lang.String
 * - javafx.scene.Image
 */


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ResourceFile {
    String value();
}
