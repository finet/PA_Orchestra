package fr.orchestra.utils.plugin;


import fr.orchestra.controller.mediaplayer.player.PlayerManager;
import fr.orchestra.plugin.templates.MetadataGetter;
import fr.orchestra.plugin.templates.TrackPlayer;
import fr.orchestra.utils.metadata.MetadataFactory;
import fr.orchestra.utils.properties.ConstProperties;
import javafx.stage.FileChooser;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.InvalidPathException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarFile;


public class PluginLoader implements ConstProperties {

    private static PluginLoader INSTANCE = new PluginLoader();

    public static PluginLoader getInstance() {
        return INSTANCE;
    }

    public static List<URL> retrieveExistingPlugin() {
        File dirPlugin = new File(PLUGINS_DIRECTORY_PATH);

        if (!dirPlugin.mkdirs() && !dirPlugin.exists()) {
            throw new InvalidPathException(PLUGINS_DIRECTORY_PATH, "The plugins directory storage can not be find and created");
        }

        List<URL> urlList = new ArrayList<>();
        for (File pluginFile : Objects.requireNonNull(dirPlugin.listFiles(pathname -> pathname.isFile() && pathname.getName().matches("^.+\\.jar$")))) {
            try {
                urlList.add(pluginFile.toURI().toURL());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return urlList;
    }

    public static boolean loadPlugin(URL plugin) {

        JarFile jar = null;
        try {
            jar = new JarFile(plugin.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert jar != null;
        Enumeration enumeration = jar.entries();
        URLClassLoader loader = new URLClassLoader(new URL[]{plugin});

        MetadataGetter metadataGetter = null;
        TrackPlayer trackPlayer = null;

        while (enumeration.hasMoreElements()) {

            String tmp = enumeration.nextElement().toString();

            if (tmp.matches("^.+\\.class$")) {

                Class classTmp = null;

                tmp = tmp.substring(0, tmp.length() - 6);
                tmp = tmp.replaceAll("[/\\\\]", ".");

                try {
                    classTmp = Class.forName(tmp, true, loader);
                } catch (ClassNotFoundException e) {
                    closeLoader(loader);
                    e.printStackTrace();
                }

                assert classTmp != null;
                if (classTmp.getInterfaces() == null) {
                    closeLoader(loader);
                    return false;
                }
                for (int cnt = 0; cnt < classTmp.getInterfaces().length; cnt++) {

                    if (classTmp.getInterfaces()[cnt].getName().equals(MetadataGetter.class.getName())) {
                        try {
                            metadataGetter = (MetadataGetter) classTmp.newInstance();
                        } catch (InstantiationException | IllegalAccessException e) {
                            closeLoader(loader);
                            e.printStackTrace();
                        }
                    } else if (classTmp.getInterfaces()[cnt].toString().equals(TrackPlayer.class.toString())) {
                        try {
                            trackPlayer = (TrackPlayer) classTmp.newInstance();
                        } catch (InstantiationException | IllegalAccessException e) {
                            closeLoader(loader);
                            e.printStackTrace();
                        }
                    }

                    if (trackPlayer != null && metadataGetter != null) {

                        MetadataFactory.getInstance().addMetadataReader(trackPlayer.getTrackFormat(), metadataGetter);
                        PlayerManager.getInstance().addTrackPlayer(trackPlayer);
                        closeLoader(loader);
                        return true;
                    }
                }
            }
        }
        closeLoader(loader);
        return false;
    }

    private static void closeLoader(URLClassLoader loader) {
        try {
            loader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // @TODO save path of last dir open in the showOpenDialog() --> if the window must be reopen, use this path as reference dir
    public static URL openAndLoadPlugin(List<URL> listPluginAlreadyPresent) {

        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("JAVA Jar File", "*.jar"));

        File jarFile = fc.showOpenDialog(null);
        if (jarFile == null) return null;

        URL jarURL = null;
        try {
            jarURL = jarFile.toURI().toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (jarURL == null || listPluginAlreadyPresent.contains(jarURL)) return null;

        if (loadPlugin(jarURL)) return jarURL;
        else return null;

    }

    public static void storePluginJarFile(URL jar) {

        String fileOutput = PLUGINS_DIRECTORY_PATH + jar.getFile().substring(jar.getFile().lastIndexOf("/"));

        try {
            PrintWriter writer = new PrintWriter(fileOutput);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try (InputStream sourceFile = new FileInputStream(jar.getFile());
             OutputStream destinationFile = new FileOutputStream(fileOutput)) {
            // Lecture par segment de 0.5Mo (512 * 1024)
            byte buffer[] = new byte[524288];
            int nbLecture;
            while ((nbLecture = sourceFile.read(buffer)) != -1) {
                destinationFile.write(buffer, 0, nbLecture);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean removePluginJarFile(URL jar) {

        File file = new File(PLUGINS_DIRECTORY_PATH + jar.getFile().substring(jar.getFile().lastIndexOf("/")));

        return file.delete();
    }

    public static void removePluginJarFile(List<URL> jars) throws InvalidPathException {

        if (jars == null || jars.isEmpty()) return;

        for (URL jar : jars) {
            if (!removePluginJarFile(jar))
                throw new InvalidPathException(jar.getFile(), "When uninstall a plugin with JAR file");
        }
    }
}