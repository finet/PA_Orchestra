package fr.orchestra.utils;

import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.model.Playlist;
import fr.orchestra.model.Track;
import fr.orchestra.plugin.templates.MetadataGetter;
import fr.orchestra.utils.format.TrackFormat;
import fr.orchestra.utils.metadata.MetadataFactory;
import fr.orchestra.utils.system.ConfSys;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static fr.orchestra.model.Playlist.readSerializedPlaylist;

/**
 * Reload all playlist of user in .playlist/nameOfUser/  directory
 */
public class PlaylistDirectory implements ConfSys {

    private List<Track> trackList = new ArrayList<>();
    private String initialPath;
    private Boolean recursivePath;
    private String initialUserDir;
    private int indexOfUserName;
    private Playlist playlist = null;
    private List<Playlist> playlistList = new ArrayList<>();


    public PlaylistDirectory(String path, Boolean subFolder) {
        super();
        this.initialPath = path;
        this.initialUserDir = WindowView.getInstance().getUser().getLogin();
        this.recursivePath = subFolder;

        String[] user = path.split(String.valueOf(FILE_SEPARATOR));
        this.indexOfUserName = user.length - 1;


    }

    /**
     * Method to call for initialize class and start reload of all playlist
     */
    public List<Playlist> load() {
        this.reloadPlaylist(this.initialPath);
        return playlistList;
    }

    /**
     * Reload all playlist on directory .playlist by recursive
     *
     * @param dir
     */
    private void reloadPlaylist(String dir) {

        File file = new File(dir);
        File[] files = file.listFiles();

        /**  get name of user directory**/
        String[] user = dir.split(String.valueOf(FILE_SEPARATOR));
        String currentDir = user[user.length - 1];

        /** if user dir was found **/
        if (user.length - 1 > indexOfUserName)
            currentDir = user[indexOfUserName + 1];

        if (files != null) {

            for (int i = 0; i < files.length; i++) {

                if (!files[i].isDirectory()) {

                    if (FilenameUtils.getExtension(dir + FILE_SEPARATOR + files[i].getName()).equals(""))
                        playlist = readSerializedPlaylist(dir + FILE_SEPARATOR + files[i].getName());
                    else
                        setTrack(dir, files[i].getName());
                }
                if (files[i].isDirectory() && this.recursivePath || i == files.length - 1) {

                    if (playlist != null && currentDir.equals(initialUserDir))
                        setPlaylist();
                }
                this.reloadPlaylist(files[i].getAbsolutePath());
            }
        }
    }


    /**
     * add listTrack in playlist and load them on interface
     */
    private void setPlaylist() {

        playlist.addTrack(trackList);
        playlistList.add(playlist);
        trackList.clear();
        playlist = null;
    }

    /**
     * Set Track object by metadata of file
     *
     * @param dir
     * @param fileName
     */
    private void setTrack(String dir, String fileName) {

        File fileAudio = new File(dir + FILE_SEPARATOR + fileName);

        // First, select the good Metadata extractor (depend of the kind of file format)
        MetadataGetter metadata = MetadataFactory.getInstance().getMetadataReader(fileAudio);
        metadata.initMetadataExtractor(fileAudio);
        trackList.add(new Track(0, metadata.getDoubleValueDurationMilliSec(),
                dir + FILE_SEPARATOR + fileName,
                TrackFormat.getFormat(fileAudio),
                metadata.getTitle(),
                metadata.getArtist(),
                metadata.getAlbum(),
                metadata.getGenre()
        ));
    }
}
