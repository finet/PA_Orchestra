package fr.orchestra.utils.dragboard;

import javafx.scene.Node;
import javafx.scene.input.*;

import java.io.Serializable;


public class DragDrop {

    public static <T extends Serializable, N extends Node> void dragDetected(T data, N nodeDragged) {

        if (data != null) {

            Dragboard db;
            if (nodeDragged != null) db = nodeDragged.startDragAndDrop(TransferMode.COPY);
            else throw new NullPointerException("Node who contain data dragged is null");

            ClipboardContent content = new ClipboardContent();
            DataFormat df = DataFormat.lookupMimeType(data.getClass().getName());
            if (df == null) df = new DataFormat(data.getClass().getName());
            content.put(df, data);
            db.setContent(content);
        } /*else throw new NullPointerException("Data dragged is null");*/
    }

    public static <T extends Serializable> void dropAllow(Class<T> dataType, DragEvent event) {

        Dragboard db = event.getDragboard();
        DataFormat dfT = DataFormat.lookupMimeType(dataType.getName());
        if (dfT == null) dfT = new DataFormat(dataType.getName());
        if (db.hasContent(dfT)) {
            event.acceptTransferModes(TransferMode.COPY);
        }
    }

    public static <T extends Serializable> Serializable getDropData(Class<T> dataType, DragEvent event) {

        Dragboard db = event.getDragboard();
        DataFormat dfT = DataFormat.lookupMimeType(dataType.getName());
        if (dfT == null) dfT = new DataFormat(dataType.getName());

        if (!db.hasContent(dfT)) {
            return null;
        }
        event.setDropCompleted(true);

        return (Serializable) db.getContent(dfT);
    }
}