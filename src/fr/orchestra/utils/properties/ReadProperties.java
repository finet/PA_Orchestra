package fr.orchestra.utils.properties;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


public final class ReadProperties implements ConstProperties {

    private ReadProperties() {
    }

    public static boolean readProperMemorizeLog() {

        InputStream input = null;
        boolean memorizeLog = false;

        //if(!(new File(FILE_PATH).exists())) return memorizeLog;

        try {
            input = new FileInputStream(FILE_PATH);
            prop.load(input);
            memorizeLog = Boolean.parseBoolean(prop.getProperty("MEMORIZE_LOG"));

        } catch (final IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return memorizeLog;
    }

    public static String[] readProperLog() {

        InputStream input = null;
        String[] log = new String[2];

        //if(!(new File(FILE_PATH).exists())) return log;

        try {
            input = new FileInputStream(FILE_PATH);
            prop.load(input);
            log[0] = prop.getProperty("LOGIN");
            log[1] = prop.getProperty("PASSWORD");

        } catch (final IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return log;
    }

    public static double readProperVolumeLevel() {

        InputStream input = null;
        double volume = 0;

        //if(!(new File(FILE_PATH).exists())) return volume;

        try {
            input = new FileInputStream(FILE_PATH);
            prop.load(input);
            volume = Double.parseDouble(prop.getProperty("VOLUME"));
        } catch (final IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return volume;
    }
}
