package fr.orchestra.utils.properties;


import fr.orchestra.OrchestraLauncher;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Class allow persistence for preference and files (playlists, local folder etc)
 */
public final class WriteProperties implements ConstProperties {

    private WriteProperties() {
    }

    /**
     * Initialize ConstProperties if file doesn't exist with "VOLUME" key = 0.0
     */
    public static void initPropers() {

        System.err.println("INSTALL_PATH=" + INSTALL_PATH);
        System.err.println("FILE_PATH=" + FILE_PATH);
        System.err.println("OS=" + System.getProperty("os.name").matches(".*[wW]indows.*"));
        System.err.println("OrchestraLauncher.class.getProtectionDomain().getCodeSource().getLocation().getPath()=" +
                OrchestraLauncher.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        Path path = Paths.get(FILE_PATH);

        try {
            if (Files.notExists(path) || Files.size(path) == 0) {

                OutputStream output = new FileOutputStream(FILE_PATH);
                writeProperVolumeLevel(0.0);
                output.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * This Class allow to remember the user. we write on config.properties login,
     * password and the status of checkbox.
     *
     * @param login
     * @param password
     * @param memorizeLog
     */
    public static void writeProperLog(String login, String password, boolean memorizeLog) {

        OutputStream output = null;
        try {
            output = new FileOutputStream(FILE_PATH);

            prop.setProperty("LOGIN", login);
            prop.setProperty("PASSWORD", password);
            prop.setProperty("MEMORIZE_LOG", String.valueOf(memorizeLog));

            prop.store(output, null);

        } catch (final IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Write on file ConstProperties "VOLUME" value
     *
     * @param volume
     */
    public static void writeProperVolumeLevel(double volume) {

        OutputStream output = null;

        try {
            output = new FileOutputStream(FILE_PATH);
            prop.setProperty("VOLUME", String.valueOf(volume));
            prop.store(output, null);

        } catch (final IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
