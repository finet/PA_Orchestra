package fr.orchestra.utils.properties;

import fr.orchestra.OrchestraLauncher;
import fr.orchestra.utils.system.ConfSys;

import java.util.Properties;

public interface ConstProperties {

    String INSTALL_PATH = OrchestraLauncher.class.getProtectionDomain().getCodeSource().getLocation().getPath();
    String FILE_PATH = INSTALL_PATH.substring(ConfSys.OS_NUM, INSTALL_PATH.lastIndexOf(ConfSys.FILE_SEPARATOR) + 1) + "config.properties";

    Properties prop = new Properties();

    String PLAYLIST_DIRECTORY_PATH = INSTALL_PATH.substring(ConfSys.OS_NUM, INSTALL_PATH.lastIndexOf(ConfSys.FILE_SEPARATOR) + 1) + ".playlists";
    String PLAYLIST_PICTURES_DIRECTORY_PATH = INSTALL_PATH.substring(ConfSys.OS_NUM, INSTALL_PATH.lastIndexOf(ConfSys.FILE_SEPARATOR) + 1) + ".pictures";
    String PLUGINS_DIRECTORY_PATH = INSTALL_PATH.substring(ConfSys.OS_NUM, INSTALL_PATH.lastIndexOf(ConfSys.FILE_SEPARATOR) + 1) + ".plugins";
}
