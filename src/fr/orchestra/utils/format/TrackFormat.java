package fr.orchestra.utils.format;

import java.io.File;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;


public class TrackFormat {

    private final static TrackFormat INSTANCE = new TrackFormat();
    /**
     * Map :
     * - Key : Name of the format
     * - Contain : Regex who math all file with the extension of the Key format
     */
    private List<String> formatsAllow = new ArrayList<>();

    private TrackFormat() {
    }

    public static TrackFormat getInstance() {
        return INSTANCE;
    }

    public static boolean fileFormatMatch(String format, String regexFormat) {

        String test = "test." + format;
        return test.matches(regexFormat);
    }

    public static boolean fileFormatMatch(String format, String[] regexFormats) {

        for (String regexFormat : regexFormats) {
            if (fileFormatMatch(format, regexFormat)) return true;
        }
        return false;
    }

    public static String getFormat(File file) {

        if (file.isFile()) {
            String[] tab = file.getName().split("\\.");

            if (tab.length != 0) return tab[tab.length - 1];
            else return "";
        } else return null;
    }

    public boolean fileAllow(File trackFile) {

        for (String format : formatsAllow) {

            if (trackFile.getName().matches(format)) {
                return true;
            }
        }
        return false;
    }

    public List<String> storeFormatsRegex(String[] formatsName) {

        if (formatsName == null) throw new NullPointerException("formatName is NULL");

        List<String> listFormatsRegex = new ArrayList<>();

        for (String format : formatsName) {

            if (format.isEmpty()) throw new InvalidParameterException("Format name's cannot be empty");
            if (listFormatsRegex.contains(format)) break;

            StringBuilder regexFormatFiles = new StringBuilder("^.+\\.");

            for (char c : format.toCharArray()) {

                char cM = 0;

                if (c >= 65 && c <= 90) cM = (char) ((int) c + 32);
                else if (c >= 97 && c <= 122) cM = (char) ((int) c - 32);

                if (cM != 0) {
                    regexFormatFiles.append("[").append(String.valueOf(c)).append(String.valueOf(cM)).append("]");
                } else regexFormatFiles.append(c);
            }
            regexFormatFiles.append('$');


            formatsAllow.add(regexFormatFiles.toString());
            listFormatsRegex.add(regexFormatFiles.toString());
        }

        return listFormatsRegex;
    }

    public void removeFormatsRegex(String[] formatsName) {

        if (formatsName == null) throw new NullPointerException("formatName is NULL");

        for (String format : formatsName) {
            formatsAllow.remove(format);
        }
    }

    public String getFormatRegex(File file) {

        for (String regex : formatsAllow) {

            if (file.getName().matches(regex)) return regex;
        }
        return null;
    }
}
