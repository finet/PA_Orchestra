package fr.orchestra;


import fr.orchestra.api.UserServices;
import fr.orchestra.controller.folderview.ViewController;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.login.WindowLog;
import fr.orchestra.utils.annotation.ClassPathParser;
import fr.orchestra.utils.annotation.IndexParser;
import fr.orchestra.utils.annotation.ResourceFile;
import fr.orchestra.utils.plugin.PluginLoader;
import fr.orchestra.utils.properties.WriteProperties;
import fr.orchestra.utils.system.ConfSys;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.management.InstanceAlreadyExistsException;
import javax.management.modelmbean.InvalidTargetObjectTypeException;
import java.net.URL;

import static fr.orchestra.utils.properties.WriteProperties.initPropers;


public class OrchestraLauncher extends Application implements ConfSys {

    private static final String mainPckgName = OrchestraLauncher.class.getPackageName();
    @ResourceFile("fxml.login")
    private static URL loginFXML;
    @ResourceFile("css.login")
    private static String loginCSS;
    @ResourceFile("pictures.logo")
    private static Image logoPictures;

    public static void main(String[] args) throws InstanceAlreadyExistsException {
        checkJavaVersion();
        UserServices.createInstance(UserServices.Mode.ONLINE);
        launch(args);
    }

    protected static void checkJavaVersion() {

        if (JAVA_VERSION_NUM < 10) {
            System.err.println("Java Runtime Environment (" + JAVA_VERSION_STRING + ") not supported [JRE == 10]");
            System.exit(0);
        } else if (JAVA_VERSION_NUM > 10) {
            System.err.println("Warning : JRE-" + JAVA_VERSION_STRING + " can be not supported or bad issues");
        }
    }

    @Override
    public void init() throws Exception {

        initPropers();
        launchAnnotationsParsers();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(loginFXML);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);

        scene.getStylesheets().add(loginCSS);
        primaryStage.getIcons().add(logoPictures);

        primaryStage.setTitle("MyOrchestra");

        WindowLog.getInstance().setScene(scene);
        WindowLog.getInstance().setRoot(root);
        WindowLog.getInstance().setPrimaryStage(primaryStage);

        primaryStage.show();
    }

    /**
     * Post javaFX's processor
     */
    @Override
    public void stop() throws Exception {

        WindowView wdv = WindowView.getInstance();
        assert wdv != null;

        ViewController vcl = wdv.getController();
        if (vcl != null) {
            WriteProperties.writeProperVolumeLevel(vcl.getPlayerWindow().getController().getVolumeLevel());
            PluginLoader.removePluginJarFile(vcl.getJarRemoveFiles());
        }
    }

    private void launchAnnotationsParsers() {

        Class[] annotationUsed = {ResourceFile.class};
        IndexParser index = new IndexParser(annotationUsed);
        Class[] cls = null;

        try {
            cls = ClassPathParser.getClasses(mainPckgName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            index.FindAnnotationField(cls);
        } catch (InvalidTargetObjectTypeException itote) {
            itote.printStackTrace();
        }
    }
}