package fr.orchestra.controller.folderview;


import fr.orchestra.controller.folderview.playlist.list.view.PlaylistList;
import fr.orchestra.controller.folderview.tree.folder.ItemEntity;
import fr.orchestra.controller.folderview.view.middle.template.AbstractMusicsList;
import fr.orchestra.controller.mediaplayer.PlayerWindow;
import fr.orchestra.model.Track;
import fr.orchestra.model.User;
import fr.orchestra.plugin.templates.MetadataGetter;
import fr.orchestra.utils.annotation.ResourceFile;
import fr.orchestra.utils.format.TrackFormat;
import fr.orchestra.utils.metadata.MetadataFactory;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * SINGLETON Class
 */
public class WindowView {

    /**
     * Instance unique pré-initialisée
     */
    private static WindowView INSTANCE = new WindowView();

    @ResourceFile(value = "fxml.folderview.folderView")
    private static URL folderViewFXML;
    @ResourceFile(value = "pictures.logo")
    private static InputStream logoPictures;
    @ResourceFile(value = "css.folderView")
    private static String folderViewCSS;

    private User user;
    private PlayerWindow playerWindow;
    private boolean winStatus = false;
    private Stage stage;
    private Parent childRoot;
    private Scene scene;
    private ViewController controller;
    private TreeViewFolder treeViewFolder;
    private AbstractMusicsList middleViewContentList;
    private PlaylistList playlistList;

    private WindowView() {
    }

    /**
     * Point d'accès pour l'instance unique du singleton
     */
    public static WindowView getInstance() {
        return INSTANCE;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Scene getScene() {
        return scene;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public ViewController getController() {
        return controller;
    }

    public TreeViewFolder getTreeViewFolder() {
        return treeViewFolder;
    }

    public void setTreeViewFolder(TreeViewFolder treeViewFolder) {
        this.treeViewFolder = treeViewFolder;
    }

    public AbstractMusicsList getMiddleViewContentList() {
        return middleViewContentList;
    }

    public void setMiddleViewContentList(AbstractMusicsList middleViewContentList) {
        this.middleViewContentList = middleViewContentList;
    }

    public void setWinStatus(boolean status) {
        this.winStatus = status;
    }

    public PlaylistList getPlaylistList() {
        return playlistList;
    }

    public void setPlaylistList(PlaylistList playlistList) {
        this.playlistList = playlistList;
    }

    public void LoadWindowView(PlayerWindow playerWindow) {

        this.playerWindow = playerWindow;

        try {

            final FXMLLoader fxmlLoader = new FXMLLoader(folderViewFXML);
            childRoot = fxmlLoader.load();
            controller = fxmlLoader.getController();

        } catch (IOException e) {
            e.printStackTrace();
        }

        scene = new Scene(childRoot);
        stage = new Stage();
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);

        controller.setStage(stage);
        controller.setWindowView(this);
        controller.setPlayerWindow(playerWindow);

        stage.getIcons().add(new Image(logoPictures));

        scene.getStylesheets().add(folderViewCSS);

        playerWindow.getController().setStageFolder(stage);
        openWindow();
    }

    public void openWindow() {

        if (winStatus) {
            stage.hide();
            playerWindow.setWinStatus(true);
            winStatus = false;
        } else {
            stage.show();
            playerWindow.setWinStatus(false);
            winStatus = true;
        }
    }

    /**
     * Exit app
     */
    public void exit() {
        Platform.exit();
    }


    /**
     * Get List<Track> from a List<ItemEntity.Music>
     */
    public List<Track> getListTrack(List<ItemEntity.Music> musicList) {

        int id = 0;
        List<Track> trackList = new ArrayList<>();

        for (ItemEntity.Music aMusicList : musicList) {

            MetadataGetter metadata = MetadataFactory.getInstance().getMetadataReader(aMusicList.getFile());
            metadata.initMetadataExtractor(aMusicList.getFile());
            trackList.add(
                    new Track(id, metadata.getDoubleValueDurationMilliSec(),
                            aMusicList.getFile().getPath(),
                            TrackFormat.getFormat(aMusicList.getFile()),
                            metadata.getTitle(),
                            metadata.getArtist(),
                            metadata.getAlbum(),
                            metadata.getGenre()
                    ));
            id++;
        }
        return trackList;
    }
}
