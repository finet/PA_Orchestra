package fr.orchestra.controller.folderview;

import fr.orchestra.controller.folderview.tree.folder.ItemEntity;
import fr.orchestra.controller.folderview.tree.folder.TreeCellEntity;
import fr.orchestra.utils.format.TrackFormat;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

import java.io.File;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;


public class TreeViewFolder {

    private static final TreeItem<ItemEntity> rootItem = new TreeItem<>();
    {
        ItemEntity item = new ItemEntity();
        item.setDisplayName("ROOT");
        rootItem.setValue(item);
    }

    private ItemEntity.Directory rootDir = null;
    private ItemEntity.Directory lastDir;
    private TreeItem<ItemEntity> lastTreeItem;


    public TreeViewFolder(TreeView<ItemEntity> treeView, File pathRootDir) {

        treeView.setRoot(rootItem);

        // If root is displayed, the CellFactory will not be able to apply and an exception will be raise
        treeView.setShowRoot(false);

        if (!rootItem.getChildren().isEmpty()) rootItem.getChildren().remove(0);

        try {
            rootDir = new ItemEntity.Directory(pathRootDir, null);
        } catch (InvalidPropertiesFormatException e) {
            e.printStackTrace();
        }
        lastDir = rootDir;
        lastTreeItem = new TreeItem<>(lastDir);

        if (!rootItem.getChildren().isEmpty()) rootItem.getChildren().set(0, lastTreeItem);
        else rootItem.getChildren().add(lastTreeItem);

        generateTreeFolder(removeBannedFormatsFiles(pathRootDir.listFiles()));

        treeView.setCellFactory(TreeCellEntity.forTreeView());
    }

    private static List<File> removeBannedFormatsFiles(File[] files) throws NullPointerException {

        if (files == null) throw new NullPointerException("File[] is null");
        List<File> newFiles = new ArrayList<>();

        for (int cnt = 0; cnt < files.length; cnt++) {

            if (files[cnt].isDirectory() || TrackFormat.getInstance().fileAllow(files[cnt])) {
                newFiles.add(files[cnt]);
            }
        }
        return newFiles;
    }

    public ItemEntity.Directory getRootDir() {
        return rootDir;
    }

    public ItemEntity.Directory getLastDir() {
        return this.lastDir;
    }

    private void generateTreeFolder(List<File> files) {


        for (File file : files) {

            if (file.isDirectory()) {

                ItemEntity.Directory entityDir = null;

                try {
                    entityDir = new ItemEntity.Directory(file, lastDir);
                    entityDir.setDisplayName(file.getName());

                } catch (InvalidPropertiesFormatException e) {
                    e.printStackTrace();
                }

                lastDir.getChildrenDir().add(entityDir);
                lastDir = entityDir;


                List<File> list = removeBannedFormatsFiles(file.listFiles());

                if (lastTreeItem != null) {
                    addTreeItem(entityDir);
                }

                if (!list.isEmpty()) generateTreeFolder(list);

                lastDir = lastDir.getParent();
                lastTreeItem = lastTreeItem.getParent();
            }
            else {
                try {
                    lastDir.getChildrenMusic().add(new ItemEntity.Music(file, lastDir));
                } catch (InvalidPropertiesFormatException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void addTreeItem(ItemEntity entity) {

        if (entity != this.rootDir) {

            TreeItem<ItemEntity> tie = new TreeItem<>(entity);
            lastTreeItem.getChildren().add(tie);
            lastTreeItem = tie;
        }
    }
}
