package fr.orchestra.controller.folderview;

import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;


public class ToolBarMenu {

    private ViewController controller;
    private double xOffset = 0;
    private double yOffset = 0;


    public ToolBarMenu(ViewController viewController, Button exitButton, Button reduceButton, Button maximizeButton) {

        this.controller = viewController;

        exitButton.setOnAction(event -> {
            WindowView.getInstance().exit();
        });

        reduceButton.setCancelButton(true);
        reduceButton.setOnAction(event -> controller.getStage().setIconified(true));

        maximizeButton.setOnAction(event -> {

            if (!controller.getStage().isMaximized())
                controller.getStage().setMaximized(true);
            else
                controller.getStage().setMaximized(false);
        });
    }

    public void setMovableWindowBy(AnchorPane anchorPane) {
        anchorPane.setOnMousePressed(this::getPosWindow);
        anchorPane.setOnMouseDragged(this::setPosWindow);
    }

    private void getPosWindow(MouseEvent mouseEvent) {
        xOffset = controller.getStage().getX() - mouseEvent.getScreenX();
        yOffset = controller.getStage().getY() - mouseEvent.getScreenY();
    }

    private void setPosWindow(MouseEvent mouseEvent) {
        controller.getStage().setX(mouseEvent.getScreenX() + xOffset);
        controller.getStage().setY(mouseEvent.getScreenY() + yOffset);
    }
}
