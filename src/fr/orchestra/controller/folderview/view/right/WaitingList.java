package fr.orchestra.controller.folderview.view.right;


import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.folderview.view.AddTrackTable;
import fr.orchestra.controller.mediaplayer.player.PlayerManager;
import fr.orchestra.model.Track;
import fr.orchestra.utils.Utils;
import fr.orchestra.utils.annotation.ResourceFile;
import fr.orchestra.utils.dragboard.DragDrop;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import javax.management.InstanceAlreadyExistsException;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class WaitingList implements AddTrackTable {

    @ResourceFile("fxml.folderview.deleteButton")
    private static URL deleteButtonFxml;
    private static WaitingList INSTANCE = null;
    private Label trackNumber;
    private Label globalDuration;
    private TableView<Track> tableTracks;
    private WindowView windowView = WindowView.getInstance();

    private WaitingList() {
    }

    private WaitingList(Label musicsNumber, Label globalDuration, TableView<Track> tableTracks,
                        TableColumn<Track, String> columnName, TableColumn<Track, String> columnArtist,
                        TableColumn<Track, String> columnAlbum, TableColumn<Track, String> columnGenre,
                        TableColumn<Track, String> columnDuration, TableColumn<Track, Button> columnDelete) {

        this.trackNumber = musicsNumber;
        this.trackNumber.setText("0");
        this.globalDuration = globalDuration;

        this.tableTracks = tableTracks;

        columnName.setCellValueFactory((TableColumn.CellDataFeatures<Track, String> track) -> new SimpleStringProperty(track.getValue().getName()));
        columnArtist.setCellValueFactory((TableColumn.CellDataFeatures<Track, String> track) -> new SimpleStringProperty(track.getValue().getArtist()));
        columnGenre.setCellValueFactory((TableColumn.CellDataFeatures<Track, String> track) -> new SimpleStringProperty(track.getValue().getGenre()));
        columnAlbum.setCellValueFactory((TableColumn.CellDataFeatures<Track, String> track) -> new SimpleStringProperty(track.getValue().getAlbum()));
        columnDuration.setCellValueFactory((TableColumn.CellDataFeatures<Track, String> track) -> {

            Date time = new Date((long) track.getValue().getTotalTime());
            return new SimpleStringProperty(new SimpleDateFormat(MUSIC_TIME_TEMPLATE).format(time));
        });
        columnDelete.setSortable(false);
        columnDelete.setCellValueFactory(track -> {

            final FXMLLoader fxmlLoader = new FXMLLoader(deleteButtonFxml);

            Button button = null;
            try {
                button = fxmlLoader.load();
                button.setOnMousePressed(event -> {
                    int tmp = tableTracks.getSelectionModel().getSelectedIndex();
                    if (tmp >= 0 && tmp < tableTracks.getItems().size()) deleteTrackForTable(tmp);
                });

            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

            return new ReadOnlyObjectWrapper<>(button);
        });
        this.tableTracks.addEventHandler(MouseEvent.MOUSE_PRESSED, handler -> {

            if (handler.getButton().equals(MouseButton.PRIMARY)) {

                if (handler.getClickCount() >= 2 && tableTracks.getSelectionModel().getSelectedItem() != null) {

                    PlayerManager pm = PlayerManager.getInstance();

                    pm.getPlayerQueue().clear();
                    pm.getPlayerQueue().addAll(tableTracks.getItems());
                    pm.setTrackNumber(tableTracks.getSelectionModel().getSelectedItem().getTrackId());
                    pm.setTrackNumber(tableTracks.getFocusModel().getFocusedCell().getRow());

                    pm.playByRepertoryClick();
                }
            }
        });

        this.tableTracks.addEventHandler(MouseEvent.DRAG_DETECTED, handler -> {
            Track trackSelected = tableTracks.getSelectionModel().getSelectedItem();
            DragDrop.dragDetected(trackSelected, tableTracks);
        });

        this.tableTracks.addEventHandler(DragEvent.DRAG_OVER, handler -> {
            DragDrop.dropAllow(Track.class, handler);
            DragDrop.dropAllow(Track[].class, handler);
        });

        this.tableTracks.addEventHandler(DragEvent.DRAG_DROPPED, handler -> {

            Track trackDropped = (Track) DragDrop.getDropData(Track.class, handler);
            if (trackDropped != null) addTrackForTable(trackDropped);
            else {
                Track[] tracksDropped = (Track[]) DragDrop.getDropData(Track[].class, handler);
                if (tracksDropped != null && tracksDropped.length != 0)
                    for (Track track : tracksDropped) addTrackForTable(track);
                else {

                    handler.setDropCompleted(false);
                    return;
                }
            }

            handler.setDropCompleted(true);
        });
    }

    public static WaitingList getInstance() {
        return INSTANCE;
    }

    public static void createInstance(Label musicsNumber, Label globalDuration, TableView<Track> table,
                                      TableColumn<Track, String> title, TableColumn<Track, String> artiste,
                                      TableColumn<Track, String> album, TableColumn<Track, String> genre,
                                      TableColumn<Track, String> duration, TableColumn<Track, Button> delete)
            throws InstanceAlreadyExistsException {

        if (INSTANCE == null)
            INSTANCE = new WaitingList(musicsNumber, globalDuration, table, title, artiste, album, genre, duration, delete);
        else throw new InstanceAlreadyExistsException("The instance of WaitingList has already been create");
    }

    public void setTrack(Track listTrack) {
        tableTracks.getItems().add(listTrack);
    }

    public List<Track> getListTrack() {
        return tableTracks.getItems();
    }

    public void setListTrack(List<Track> listTrack) {
        tableTracks.getItems().addAll(listTrack);
    }

    public void removeINSTANCE() {
        INSTANCE = null;
    }

    @Override
    public void addTrackForTable(Track track) {

        setTrack(track);
        int tmp = Integer.parseInt(trackNumber.getText());
        trackNumber.setText(String.valueOf(tmp + 1));

        globalDuration.setText(Utils.sumTwoTime(globalDuration.getText(), (long) track.getTotalTime(), GLOBAL_TIME_TEMPLATE));
    }

    @Override
    public void deleteTrackForTable(int trackPos) {
        Track trackDelete = tableTracks.getItems().get(trackPos);
        tableTracks.getItems().remove(trackPos);

        int tmp = Integer.parseInt(trackNumber.getText());
        trackNumber.setText(String.valueOf(tmp - 1));

        globalDuration.setText(Utils.deductTime(globalDuration.getText(), (long) trackDelete.getTotalTime(), GLOBAL_TIME_TEMPLATE));

    }
}
