package fr.orchestra.controller.folderview.view;

import fr.orchestra.model.Track;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public interface AddTrackTable {
    String MUSIC_TIME_TEMPLATE = "mm:ss";
    String GLOBAL_TIME_TEMPLATE = "HH:mm:ss";

    void addTrackForTable(Track track);

    void deleteTrackForTable(int trackPos) throws IOException, ParseException, JSONException;
}
