package fr.orchestra.controller.folderview.view.middle;

import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.folderview.tree.folder.ItemEntity;
import fr.orchestra.controller.folderview.view.middle.template.AbstractMusicsList;
import fr.orchestra.model.Playlist;
import fr.orchestra.model.Track;
import fr.orchestra.utils.annotation.ResourceFile;

import java.net.URL;
import java.util.List;

public class MusicsResearchList extends AbstractMusicsList {

    @ResourceFile(value = "fxml.folderview.researchView")
    private static URL fxmlURL;
    private List<ItemEntity.Directory> directories;
    private List<Playlist> playlists;

    /**
     * Description : This constructor must show the result of a research between the music library of the user
     * Location : Middle pane of the folderview
     *
     * @param tracks      List of musics to be display
     * @param directories List of directories to be display
     * @param playlists   List of playlist to be display
     */
    public MusicsResearchList(String nameObjectSearch, Track[] tracks, List<ItemEntity.Directory> directories, List<Playlist> playlists) {
        super("Résultats pour la recherche \"" + nameObjectSearch + "\"", tracks);

        this.directories = directories;
        this.playlists = playlists;

        WindowView.getInstance().setMiddleViewContentList(this);
    }

    public List<ItemEntity.Directory> getDirectories() {
        return directories;
    }

    public List<Playlist> getPlaylists() {
        return playlists;
    }

    @Override
    protected URL getFxmlUrl() {
        return fxmlURL;
    }
}
