package fr.orchestra.controller.folderview.view.middle;

import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.folderview.view.middle.template.AbstractMusicsList;
import fr.orchestra.model.Playlist;
import fr.orchestra.utils.annotation.ResourceFile;

import java.net.URL;


public class MusicsPlaylistList extends AbstractMusicsList {

    @ResourceFile("fxml.folderview.playlistView")
    private static URL playlistView;
    private Playlist playlist;


    /**
     * Description : This constructor must show a playlist in the middle pane
     * Location : Middle pane of the folderview
     *
     * @param playlist Playlist to be print
     */

    public MusicsPlaylistList(Playlist playlist) {
        super(playlist.getPlaylistName(), playlist.getTrackList());

        this.playlist = playlist;
        setImageParent(playlist.getImagePlaylist());
        WindowView.getInstance().setMiddleViewContentList(this);
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    @Override
    protected URL getFxmlUrl() {
        return playlistView;
    }
}
