package fr.orchestra.controller.folderview.view.middle;

import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.folderview.tree.folder.ItemEntity;
import fr.orchestra.controller.folderview.view.middle.template.AbstractMusicsList;
import fr.orchestra.model.Track;
import fr.orchestra.utils.annotation.ResourceFile;

import java.net.URL;
import java.util.List;

public class MusicsDirectoryList extends AbstractMusicsList {

    @ResourceFile("fxml.folderview.repertoryView")
    private static URL fxmlURL;
    private List<ItemEntity.Directory> directories;

    /**
     * Description : This constructor must show an middle of sub-files located in a directory
     * when press on a item in the TreeViewFolder
     * Location : Middle pane of the folderview
     *
     * @param dirName     name of the directory
     * @param tracks      List of musics to be display
     * @param directories List of directories to be display
     */
    public MusicsDirectoryList(String dirName, Track[] tracks, List<ItemEntity.Directory> directories) {
        super(dirName, tracks);

        this.directories = directories;
        WindowView.getInstance().setMiddleViewContentList(this);
    }

    public List<ItemEntity.Directory> getDirectories() {
        return directories;
    }

    @Override
    protected URL getFxmlUrl() {
        return fxmlURL;
    }
}
