package fr.orchestra.controller.folderview.view.middle;


import fr.orchestra.api.UserServices;
import fr.orchestra.api.interfaces.PlaylistOperations;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.folderview.view.AddTrackTable;
import fr.orchestra.controller.folderview.view.middle.template.AbstractViewController;
import fr.orchestra.model.Playlist;
import fr.orchestra.model.Track;
import fr.orchestra.utils.Utils;
import fr.orchestra.utils.annotation.ResourceFile;
import fr.orchestra.utils.dragboard.DragDrop;
import fr.orchestra.utils.properties.ConstProperties;
import fr.orchestra.utils.system.ConfSys;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import static fr.orchestra.utils.Utils.deleteFile;


public class PlaylistViewController extends AbstractViewController implements AddTrackTable, ConstProperties, ConfSys {

    private WindowView wdv = WindowView.getInstance();
    private MusicsPlaylistList mp;

    @ResourceFile("fxml.folderview.deleteButton")
    private static URL deleteButtonFxml;

    @FXML
    protected Label ownerName;
    @FXML
    protected Label labelVisibility;
    @FXML
    protected Pane paneOption;
    @FXML
    protected MenuItem publicVisibility;
    @FXML
    protected MenuItem privateVisibility;
    @FXML
    private ImageView dirImage;
    @FXML
    private Label musicNumber;
    @FXML
    private Label totalMusicDuration;
    @FXML
    private TableColumn<Track, Button> columnDelete;
    @FXML
    private TextField passwordInput;

    @FXML
    public void buttonSave(MouseEvent event) throws JSONException {

        String visibility = (labelVisibility.getText().equals("Publique")) ? "0" : "1";

        JSONObject jsonUpdateVisibilityPlaylist = UserServices.getPlaylistOps().updateVisibilityPlaylist(mp.getPlaylist().getPlaylistName(), visibility, wdv.getUser());

        if (jsonUpdateVisibilityPlaylist.get("error").equals("false")) {
            wdv.getController().setMessageUpload(true);
            wdv.getController().setMessage("Visibilitée à jour");
        }
        else {
            wdv.getController().setMessageUpload(false);
            wdv.getController().setMessage(jsonUpdateVisibilityPlaylist.get("message").toString());
        }

        if (passwordInput.getText().length() > 0) UserServices.getPlaylistOps().updatePasswordPlaylist(mp.getPlaylist().getPlaylistName(), passwordInput.getText(), wdv.getUser());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        columnDelete.setSortable(false);
        columnDelete.setCellValueFactory((track -> {

            final FXMLLoader fxmlLoader = new FXMLLoader(deleteButtonFxml);
            Button button = null;
            try {
                button = fxmlLoader.load();

                button.addEventHandler(MouseEvent.MOUSE_PRESSED, handler -> {
                    int tmp = tableTracks.getSelectionModel().getSelectedIndex();
                    if (tmp >= 0 && tmp < tableTracks.getItems().size()) try {
                        deleteTrackForTable(tmp);
                    } catch (IOException | ParseException | JSONException e) {
                        e.printStackTrace();
                    }
                });
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

            return new ReadOnlyObjectWrapper<>(button);
        }));

        mp = (MusicsPlaylistList) aml;
        Playlist playlist = mp.getPlaylist();

        musicNumber.setText(String.valueOf(trackList.size()));

        double ttDuration = 0.0;
        for (Track track : trackList) {
            ttDuration += track.getTotalTime();
        }

        if ((long) ttDuration > 0) totalMusicDuration.setText(new SimpleDateFormat(GLOBAL_TIME_TEMPLATE).format(new Date((long) ttDuration)));

        dirImage.setImage(mp.getImageParent());
        ownerName.setText(playlist.getUserPseudo());

        tableTracks.addEventHandler(DragEvent.DRAG_OVER, handler -> DragDrop.dropAllow(Track.class, handler));

        tableTracks.addEventHandler(DragEvent.DRAG_DROPPED, handler -> {
            addTrackForTable((Track) DragDrop.getDropData(Track.class, handler));
            handler.setDropCompleted(true);
        });

        JSONObject jsonAuthorization;

        try {
            jsonAuthorization = UserServices.getPlaylistOps().checkAuthorizationForPlaylist(playlist.getPlaylistName(), wdv.getUser());

            if (jsonAuthorization.get("error").toString().equals("true")) paneOption.setVisible(false);
            else {
                JSONObject jsonPlaylist = (JSONObject) jsonAuthorization.get("playlist");

                if (jsonPlaylist.get("visibility").toString().equals("0")) {
                    labelVisibility.setText("Privée");
                    passwordInput.setVisible(false);
                } else {
                    labelVisibility.setText("Publique");
                    passwordInput.setVisible(true);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        publicVisibility.setOnAction(event -> {
            labelVisibility.setText("Publique");
            passwordInput.setVisible(true);
        });
        privateVisibility.setOnAction(event -> {
            labelVisibility.setText("Privée");
            passwordInput.setVisible(false);
        });
    }

    @Override
    public void addTrackForTable(Track track) {

        tableTracks.getItems().add(track);
        trackList.add(track);
        int tmp = Integer.parseInt(musicNumber.getText());
        musicNumber.setText(String.valueOf(tmp + 1));

        totalMusicDuration.setText(Utils.sumTwoTime(totalMusicDuration.getText(), (long) track.getTotalTime(), GLOBAL_TIME_TEMPLATE));
    }

    @Override
    public void deleteTrackForTable(int trackPos) throws IOException, ParseException, JSONException {
        PlaylistOperations plOps = UserServices.getPlaylistOps();

        if (!plOps.checkAuthorizationForPlaylist(mp.getPlaylist().getPlaylistName(), wdv.getUser()).get("message").toString().equals("Vous ne disposez pas des droits nécessairent")) {

            String fileFolder = tableTracks.getItems().get(trackPos).getFileFolder();
            String trackName = tableTracks.getItems().get(trackPos).getName();
            String ext = fileFolder.substring(fileFolder.lastIndexOf('.') + 1, fileFolder.length());
            String src_tracks = tableTracks.getItems().get(trackPos).getArtist() + "_" + trackName + "." + ext;
            src_tracks = src_tracks.replaceAll(" ", "_");

            JSONObject track = (JSONObject) UserServices.getTrackOps().getTrackBySrcTracks(src_tracks).get("track");
            int idTrack = Integer.parseInt(track.get("id").toString());

            JSONObject playlist = (JSONObject) plOps.getPlaylistByName(mp.getPlaylist().getPlaylistName()).get("playlist");
            int idPlaylist = Integer.parseInt(playlist.get("id").toString());

            plOps.deleteLinkBetweenPlaylistAndTrack(idPlaylist, idTrack, wdv.getUser());
        }

        Track trackDelete = tableTracks.getItems().get(trackPos);
        tableTracks.getItems().remove(trackPos);
        trackList.remove(trackPos);
        int tmp = Integer.parseInt(musicNumber.getText());
        musicNumber.setText(String.valueOf(tmp - 1));

        totalMusicDuration.setText(Utils.deductTime(totalMusicDuration.getText(), (long) trackDelete.getTotalTime(), GLOBAL_TIME_TEMPLATE));

        /* delete track of directory playlist */
        String audioFilePath = PLAYLIST_DIRECTORY_PATH + FILE_SEPARATOR +
                wdv.getUser().getLogin() + FILE_SEPARATOR +
                mp.getPlaylist().getPlaylistName() + FILE_SEPARATOR +
                trackDelete.getName() + "." + trackDelete.getFileFormat();

        deleteFile(audioFilePath);
    }
}