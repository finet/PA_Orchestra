package fr.orchestra.controller.folderview.view.middle;

import org.json.JSONObject;
import fr.orchestra.api.UserServices;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.folderview.playlist.list.view.CheckPasswordWindow;
import fr.orchestra.controller.folderview.view.middle.template.AbstractViewController;
import fr.orchestra.model.Playlist;
import fr.orchestra.model.Track;
import fr.orchestra.utils.system.ConfSys;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.management.InstanceAlreadyExistsException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static fr.orchestra.controller.folderview.playlist.newplaylist.window.NewPlaylistController.savePlaylistToDirectory;
import static fr.orchestra.utils.properties.ConstProperties.PLAYLIST_DIRECTORY_PATH;
import static fr.orchestra.utils.properties.ConstProperties.PLAYLIST_PICTURES_DIRECTORY_PATH;


public class ResearchViewController extends AbstractViewController implements ConfSys {

    private WindowView wdv = WindowView.getInstance();

    @FXML
    private Label musicNumber;
    @FXML
    private ListView<Playlist> listViewPlaylist;

    public static void saveToLocalTrack(List<Track> tracks, Playlist playlist) throws IOException {
        if (tracks.size() != 0) {
            for (Track track : tracks) {
                byte[] trackDownload = UserServices.getTrackOps().download(track);

                FileOutputStream sortie = new FileOutputStream(PLAYLIST_DIRECTORY_PATH +
                        FILE_SEPARATOR + WindowView.getInstance().getUser().getLogin() +
                        FILE_SEPARATOR + playlist.getPlaylistName() +
                        FILE_SEPARATOR + track.getName() +
                        "." + track.getFileFormat());
                sortie.write(trackDownload);
                sortie.close();
            }
        }
    }

    public static void saveToLocalImage(Playlist playlist) throws IOException {
        if (playlist != null) {
            byte[] trackDownload = UserServices.getPlaylistOps().download(playlist);

            FileOutputStream sortie = new FileOutputStream(playlist.getImagePlaylistUrl().substring(5));

            sortie.write(trackDownload);
            sortie.close();
        }
    }

    public static List<Track> JsonObjectListTrack(JSONObject jsonObject, String playlistName) throws JSONException, ParseException {
        JSONParser parser = new JSONParser();
        JSONArray jsonArray = (JSONArray) parser.parse(jsonObject.get("track").toString());
        List<Track> listTrack = new ArrayList<>();

        int cpt;
        for (cpt = 0; jsonArray.size() > cpt; cpt++) {
            JSONObject jsonObjectTrack = (JSONObject) jsonArray.get(cpt);

            Track track = JsonObjectTrack(jsonObjectTrack);

            String playlistPath = PLAYLIST_DIRECTORY_PATH + FILE_SEPARATOR +
                    WindowView.getInstance().getUser().getLogin() + FILE_SEPARATOR +
                    playlistName + FILE_SEPARATOR;
            String dest = playlistPath + track.getName() + "." + track.getFileFormat();

            track.setFileFolder(dest);

            listTrack.add(track);
        }
        return listTrack;
    }

    public static Track JsonObjectTrack(JSONObject jsonObject) throws JSONException {

        Track track = new Track(Integer.valueOf(getStringJsonObject(jsonObject.get("id"))),
                Double.valueOf(getStringJsonObject(jsonObject.get("duration"))),
                getStringJsonObject(jsonObject.get("src_tracks")),
                getStringJsonObject(jsonObject.get("src_tracks")).substring(getStringJsonObject(jsonObject.get("src_tracks")).lastIndexOf(".") + 1),
                getStringJsonObject(jsonObject.get("title")),
                getStringJsonObject(jsonObject.get("artist")),
                getStringJsonObject(jsonObject.get("album")),
                getStringJsonObject(jsonObject.get("genre")));

        track.setSrcTracks(getStringJsonObject(jsonObject.get("src_tracks")));
        return track;
    }

    public static String getStringJsonObject(Object value) {

        return (value == null) ? "" : value.toString();
    }

    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        // This is the only way to delete an event : make a new one useless -> removeEventHandler() doesn't work.
        tableTracks.setOnMouseClicked(Event::consume);
        tableTracks.setOnDragDetected(Event::consume);

        MusicsResearchList mrl = (MusicsResearchList) aml;

        musicNumber.setText(String.valueOf(trackList.size()));

        List<Playlist> playlists = mrl.getPlaylists();
        listViewPlaylist.getItems().setAll(playlists);

        listViewPlaylist.setCellFactory((ListView<Playlist> lv) -> new ListCell<>() {
            @Override
            protected void updateItem(Playlist item, boolean empty) {
                super.updateItem(item, empty);

                if (!empty && item != null) {
                    setText(item.getPlaylistName());
                }
            }
        });

        listViewPlaylist.setOnMouseClicked(event -> {
            Playlist focusedPlt = listViewPlaylist.getFocusModel().getFocusedItem();

            if (focusedPlt != null && event.getClickCount() >= 2) {

                try {
                    if (wdv.getPlaylistList().havePlaylist(focusedPlt.getPlaylistName())) {
                        wdv.getController().setMessageUpload(false);
                        wdv.getController().setMessage("Cette playlist a déjà été importé");
                        return;
                    }

                    JSONObject result = UserServices.getPlaylistOps().getPlaylist(focusedPlt, wdv.getUser());

                    if (result.get("visibilty").toString().equals("1") && result.get("password") != null) {
                        try {
                            CheckPasswordWindow.createInstance(result.get("name").toString(), result);
                        } catch (InstanceAlreadyExistsException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        Image image = new Image("file:" + PLAYLIST_PICTURES_DIRECTORY_PATH + FILE_SEPARATOR + result.get("src_image"));

                        final Playlist playlist = new Playlist(UserServices.getUserOp().getUserById(Integer.parseInt(getStringJsonObject(result.get("user_id")))),
                                image,
                                getStringJsonObject(result.get("name")),
                                true);

                        final List<Track> listTrack = JsonObjectListTrack(result, playlist.getPlaylistName());

                        playlist.addTrack(listTrack);
                        savePlaylistToDirectory(playlist);

                        Task task = new Task<Boolean>() {
                            {
                                this.setOnSucceeded(t -> {
                                    wdv.getPlaylistList().addPlaylist(playlist);
                                    wdv.getController().setMessageUpload(true);
                                    wdv.getController().setMessage("La playlist " + playlist.getPlaylistName() + " à été importé");
                                });
                            }

                            @Override
                            public Boolean call() throws IOException {
                                saveToLocalTrack(listTrack, playlist);
                                saveToLocalImage(playlist);
                                return true;
                            }
                        };

                        wdv.getController().setMessageUpload(true);
                        wdv.getController().setMessage("Synchronisation en cours ...");
                        new Thread(task).start();
                    }
                } catch (ParseException | IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}