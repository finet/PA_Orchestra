package fr.orchestra.controller.folderview.view.middle;


import fr.orchestra.controller.folderview.tree.folder.ItemEntity;
import fr.orchestra.controller.folderview.view.middle.template.AbstractViewController;
import fr.orchestra.model.Track;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ResourceBundle;


public class DirectoryViewController extends AbstractViewController {


    @FXML
    private Label musicNumber;

    @FXML
    private Label totalMusicDuration;

    @FXML
    private ListView<ItemEntity.Directory> listViewDir;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        MusicsDirectoryList mdl = (MusicsDirectoryList) aml;

        musicNumber.setText(String.valueOf(trackList.size()));

        double ttDuration = 0.0;
        for (Track track : trackList) {
            ttDuration += track.getTotalTime();
        }
        totalMusicDuration.setText(new SimpleDateFormat("mm:ss").format(ttDuration));

        List<ItemEntity.Directory> dirList = mdl.getDirectories();
        listViewDir.getItems().setAll(dirList);

        listViewDir.setCellFactory((ListView<ItemEntity.Directory> lv) -> new ListCell<>() {
            @Override
            protected void updateItem(ItemEntity.Directory item, boolean empty) {
                super.updateItem(item, empty);

                if (!empty && item != null) {
                    setText(item.getDisplayName());
                }
            }
        });

        listViewDir.setOnMouseClicked(event -> {
            if (listViewDir.getFocusModel().getFocusedItem() != null) {
                ItemEntity.Directory dirSelected = listViewDir.getFocusModel().getFocusedItem();

                windowView.setMiddleViewContentList(new MusicsDirectoryList(dirSelected.getDisplayName(), windowView.getListTrack(dirSelected.getChildrenMusic()).toArray(new Track[0]), dirSelected.getChildrenDir()));
                windowView.getMiddleViewContentList().printView();
            }
        });
    }
}

