package fr.orchestra.controller.folderview.view.middle.template;

import fr.orchestra.controller.folderview.ViewController;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.model.Track;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public abstract class AbstractMusicsList {

    private String name;
    private Image imageParent;
    private List<Track> trackList = new ArrayList<>();

    private ViewController viewController = WindowView.getInstance().getController();

    public AbstractMusicsList(String name, Track[] trackList) {

        this.name = name;
        this.trackList.addAll(Arrays.asList(trackList));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Image getImageParent() {
        return imageParent;
    }

    public void setImageParent(Image imageParent) {
        this.imageParent = imageParent;
    }

    public List<Track> getTrackList() {
        return trackList;
    }

    public void printView() {
        final URL fxmlURL = getFxmlUrl();
        final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);

        try {
            viewController.setPrintWindowAnchorPane(fxmlLoader.load());
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    protected abstract URL getFxmlUrl();
}
