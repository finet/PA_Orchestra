package fr.orchestra.controller.folderview.view.middle.template;


import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.mediaplayer.player.PlayerManager;
import fr.orchestra.model.Track;
import fr.orchestra.utils.dragboard.DragDrop;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;


public abstract class AbstractViewController implements Initializable {

    protected WindowView windowView;
    @FXML
    protected TableView<Track> tableTracks;
    @FXML
    protected TableColumn<Track, String> columnName;
    @FXML
    protected TableColumn<Track, String> columnArtist;
    @FXML
    protected TableColumn<Track, String> columnGenre;
    @FXML
    protected TableColumn<Track, String> columnAlbum;
    protected AbstractMusicsList aml;
    protected List<Track> trackList;
    @FXML
    private Label viewName;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        windowView = WindowView.getInstance();

        aml = windowView.getMiddleViewContentList();

        trackList = aml.getTrackList();
        tableTracks.getItems().setAll(trackList);

        columnName.setCellValueFactory((TableColumn.CellDataFeatures<Track, String> track) -> new SimpleStringProperty(track.getValue().getName()));
        columnArtist.setCellValueFactory((TableColumn.CellDataFeatures<Track, String> track) -> new SimpleStringProperty(track.getValue().getArtist()));
        columnGenre.setCellValueFactory((TableColumn.CellDataFeatures<Track, String> track) -> new SimpleStringProperty(track.getValue().getGenre()));
        columnAlbum.setCellValueFactory((TableColumn.CellDataFeatures<Track, String> track) -> new SimpleStringProperty(track.getValue().getAlbum()));

        // Remove selection model when click outside of a not empty line
        windowView.getScene().addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
            Node source = event.getPickResult().getIntersectedNode();

            Node initSource = source;
            // Move up through the node hierarchy until a TableRow or scene root is found
            while (source != null && (!(source instanceof TableRow))) {
                source = source.getParent();
            }

            // Clear selection on click anywhere but on a filled row
            if (source == null || (source instanceof TableRow && ((TableRow) source).isEmpty())) {
                tableTracks.getSelectionModel().clearSelection();
            }
        });
    }

    @FXML
    private void onDragLineDetected(MouseEvent event) {

        Track trackSelected = tableTracks.getSelectionModel().getSelectedItem();
        DragDrop.dragDetected(trackSelected, tableTracks);
    }

    @FXML
    private void onMouseClickedLine(MouseEvent event) {

        if (event.getButton().equals(MouseButton.PRIMARY)) {

            if (event.getClickCount() >= 2 && tableTracks.getSelectionModel().getSelectedItem() != null) {

                PlayerManager pm = PlayerManager.getInstance();

                pm.getPlayerQueue().clear();
                pm.getPlayerQueue().addAll(trackList);

                /* Warning, getTrackId() don't work on playlist :/ */
                pm.setTrackNumber(tableTracks.getSelectionModel().getSelectedItem().getTrackId());
                pm.setTrackNumber(tableTracks.getFocusModel().getFocusedCell().getRow());

                pm.playByRepertoryClick();
            }
        }
    }
}
