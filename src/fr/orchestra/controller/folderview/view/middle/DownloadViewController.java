package fr.orchestra.controller.folderview.view.middle;

import fr.orchestra.controller.folderview.view.middle.template.AbstractViewController;
import fr.orchestra.model.Track;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

public class DownloadViewController extends AbstractViewController {

    @FXML
    private Label musicNumber;

    @FXML
    private Label totalMusicDuration;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        //MusicsDownloadList mdl = (MusicsDownloadList) aml;

        musicNumber.setText(String.valueOf(trackList.size()));

        double ttDuration = 0.0;
        for (Track track : trackList) {
            ttDuration += track.getTotalTime();
        }
        totalMusicDuration.setText(new SimpleDateFormat("mm:ss").format(ttDuration));
    }
}
