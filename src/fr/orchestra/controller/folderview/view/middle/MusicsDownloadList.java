package fr.orchestra.controller.folderview.view.middle;


import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.folderview.view.middle.template.AbstractMusicsList;
import fr.orchestra.model.Track;
import fr.orchestra.utils.annotation.ResourceFile;

import java.net.URL;


public class MusicsDownloadList extends AbstractMusicsList {

    @ResourceFile("fxml.folderview.downloadView")
    private static URL downloadView;

    /**
     * Description : This constructor must show the music who have been download
     * Location : Middle pane of the folderview
     *
     * @param tracks Musics downloaded to be print
     */

    public MusicsDownloadList(Track[] tracks) {
        super("Musiques Téléchargées", tracks);

        WindowView.getInstance().setMiddleViewContentList(this);
    }

    @Override
    protected URL getFxmlUrl() {
        return downloadView;
    }
}
