package fr.orchestra.controller.folderview.plugin;

import fr.orchestra.utils.annotation.ResourceFile;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;

public class DeletePluginListView {

    @ResourceFile("fxml.folderview.deletePluginView")
    private static URL fxmlURL;
    @ResourceFile(value = "css.folderView")
    private static String folderViewCSS;
    private Parent childRoot;
    private Scene scene;
    private Stage stage;
    private PluginViewController controller;

    public DeletePluginListView() throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
        this.childRoot = fxmlLoader.load();
        this.controller = fxmlLoader.getController();

        scene = new Scene(childRoot);
        stage = new Stage();
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);

        /** initialise la stage du lecteur audio dans son controller */
        controller.setStage(stage);

        /** Charge css for window design */
        scene.getStylesheets().add(folderViewCSS);
        stage.show();
    }

    public Stage getStage() {
        return stage;
    }

    public PluginViewController getController() {
        return controller;
    }
}
