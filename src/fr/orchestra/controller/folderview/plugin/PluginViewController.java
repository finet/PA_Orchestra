package fr.orchestra.controller.folderview.plugin;


import fr.orchestra.controller.folderview.WindowView;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;


public class PluginViewController implements Initializable {

    private final Map<String, ObservableValue<Boolean>> properties = new HashMap<>();
    private Stage stage;
    @FXML
    private ListView<String> pluginListView;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        List<URL> pluginList = WindowView.getInstance().getController().getJarFiles();
        String[] pluginsName = new String[pluginList.size()];

        int cnt = 0;
        for (URL plugin : pluginList) {
            pluginsName[cnt] = plugin.getFile();
        }

        for (final String name : pluginsName) {
            final BooleanProperty property = new SimpleBooleanProperty();

            property.setValue(false);
            properties.put(name, property);

        }

        final Callback<String, ObservableValue<Boolean>> propertyAccessor = properties::get;
        final StringConverter<String> labelConverter = new StringConverter<>() {

            @Override
            public String toString(String string) {
                return string == null ? null : string.substring(string.lastIndexOf('/') + 1);
            }

            @Override
            public String fromString(String string) {
                throw new UnsupportedOperationException("Not use");
            }
        };


        pluginListView.setCellFactory((ListView<String> p) -> {
            final CheckBoxListCell<String> result = new CheckBoxListCell<>();
            result.setConverter(labelConverter);
            result.setSelectedStateCallback(propertyAccessor);
            return result;
        });
        pluginListView.getItems().setAll(pluginsName);
    }

    @FXML
    private void validatePluginDeleted(ActionEvent event) {

        List<URL> pluginToDelete = new ArrayList<>();
        for (Map.Entry<String, ObservableValue<Boolean>> entry : properties.entrySet()) {
            if (entry.getValue().getValue()) {
                try {
                    pluginToDelete.add(new File(entry.getKey()).toURI().toURL());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }

        WindowView.getInstance().getController().getJarRemoveFiles().addAll(pluginToDelete);
        stage.close();
    }

    @FXML
    private void close() {
        stage.close();
    }
}
