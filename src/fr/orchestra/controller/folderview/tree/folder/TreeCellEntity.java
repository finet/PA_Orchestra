package fr.orchestra.controller.folderview.tree.folder;

import fr.orchestra.model.Track;
import fr.orchestra.utils.annotation.ResourceFile;
import fr.orchestra.utils.dragboard.DragDrop;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;

public class TreeCellEntity extends TreeCell<ItemEntity> {

    @ResourceFile("fxml.folderview.treeItem")
    private static URL treeItemFXML;
    private Node folderRenderer = null;
    private TreeItemController treeItemController = null;

    public static Callback<TreeView<ItemEntity>, TreeCell<ItemEntity>> forTreeView() {
        return (TreeView<ItemEntity> treeView) -> new TreeCellEntity();
    }

    @Override
    protected void updateItem(final ItemEntity value, final boolean empty) {

        super.updateItem(value, empty);

        textProperty().unbind();
        setText(null);
        setStyle(null);
        setGraphic(null);
        if (value != null && !empty) {
            if (value instanceof ItemEntity.Directory) {
                clearRenderer();
                configureForDirectory((ItemEntity.Directory) value);
            } else {
                clearRenderer();
            }
        } else {
            clearRenderer();
        }
    }

    private void configureForDirectory(final ItemEntity.Directory value) {

        try {
            if (folderRenderer == null) {

                final FXMLLoader fxmlLoader = new FXMLLoader(treeItemFXML);

                folderRenderer = fxmlLoader.load();
                treeItemController = fxmlLoader.getController();
                confForDragAndDrop();
            }
            treeItemController.setDirectory(value);
            setGraphic(folderRenderer);

        } catch (IOException ioe) {

        }
    }

    private void clearRenderer() {

        if (folderRenderer != null) {
            treeItemController.setDirectory(null);
            folderRenderer = null;
            treeItemController = null;
        }
    }

    private void confForDragAndDrop() {
        this.addEventHandler(MouseEvent.DRAG_DETECTED, handler -> {
            Track[] tracksSelected = treeItemController.getTrackList().toArray(new Track[0]);
            DragDrop.dragDetected(tracksSelected, this);
        });
    }
}
