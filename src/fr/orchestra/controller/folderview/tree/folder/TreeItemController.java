package fr.orchestra.controller.folderview.tree.folder;


import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.folderview.view.middle.MusicsDirectoryList;
import fr.orchestra.model.Track;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class TreeItemController implements Initializable {

    private final ObjectProperty<ItemEntity.Directory> directory = new SimpleObjectProperty<>(this, "directory");
    @FXML
    private Label dirName;
    @FXML
    private AnchorPane mainPane;
    private List<Track> trackList = null;

    @FXML
    private void OpenLocalDirectory(MouseEvent event) {

        MusicsDirectoryList pwa = new MusicsDirectoryList(dirName.getText(), getTrackList().toArray(new Track[0]), getSubDirectories());
        pwa.printView();
        event.consume();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public ItemEntity.Directory getDirectory() {
        return directory.get();
    }

    public void setDirectory(ItemEntity.Directory directory) {

        if (directory == null) unbindDisplayName();

        this.directory.set(directory);

        if (this.directory.get() != null) bindDisplayName();
    }

    public ObjectProperty<ItemEntity.Directory> directoryProperty() {
        return directory;
    }

    private void bindDisplayName() {
        dirName.textProperty().bind(Bindings.selectString(directory, "displayName"));
    }

    private void unbindDisplayName() {
        dirName.textProperty().unbind();
    }

    private List<ItemEntity.Music> getSubFiles() {

        return new ArrayList<>(directory.get().getChildrenMusic());
    }

    private List<ItemEntity.Directory> getSubDirectories() {

        return new ArrayList<>(directory.get().getChildrenDir());
    }

    public List<Track> getTrackList() {

        if (trackList == null) {
            List<ItemEntity.Music> musicList = getSubFiles();
            trackList = WindowView.getInstance().getListTrack(musicList);
        }
        return trackList;
    }
}
