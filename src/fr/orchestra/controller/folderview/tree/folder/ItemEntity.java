package fr.orchestra.controller.folderview.tree.folder;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.util.InvalidPropertiesFormatException;
import java.util.LinkedList;


public class ItemEntity {

    private static int ID_GENERATOR = 0;
    private final ReadOnlyStringWrapper id = new ReadOnlyStringWrapper(this, "id");
    private final ReadOnlyObjectWrapper<Directory> parent = new ReadOnlyObjectWrapper<>(this, "parent");
    private final StringProperty displayName = new SimpleStringProperty(this, "displayName");

    public ItemEntity() {
        id.set(String.valueOf(ID_GENERATOR++));
    }

    public String getId() {
        return id.get();
    }

    public ReadOnlyStringProperty idProperty() {
        return id.getReadOnlyProperty();
    }

    public final Directory getParent() {
        return parent.get();
    }

    protected final void setParent(final Directory value) {
        parent.set(value);
    }

    public final ReadOnlyObjectProperty<Directory> parentProperty() {
        return parent.getReadOnlyProperty();
    }

    public final String getDisplayName() {
        return displayName.get();
    }

    public final void setDisplayName(final String value) {
        displayName.set(value);
    }

    public final StringProperty displayNameProperty() {
        return displayName;
    }


    public static class Directory extends ItemEntity {

        private final ListProperty<Directory> childrenDir = new SimpleListProperty<>(FXCollections.observableList(new LinkedList<>()));
        private final ListProperty<Music> childrenMusic = new SimpleListProperty<>(FXCollections.observableList(new LinkedList<>()));
        private File dir;

        public Directory(File dir, Directory parentDir) throws InvalidPropertiesFormatException {

            if (!dir.isDirectory()) throw new InvalidPropertiesFormatException("This class require a DIRECTORY File");

            this.dir = dir;
            setDisplayName(dir.getName());

            setParent(parentDir);
        }

        public File getDirFile() {
            return dir;
        }

        public final ObservableList<Directory> getChildrenDir() {
            return childrenDir;
        }

        public final ObservableList<Music> getChildrenMusic() {
            return childrenMusic;
        }

        @Override
        public String toString() {
            return dir.toString()
                    + " : " + childrenDir.toString()
                    + " && \n" + childrenMusic.toString();
        }
    }

    public static class Music extends ItemEntity {

        private File file;

        private Music() {
        }


        /*public void setFile(File file, Directory parentDir) throws InvalidPropertiesFormatException {

            if(!file.isFile()) throw new InvalidPropertiesFormatException("This getter require a FILE File");
            this.file = file;

            setParent(parentDir);
        }*/

        public Music(File file, Directory parentDir) throws InvalidPropertiesFormatException {

            if (!file.isFile()) throw new InvalidPropertiesFormatException("This class require a FILE File");
            this.file = file;

            setParent(parentDir);
        }

        public File getFile() {
            return file;
        }
    }
}
