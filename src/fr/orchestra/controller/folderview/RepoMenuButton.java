package fr.orchestra.controller.folderview;

import fr.orchestra.controller.folderview.tree.folder.ItemEntity;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;

import java.io.File;

public class RepoMenuButton {

    private ViewController viewController;

    private MenuButton repoMenuButton;
    private TreeView<ItemEntity> treeView;
    private TreeViewFolder tvf;

    public RepoMenuButton(MenuButton repoMenuButton, TreeView<ItemEntity> treeView, ViewController viewController) {

        this.viewController = viewController;

        this.repoMenuButton = repoMenuButton;
        MenuItem addRepoMenuItem = repoMenuButton.getItems().get(repoMenuButton.getItems().size() - 1);

        this.treeView = treeView;
        addRepoMenuItem.setOnAction(event -> openFileExplorer());
    }

    public MenuButton getRepoMenuButton() {
        return repoMenuButton;
    }

    private void openFileExplorer() {

        DirectoryChooser dc = new DirectoryChooser();
        File currentRoot = dc.showDialog(null);

        // @TODO uncomment last line and delete next line
        // WINDOW
        // File currentRoot = new File("D:\\zik");
        // File currentRoot = new File("C:\\Users\\paul\\Desktop\\Musiques");
        // File currentRoot = new File("F:\\ESGI\\ProjetAnnuel\\testTreeFolder");

        // LINUX
        // File currentRoot = new File("/media/damien/BAGUETTE/ESGI/ProjetAnnuel/testTreeFolder");
        // File currentRoot = new File("/home/damien/benchmark");


        if (currentRoot != null && currentRoot.exists()) {

            //repoMenuButton.setText(currentRoot.getPlaylistName());
            viewController.getWindowView().setTreeViewFolder(new TreeViewFolder(treeView, currentRoot));

            int itemPos = hasItem(currentRoot);
            if (itemPos == -1) {
                repoMenuButton.getItems().add(0, new RootCustomMenuItem(currentRoot));
            } else {
                repoMenuButton.getItems().remove(itemPos);
                repoMenuButton.getItems().add(0, new RootCustomMenuItem(currentRoot));
            }
        }
    }

    private int hasItem(File file) {

        for (int cnt = 0; cnt < repoMenuButton.getItems().size(); cnt++) {

            MenuItem tmpMi = repoMenuButton.getItems().get(cnt);

            if (tmpMi.getText().equalsIgnoreCase(file.toString())) return cnt;
        }
        return -1;
    }

    private class RootCustomMenuItem extends CustomMenuItem {

        private Label lbl;
        private Pane pn;
        private File pathRoot;

        public RootCustomMenuItem(File pathRoot) {

            lbl = new Label(pathRoot.getName());
            lbl.setStyle("-fx-min-width: 150px; -fx-pref-width: 150px; -fx-max-width: 150px;");
            this.setText("Fichiers locaux");
            this.setContent(lbl);
            this.pathRoot = pathRoot;

            lbl.setOnMouseClicked(this::actionChoose);
        }

        private void actionChoose(MouseEvent event) {

            if (event.getButton() == MouseButton.PRIMARY) {
                repoMenuButton.getItems().remove(this);
            }

            File currentRoot = this.pathRoot;
            repoMenuButton.getItems().remove(this);
            repoMenuButton.getItems().add(0, this);
            viewController.getWindowView().setTreeViewFolder(new TreeViewFolder(treeView, currentRoot));
        }

        private boolean hasFile(File file) {

            return pathRoot == file;
        }
    }
}
