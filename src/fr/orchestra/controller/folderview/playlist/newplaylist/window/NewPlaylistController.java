package fr.orchestra.controller.folderview.playlist.newplaylist.window;

import fr.orchestra.api.UserServices;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.folderview.playlist.list.view.PlaylistList;
import fr.orchestra.model.Playlist;
import fr.orchestra.utils.properties.ConstProperties;
import fr.orchestra.utils.system.ConfSys;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.client.HttpResponseException;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.ResourceBundle;

import static fr.orchestra.utils.Utils.copyFileUsingStream;


public class NewPlaylistController implements Initializable, ConstProperties, ConfSys {

    private Stage stage;
    private PlaylistList pll;

    @FXML
    private TextField nameInput;
    @FXML
    private ImageView imagePlaylist;

    public static Image copyImage(Image image, String newName) {

        String ext = FilenameUtils.getExtension(image.getUrl());
        String dest = PLAYLIST_PICTURES_DIRECTORY_PATH + FILE_SEPARATOR + newName + "." + ext;

        copyFileUsingStream(image.getUrl().substring(5 + ConfSys.OS_NUM), dest);
        return new Image("file:" + PLAYLIST_PICTURES_DIRECTORY_PATH + FILE_SEPARATOR + newName + "." + ext);
    }

    public static void savePlaylistToDirectory(Playlist playlist) {

        Path directoryPlaylist = Paths.get(PLAYLIST_DIRECTORY_PATH + FILE_SEPARATOR +
                WindowView.getInstance().getUser().getLogin() + FILE_SEPARATOR +
                playlist.getPlaylistName());

        /* create directory for Playlist user */
        try {
            Files.createDirectory(directoryPlaylist);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Playlist.writeSerializedPlaylist(playlist, directoryPlaylist);
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pll = WindowView.getInstance().getPlaylistList();
    }

    @FXML
    private void close() {
        stage.close();
    }

    @FXML
    private void SendNewPlaylistInf(MouseEvent event) {

        String name = nameInput.getText();
        if (name.isEmpty() || pll.havePlaylist(name) || name.length() < 3 || name.matches(".*\\s.*")) {
            nameInput.clear();
            nameInput.setPromptText("Nom de la Playlist INVALIDE");
            return;
        }

        Image image = copyImage(imagePlaylist.getImage(), name);
        Playlist playlist = new Playlist(WindowView.getInstance().getUser().getLogin(), image, name, true);

        String check = checkImage(image);

        if (check.equals("")) {
            try {
                if (!UserServices.getPlaylistOps().createNewPlaylist(playlist, WindowView.getInstance().getUser())) {
                    nameInput.clear();
                    nameInput.setPromptText("Erreur lors de la création de la playlist");
                    return;
                }
            } catch (HttpResponseException | JSONException e) {
                e.printStackTrace();
            }

            WindowView.getInstance().getController().setMessageUpload(true);
            WindowView.getInstance().getController().setMessage("Playlist créée");
        }
        else {
            nameInput.clear();
            nameInput.setPromptText(check);
            return;
        }


        savePlaylistToDirectory(playlist);

        pll.addPlaylist(playlist);
        close();
    }

    private String checkImage(Image image) {
        int MAX_MEMORY_SIZE = 1024 * 1024 * 3;

        File file = new File(image.getUrl().substring(5));
        String extension = FilenameUtils.getExtension(image.getUrl());
        long size = file.length();

        String[] extendsFileArray = {"png", "jpg", "gif"};
        if (!Arrays.asList(extendsFileArray).contains(extension))
            return "Extension non acceptée, seulement \".png\", \".jpg\" et \".gif\" le sont";
        if (size > MAX_MEMORY_SIZE)
            return "Taille maximum de l'image: " + MAX_MEMORY_SIZE + "Mo ; Contre " + size + "Mo pour la votre";
        return "";
    }

    @FXML
    private void SelectPicture(MouseEvent event) {

        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        File imageFile = fc.showOpenDialog(null);

        if (imageFile != null && imageFile.exists()) {
            imagePlaylist.setImage(new Image(imageFile.toURI().toString()));
        }
    }
}