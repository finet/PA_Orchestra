package fr.orchestra.controller.folderview.playlist.list.view;


import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.folderview.view.middle.MusicsPlaylistList;
import fr.orchestra.model.Playlist;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;


public class PlaylistList {

    private ListView<Playlist> playlistList;

    public PlaylistList(ListView<Playlist> playlistList) {

        this.playlistList = playlistList;
        this.playlistList.setCellFactory(PlaylistListCell.forPlaylistListView());

        playlistList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {

            if (newValue != null && !newValue.equals(oldValue)) {

                MusicsPlaylistList mp = new MusicsPlaylistList(newValue);
                mp.printView();
            }
        });

        WindowView.getInstance().setPlaylistList(this);
    }

    public void addPlaylist(Playlist playlist) {
        playlistList.getItems().add(playlist);
    }

    public void removePlaylist(Playlist playlist) {
        playlistList.getItems().remove(playlist);
    }

    public boolean havePlaylist(String plName) {

        ObservableList<Playlist> obl = playlistList.getItems();
        for (int cnt = 0; cnt < playlistList.getItems().size(); cnt++) {
            if (obl.get(cnt).getPlaylistName().equals(plName)) return true;
        }
        return false;
    }
}
