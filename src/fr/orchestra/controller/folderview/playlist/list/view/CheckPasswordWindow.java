package fr.orchestra.controller.folderview.playlist.list.view;

import org.json.JSONObject;
import fr.orchestra.utils.annotation.ResourceFile;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.management.InstanceAlreadyExistsException;
import java.io.IOException;
import java.net.URL;


public class CheckPasswordWindow {

    private static CheckPasswordWindow INSTANCE = null;

    public static CheckPasswordWindow getInstance() {
        return INSTANCE;
    }

    public static synchronized void createInstance(String name, JSONObject result) throws IOException, InstanceAlreadyExistsException {
        if (INSTANCE != null)
            throw new InstanceAlreadyExistsException("One instance of " + CheckPasswordWindow.class + " is already existing, you cannot create a new one");
        else INSTANCE = new CheckPasswordWindow(name, result);
    }

    @ResourceFile(value = "fxml.folderview.checkPassword")
    private static URL fxmlURL;
    @ResourceFile(value = "css.folderView")
    private static String folderViewCSS;

    private Stage stage;
    private CheckPasswordController controller;
    private String name;

    private JSONObject result;

    private CheckPasswordWindow(String name, JSONObject result) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
        Parent childRoot = fxmlLoader.load();
        this.controller = fxmlLoader.getController();

        this.name = name;
        this.result = result;

        Scene scene = new Scene(childRoot);
        stage = new Stage();
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);

        /* initialise la stage du lecteur audio dans son controller */
        controller.setStage(stage);

        /* Charge css for window design */
        scene.getStylesheets().add(folderViewCSS);
        stage.show();
    }

    public Stage getStage() {
        return stage;
    }

    public CheckPasswordController getController() {
        return controller;
    }

    public String getName() {
        return name;
    }

    public JSONObject getResult() {
        return result;
    }
}
