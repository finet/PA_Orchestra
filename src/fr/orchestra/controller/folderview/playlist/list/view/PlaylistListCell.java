package fr.orchestra.controller.folderview.playlist.list.view;

import org.json.JSONException;
import org.json.JSONObject;
import fr.orchestra.api.UserServices;
import fr.orchestra.controller.folderview.ViewController;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.model.Playlist;
import fr.orchestra.model.Track;
import fr.orchestra.model.User;
import fr.orchestra.utils.annotation.ResourceFile;
import fr.orchestra.utils.dragboard.DragDrop;
import fr.orchestra.utils.system.ConfSys;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import static fr.orchestra.utils.Utils.copyFileUsingStream;
import static fr.orchestra.utils.properties.ConstProperties.PLAYLIST_DIRECTORY_PATH;


public class PlaylistListCell extends ListCell<Playlist> implements ConfSys {

    @ResourceFile(value = "fxml.folderview.playlistCell")
    private static URL fxmlURL;

    private WindowView wdv = WindowView.getInstance();
    private ViewController vcl = wdv.getController();
    private User user = wdv.getUser();
    private Pane pane = null;
    private PlaylistCellController playlistCellController;

    private PlaylistListCell() {
    }

    public static Callback<ListView<Playlist>, ListCell<Playlist>> forPlaylistListView() {

        return (ListView<Playlist> ListView) -> new PlaylistListCell();

    }

    @Override
    protected void updateItem(Playlist playlist, boolean empty) {
        super.updateItem(playlist, empty);

        Node graphic;
        setText(null);
        setStyle(null);
        setGraphic(null);
        if (!empty && playlist != null) {

            configureItem();
            graphic = pane;

            if (playlistCellController != null) playlistCellController.setPlaylist(playlist);
            setGraphic(graphic);
        }
    }

    private void configureItem() {

        try {
            if (pane == null) {
                final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
                pane = fxmlLoader.load();
                playlistCellController = fxmlLoader.getController();
                confForDragAndDrop();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void confForDragAndDrop() {
        this.addEventHandler(MouseEvent.DRAG_DETECTED, handler -> {
            Track[] trackSelected = playlistCellController.getPlaylist().getTrackList();
            DragDrop.dragDetected(trackSelected, this);
        });

        this.addEventHandler(DragEvent.DRAG_OVER, handler -> {
            DragDrop.dropAllow(Track.class, handler);
            DragDrop.dropAllow(Track[].class, handler);
        });

        this.addEventHandler(DragEvent.DRAG_DROPPED, this::getTrackByDrop);
    }

    private void getTrackByDrop(DragEvent event) {

        if (pane == null) {
            event.setDropCompleted(false);
            return;
        }

        Track trackDropped = (Track) DragDrop.getDropData(Track.class, event);

        if (trackDropped != null) {
            for (Track trackPlaylist : playlistCellController.getPlaylist().getTrackList()) {
                if (trackPlaylist.getName().equals(trackDropped.getName()) && trackPlaylist.getArtist().equals(trackDropped.getArtist())) {
                    vcl.setMessageUpload(false);
                    vcl.setMessage("Cette musique a déjà était ajouté à la playlist");
                    return;
                }
            }
            playlistCellController.getPlaylist().addTrack(trackDropped);
            addTrackToDirectory(trackDropped);
            UploadTrackToPlaylist(trackDropped);
        } else {

            Track[] tracksDropped = (Track[]) DragDrop.getDropData(Track[].class, event);

            if (tracksDropped != null && tracksDropped.length != 0) {
                for (Track trackPlaylistDropped : tracksDropped) {
                    boolean error = false;

                    for (Track trackPlaylist : playlistCellController.getPlaylist().getTrackList()) {
                        if (trackPlaylist.getName().equals(trackPlaylistDropped.getName()) && trackPlaylist.getArtist().equals(trackPlaylistDropped.getArtist())) {
                            error = true;
                        }
                    }

                    if (!error) {
                        playlistCellController.getPlaylist().addTrack(trackPlaylistDropped);
                        addTrackToDirectory(trackPlaylistDropped);
                        UploadTrackToPlaylist(trackPlaylistDropped);
                    }
                }
            } else {
                event.setDropCompleted(false);
                return;
            }
        }
        event.setDropCompleted(true);
    }

    /**
     * Add track in directory of playlist
     */
    private void addTrackToDirectory(List<Track> trackList) {

        String playlistPath = PLAYLIST_DIRECTORY_PATH + FILE_SEPARATOR +
                user.getLogin() + FILE_SEPARATOR +
                playlistCellController.getPlaylist().getPlaylistName() + FILE_SEPARATOR;

        for (Track aTrackList : trackList) {
            String dest = playlistPath + aTrackList.getName() + "." + aTrackList.getFileFormat();
            copyFileUsingStream(aTrackList.getFileFolder(), dest);
        }
    }

    private void addTrackToDirectory(Track track) {

        String playlistPath = PLAYLIST_DIRECTORY_PATH + FILE_SEPARATOR +
                user.getLogin() + FILE_SEPARATOR +
                playlistCellController.getPlaylist().getPlaylistName() + FILE_SEPARATOR;

        String dest = playlistPath + track.getName() + "." + track.getFileFormat();
        copyFileUsingStream(track.getFileFolder(), dest);
    }

    private void UploadTrackToPlaylist(List<Track> trackList) {

        vcl.allTrackUploadStatus = -1;

        Task<? extends Void> task = new Task<Void>() {
            @Override
            public Void call() throws JSONException {

                for (Track aTrackList : trackList) {
                    if (Track.checkTrack(aTrackList).equals(""))
                        vcl.allTrackUploadStatus(upload(aTrackList).equals(""));
                    else vcl.allTrackUploadStatus(false);
                }

                return null;
            }
        };
        new Thread(task).start();
    }


    private void UploadTrackToPlaylist(Track track) {

        String resultCheckTrack = Track.checkTrack(track);

        Task<? extends Void> task = new Task<Void>() {
            @Override
            public Void call() throws JSONException {

                String tmpMsg;

                if (resultCheckTrack.equals("")/* || resultCheckTrack.equals("Son déjà enregistré")*/) {

                    String result = upload(track);

                    if (result.equals("")) {
                        vcl.setMessageUpload(true);
                        tmpMsg = track.getName() + " de " + track.getArtist() + " à été exporté";
                    } else {
                        vcl.setMessageUpload(false);
                        tmpMsg = result;
                    }
                } else {
                    vcl.setMessageUpload(false);
                    tmpMsg = resultCheckTrack;
                }

                /* Update interface on the JavaFX Application Thread */
                final String finalTmpMsg = tmpMsg;
                Platform.runLater(() -> vcl.setMessage(finalTmpMsg));
                return null;
            }
        };
        new Thread(task).start();
    }


    private String upload(Track track) throws JSONException {

        Playlist playlist = playlistCellController.getPlaylist();

        JSONObject JSONAuthorization = UserServices.getPlaylistOps().checkAuthorizationForPlaylist(playlist.getPlaylistName(), user);

        if (JSONAuthorization.get("error").toString().equals("false")) {

            JSONObject jsonPlaylist = (JSONObject) JSONAuthorization.get("playlist");
            int playlist_id = Integer.valueOf(jsonPlaylist.get("id").toString());
            JSONObject jsonCreateNewTracks = UserServices.getTrackOps().createNewTracks(track, user);

            if (jsonCreateNewTracks.get("error").toString().equals("false") || jsonCreateNewTracks.get("message").toString().equals("Son déja enregistré")) {

                JSONObject JSONTrack = (JSONObject) jsonCreateNewTracks.get("track");
                int trackId = Integer.valueOf(JSONTrack.get("id").toString());
                JSONObject jsonAddLinkTrackPlaylist = UserServices.getPlaylistOps().createNewTrackToPlaylist(trackId, playlist_id, user);

                if (jsonAddLinkTrackPlaylist.get("error").toString().equals("false")) return "";
                else return jsonAddLinkTrackPlaylist.get("message").toString();
            }
            else return jsonCreateNewTracks.get("message").toString();
        }
        else return JSONAuthorization.get("message").toString();
    }
}
