package fr.orchestra.controller.folderview.playlist.list.view;


import fr.orchestra.api.UserServices;
import fr.orchestra.controller.folderview.ViewController;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.model.Playlist;
import fr.orchestra.model.Track;
import fr.orchestra.utils.properties.ConstProperties;
import fr.orchestra.utils.system.ConfSys;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.json.JSONObject;
import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.List;
import java.util.ResourceBundle;

import static fr.orchestra.controller.folderview.playlist.newplaylist.window.NewPlaylistController.savePlaylistToDirectory;
import static fr.orchestra.controller.folderview.view.middle.ResearchViewController.*;


public class CheckPasswordController implements Initializable, ConstProperties, ConfSys {

    private Stage stage;
    private WindowView wdv;
    private PlaylistList pll;
    private ViewController vcl;
    private CheckPasswordWindow cpw;

    @FXML
    private TextField checkPasswordInput;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        wdv = WindowView.getInstance();
        pll = wdv.getPlaylistList();
        vcl = wdv.getController();
        cpw = CheckPasswordWindow.getInstance();
    }

    @FXML
    private void close() {
        stage.close();
    }

    @FXML
    private void onCheckPassword(MouseEvent event) throws IOException, ParseException, org.json.simple.parser.ParseException, JSONException {

        JSONObject resultAuth = UserServices.getPlaylistOps().auth(cpw.getName(), checkPasswordInput.getText(), wdv.getUser());

        if (resultAuth.get("error").toString().equals("true")) {
            this.checkPasswordInput.setText("");
            this.checkPasswordInput.setPromptText(resultAuth.get("message").toString());
        }
        else {

            this.stage.close();
            Image image = new Image("file:" + PLAYLIST_PICTURES_DIRECTORY_PATH + FILE_SEPARATOR + cpw.getResult().get("src_image"));

            final Playlist playlist = new Playlist(UserServices.getUserOp().getUserById(Integer.parseInt(getStringJsonObject(cpw.getResult().get("user_id")))),
                    image,
                    getStringJsonObject(cpw.getResult().get("name")),
                    true);

            final List<Track> listTrack = JsonObjectListTrack(cpw.getResult(), playlist.getPlaylistName());

            playlist.addTrack(listTrack);
            savePlaylistToDirectory(playlist);

            Task task = new Task<Boolean>() {
                {
                    this.setOnSucceeded(t -> {

                        pll.addPlaylist(playlist);
                        vcl.setMessageUpload(true);
                        vcl.setMessage("La playlist " + playlist.getPlaylistName() + " à été importé");
                    });
                }

                @Override
                public Boolean call() throws IOException {
                    saveToLocalTrack(listTrack, playlist);
                    saveToLocalImage(playlist);
                    return true;
                }
            };

            vcl.setMessageUpload(true);
            vcl.setMessage("Synchronisation en cours ...");
            new Thread(task).start();
        }
    }
}
