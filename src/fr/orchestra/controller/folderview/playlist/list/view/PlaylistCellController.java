package fr.orchestra.controller.folderview.playlist.list.view;


import org.json.JSONException;
import org.json.JSONObject;
import fr.orchestra.api.UserServices;
import fr.orchestra.api.interfaces.PlaylistOperations;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.folderview.view.middle.MusicsPlaylistList;
import fr.orchestra.model.Playlist;
import fr.orchestra.model.Track;
import fr.orchestra.utils.properties.ConstProperties;
import fr.orchestra.utils.system.ConfSys;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;

import static fr.orchestra.utils.Utils.deleteDirectory;


public class PlaylistCellController implements ConstProperties, ConfSys {

    private final ObjectProperty<Playlist> playlist = new SimpleObjectProperty<>(this, "playlist");

    @FXML
    private ImageView iconPlaylist;
    @FXML
    private Label playlistName;

    private MusicsPlaylistList mp;
    private WindowView windowView = WindowView.getInstance();

    @FXML
    private void loadPlaylistInView(MouseEvent event) {

        mp = new MusicsPlaylistList(getPlaylist());
        windowView.setMiddleViewContentList(mp);
        mp.printView();
        event.consume();
    }

    // @TODO Save path -> User Utilization Data
    @FXML
    private void switchPicture(MouseEvent event) {

        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        File imageFile = fc.showOpenDialog(null);

        if (imageFile != null && imageFile.exists()) {
            Image img = new Image(imageFile.toURI().toString());

            Playlist playlist = getPlaylist();
            playlist.setImagePlaylist(img);

            setPlaylist(playlist);
        }
        event.consume();
    }

    @FXML
    private void deletePlaylist(MouseEvent event) throws IOException, ParseException, JSONException {
        PlaylistOperations plOps = UserServices.getPlaylistOps();

        // delete directory of playlist
        deleteDirectory(PLAYLIST_DIRECTORY_PATH + FILE_SEPARATOR +
                WindowView.getInstance().getUser().getLogin() + FILE_SEPARATOR +
                getPlaylist().getPlaylistName());

        // First case : User is connected and the playlist belongs to him
        if (!plOps.checkAuthorizationForPlaylist(getPlaylist().getPlaylistName(), windowView.getUser()).get("message").toString().equals("Vous ne disposez pas des droits nécessairent")) {

            for (Track track : getPlaylist().getTrackList()) {

                String fileFolder = track.getFileFolder();
                String trackName = track.getName();
                String ext = fileFolder.substring(fileFolder.lastIndexOf('.') + 1, fileFolder.length());
                String srcTracks = track.getArtist() + "_" + trackName + "." + ext;
                srcTracks = srcTracks.replaceAll(" ", "_");

                JSONObject trackBdd = (JSONObject) UserServices.getTrackOps().getTrackBySrcTracks(srcTracks).get("track");
                int idTrack = Integer.parseInt(trackBdd.get("id").toString());

                JSONObject playlist = (JSONObject) plOps.getPlaylistByName(mp.getPlaylist().getPlaylistName()).get("playlist");
                int idPlaylist = Integer.parseInt(playlist.get("id").toString());

                plOps.deleteLinkBetweenPlaylistAndTrack(idPlaylist, idTrack, WindowView.getInstance().getUser());
            }

            JSONObject result = plOps.deletePlaylist(getPlaylist(), WindowView.getInstance().getUser());

            if (result.get("error").toString().equals("false")) {

                windowView.getController().setMessageUpload(true);
                windowView.getController().setMessage("Suppression réussit");
                windowView.getPlaylistList().removePlaylist(getPlaylist());
                unbindDisplayName();
                unbindImage();
                mp = null;
                playlist.setValue(null);
                event.consume();
            } else {
                windowView.getController().setMessageUpload(false);
                windowView.getController().setMessage("Erreur lors de la suppression");
            }
        }
        // The owner of the playlist is not the current user
        else {

            windowView.getController().setMessageUpload(true);
            windowView.getController().setMessage("Suppression réussit");
            windowView.getPlaylistList().removePlaylist(getPlaylist());
            unbindDisplayName();
            unbindImage();
            mp = null;
            playlist.setValue(null);
            event.consume();
        }
    }

    public Playlist getPlaylist() {
        return playlist.get();
    }

    /**
     * If the playlist is upgraded, change the name and the picture
     */
    public void setPlaylist(Playlist playlist) {

        this.playlist.set(playlist);
        if (playlist == null) {
            if (this.playlist.get() != null) {
                unbindDisplayName();
                unbindImage();
            }
        }
        else {
            bindDisplayName();
            if (playlist.getImagePlaylist() != null) bindImage();
        }
    }

    public ObjectProperty<Playlist> playlistProperty() {
        return playlist;
    }

    private void bindDisplayName() { playlistName.textProperty().bind(Bindings.selectString(playlist, "playlistName")); }

    private void unbindDisplayName() {
        playlistName.textProperty().unbind();
    }

    private void bindImage() {
        iconPlaylist.imageProperty().bind(Bindings.select(playlist, "imagePlaylist"));
    }

    private void unbindImage() {
        iconPlaylist.imageProperty().unbind();
    }
}