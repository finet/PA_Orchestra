package fr.orchestra.controller.folderview;


import com.github.openjson.JSONException;
import fr.orchestra.api.UserServices;
import fr.orchestra.api.interfaces.ConstApi;
import fr.orchestra.api.interfaces.TrackOperations;
import fr.orchestra.controller.folderview.playlist.list.view.PlaylistList;
import fr.orchestra.controller.folderview.playlist.newplaylist.window.NewPlaylistWindow;
import fr.orchestra.controller.folderview.plugin.DeletePluginListView;
import fr.orchestra.controller.folderview.tree.folder.ItemEntity;
import fr.orchestra.controller.folderview.view.middle.MusicsDownloadList;
import fr.orchestra.controller.folderview.view.middle.MusicsResearchList;
import fr.orchestra.controller.folderview.view.middle.template.AbstractMusicsList;
import fr.orchestra.controller.folderview.view.right.WaitingList;
import fr.orchestra.controller.mediaplayer.PlayerWindow;
import fr.orchestra.controller.mediaplayer.player.PlayerManager;
import fr.orchestra.model.Playlist;
import fr.orchestra.model.Track;
import fr.orchestra.utils.PlaylistDirectory;
import fr.orchestra.utils.plugin.PluginLoader;
import fr.orchestra.utils.properties.ConstProperties;
import fr.orchestra.view.AudioPlayerPictures;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import javax.management.InstanceAlreadyExistsException;
import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static fr.orchestra.model.Track.setTrack;
import static fr.orchestra.utils.Utils.updateCurrentTime;


public class ViewController implements Initializable, ConstApi, ConstProperties {

    private static URL fxmlUrlPluginView;
    public int allTrackUploadStatus;
    @FXML
    Label currentTrackLabel;
    private Background uploadRacineDisable;
    private Background uploadRacineDisableHover;
    private Background uploadRacineEnableHover;
    private Background uploadRacineEnable;
    private Background playIcon;
    private Background playIconHover;
    private Background pauseIcon;
    private Background pauseIconHover;
    private Background openPlayerIcon;
    private Background openPlayerIconHover;
    private Background openPlayerIconEnable;
    private Background openPlayerIconEnableHover;
    private boolean playStatus = false;
    private boolean openPlayerStatus = false;
    private boolean repeat_status = false;
    private Font digital_font;
    private PlayerManager playerManager;
    private WindowView windowView;
    private PlayerWindow playerWindow;
    private Stage stage;
    private ToolBarMenu rtb;
    private RepoMenuButton rmb;
    private AbstractMusicsList pwa;
    private PlaylistList pll;
    private boolean uploadLocalFilesButtonStatus;
    private DeletePluginListView deletePluginView;
    private List<Track> trackList = new ArrayList<>();
    private List<URL> jarFiles = new ArrayList<>();
    private List<URL> jarRemoveFiles = new ArrayList<>();
    private List<Track> trackDownloadList = new ArrayList<>();
    private StringProperty message = new SimpleStringProperty("");

    @FXML
    private AnchorPane mainPane;
    @FXML
    private AnchorPane printWindowAnchorPane;
    @FXML
    private Button exitButton;
    @FXML
    private Button reduceButton;
    @FXML
    private Button maximizeButton;
    @FXML
    private Button openPlayerBtn;
    @FXML
    private MenuButton repoMenuButton;
    @FXML
    private TreeView<ItemEntity> treeView;
    @FXML
    private ListView<Playlist> listPlaylist;
    @FXML
    private Button uploadLocalFilesButton;
    @FXML
    private Label resultUploadLabel;
    @FXML
    private TextField inputResearch;
    @FXML
    private ToggleButton playBtn;
    @FXML
    private Label totalDurationLabel;
    @FXML
    private Slider seekSlider;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label currentDurationLabel;
    @FXML
    private Label playlistName;
    @FXML
    private Label playlistQueueDuration;
    @FXML
    private Label trackNumber;
    @FXML
    private TableView<Track> tableTracks;
    @FXML
    private TableColumn<Track, String> columnName;
    @FXML
    private TableColumn<Track, String> columnArtist;
    @FXML
    private TableColumn<Track, String> columnGenre;
    @FXML
    private TableColumn<Track, String> columnAlbum;
    @FXML
    private TableColumn<Track, String> columnDuration;
    @FXML
    private TableColumn<Track, Button> columnDelete;

    /**
     * GETTER SETTER
     **/
    public Label getTotalDurationLabel() {
        return totalDurationLabel;
    }

    public Label getCurrentDurationLabel() {
        return currentDurationLabel;
    }

    public Slider getSeekSlider() {
        return seekSlider;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setRepeat_status(boolean status) {
        this.repeat_status = status;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public WindowView getWindowView() {
        return windowView;
    }

    public void setWindowView(WindowView windowView) {
        this.windowView = windowView;
    }

    public PlayerWindow getPlayerWindow() {
        return playerWindow;
    }

    public void setPlayerWindow(PlayerWindow playerWindow) {
        this.playerWindow = playerWindow;
    }

    public ListView getListPlaylist() {
        return listPlaylist;
    }

    public AnchorPane getPrintWindowAnchorPane() {
        return printWindowAnchorPane;
    }

    public void setPrintWindowAnchorPane(AnchorPane printWindowAnchorPane) {

        this.printWindowAnchorPane.getChildren().setAll(printWindowAnchorPane.getChildren());
        this.printWindowAnchorPane.setVisible(true);
    }

    public RepoMenuButton getRbm() {
        return rmb;
    }

    public Label getCurrentTrackLabel() {
        return currentTrackLabel;
    }

    public List<URL> getJarFiles() {
        return jarFiles;
    }

    public List<URL> getJarRemoveFiles() {
        return jarRemoveFiles;
    }

    public void setTrackDownload(Track trackDownload) {
        this.trackDownloadList.add(trackDownload);
    }

    public void setResultUploadLabel(String resultUploadLabel) {
        this.resultUploadLabel.setText(resultUploadLabel);
    }

    public void setMessage(String message) {
        this.message.set(message);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        playerManager = PlayerManager.getInstance();
        playerManager.setFolderController(this);

        initPlaylist();
        loadIcons();
        loadFonts();
        addEventOnBtn();
        playBtn.setBackground(playIcon);
        openPlayerBtn.setBackground(openPlayerIcon);
        uploadLocalFilesButton.setBackground(uploadRacineDisable);
        resultUploadLabel.textProperty().bind(message);

        rtb = new ToolBarMenu(this, exitButton, reduceButton, maximizeButton);
        rmb = new RepoMenuButton(repoMenuButton, treeView, this);
        pll = new PlaylistList(listPlaylist);

        if (WaitingList.getInstance() == null) {
            try {
                WaitingList.createInstance(trackNumber, playlistQueueDuration, tableTracks, columnName, columnArtist, columnGenre, columnAlbum, columnDuration, columnDelete);
            } catch (InstanceAlreadyExistsException e) {
                e.printStackTrace();
            }
        }

        rtb.setMovableWindowBy(mainPane);

        for (URL urlJar : PluginLoader.retrieveExistingPlugin()) {
            if (PluginLoader.loadPlugin(urlJar)) jarFiles.add(urlJar);
        }
    }

    /**
     * create racine of playlist and load each playlist
     */
    private void initPlaylist() {

        Path path = Paths.get(PLAYLIST_DIRECTORY_PATH);
        String userLogin = WindowView.getInstance().getUser().getLogin();

        // create initial playlist directory if doesn't exit
        if (Files.notExists(path)) {
            try {
                Files.createDirectory(Paths.get(PLAYLIST_DIRECTORY_PATH));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // create directory of user if doesn't exit
        if (Files.notExists(Paths.get(path + "/" + userLogin))) {
            try {
                Files.createDirectory(Paths.get(PLAYLIST_DIRECTORY_PATH + "/" + userLogin));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // create directory of pictures if doesn't exit
        if (Files.notExists(Paths.get(PLAYLIST_PICTURES_DIRECTORY_PATH))) {
            try {
                Files.createDirectory(Paths.get(PLAYLIST_PICTURES_DIRECTORY_PATH));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // load and initialize all playlist and track of user on application
        else {
            List<Playlist> playlistList = loadEachPlaylist();
            listPlaylist.getItems().addAll(playlistList);
        }
    }

    private List<Playlist> loadEachPlaylist() {
        PlaylistDirectory playlistDirectory = new PlaylistDirectory(PLAYLIST_DIRECTORY_PATH, true);
        return playlistDirectory.load();
    }

    @FXML
    private void runResearch() {

        if (inputResearch.getText().length() < 3) {
            setMessageUpload(false);
            message.setValue("Minimum 3 caractère pour une recherche");
        } else {
            List<Track> trackList = new ArrayList<>();
            List<ItemEntity.Directory> directories = new ArrayList<>();
            List<Playlist> playlistList;

            // call api
            try {
                TrackOperations tkOp = UserServices.getTrackOps();
                trackList.addAll(tkOp.getTrackResearch(1, inputResearch.getText())); // by title
                trackList.addAll(tkOp.getTrackResearch(2, inputResearch.getText())); // by artist
                trackList.addAll(tkOp.getTrackResearch(3, inputResearch.getText())); // by alum
                playlistList = new ArrayList<>(UserServices.getPlaylistOps().getPlaylistResearch(inputResearch.getText()));

                MusicsResearchList mrl = new MusicsResearchList(inputResearch.getText(), trackList.toArray(new Track[0]), directories, playlistList);
                mrl.printView();

            } catch (IOException | JSONException | org.json.JSONException e) {
                e.printStackTrace();
            }

            setMessageUpload(true);
            message.setValue("");
        }
    }

    @FXML
    private void logout() {

        playerWindow.getController().logout();
        WindowView.getInstance().setWinStatus(false);
    }

    @FXML
    private void myAccountOnWeb() throws IOException {

        Desktop.getDesktop().browse(java.net.URI.create(MY_ACCOUNT));
    }

    @FXML
    private void openPlayer() {

        playerWindow.openWindow();
        stage.hide();
    }

    @FXML
    private void newPlaylist() throws IOException {

        new NewPlaylistWindow();
    }

    /**
     * Open the view where download tracks are stored
     */
    @FXML
    private void openViewTrackDownload() {

        MusicsDownloadList mdl = new MusicsDownloadList(trackDownloadList.toArray(new Track[0]));
        mdl.printView();
    }

    /*****************************************************************************************
     * Plugin adding and deleting event
     */

    @FXML
    private void addPlugin() {

        URL jarURL = PluginLoader.openAndLoadPlugin(jarFiles);

        if (jarURL != null && !jarFiles.contains(jarURL)) {
            jarFiles.add(jarURL);
            PluginLoader.storePluginJarFile(jarURL);
        }
    }

    @FXML
    private void delPlugin() {

        try {
            deletePluginView = new DeletePluginListView();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*****************************************************************************************
     * @Job : Gestion of playing Track
     */
    @FXML
    public void play() {

        playerManager.play();
    }

    @FXML
    private void next() {

        if (repeat_status)
            playerManager.reload();
        else {
            playerManager.setTrackNumber(playerManager.getTrackNumber() + 1);
            playerManager.runTrack();
        }
    }

    @FXML
    private void previous() {

        if (repeat_status)
            playerManager.reload();
        else {
            playerManager.setTrackNumber(playerManager.getTrackNumber() - 1);
            playerManager.runTrack();
        }
    }

    /**
     * Track to upload
     */
    @FXML
    private void uploadLocalFiles() {

        uploadLocalFilesButtonStatus = true;

        Task task = new Task<Void>() {
            @Override
            public Void call() throws org.json.JSONException {

                String tmpMsg = "Aucun Fichier local chargé";
                allTrackUploadStatus = -1;

                if (windowView.getTreeViewFolder() == null)
                    setMessageUpload(false);

                else {

                    /* Update interface on the JavaFX Application Thread */
                    Platform.runLater(() -> message.setValue("Synchronisation en cours ..."));

                    setAllTracksToUpload(windowView.getTreeViewFolder().getRootDir());

                    for (Track track : trackList) {
                        if (!UserServices.getTrackOps().createNewTracks(track, windowView.getUser()).get("error").toString().equals("false"))
                            allTrackUploadStatus(false);
                        else
                            allTrackUploadStatus(true);
                    }
                    if (allTrackUploadStatus == 1) {
                        setMessageUpload(true);
                        tmpMsg = "Arborescence upload";
                    } else if (allTrackUploadStatus == 0) {
                        setMessageUpload(true);
                        tmpMsg = "Arborescence upload (Certaines musiques ne peuvent pas être partagé";
                    } else {
                        setMessageUpload(false);
                        tmpMsg = "Aucune musique upload";
                    }
                }

                /* Update interface on the JavaFX Application Thread */
                final String finalTmpMsg = tmpMsg;
                Platform.runLater(() -> message.setValue(finalTmpMsg));

                uploadLocalFilesButton.setBackground(uploadRacineDisable);
                uploadLocalFilesButtonStatus = false;
                return null;
            }
        };

        /* start Task on new Thread */
        new Thread(task).start();
    }

    private void setAllTracksToUpload(ItemEntity.Directory rootDir) {

        // get tracks
        for (int cpt = 0; cpt < rootDir.getChildrenMusic().size(); cpt++) {
            Track track = setTrack(rootDir.getChildrenMusic().get(cpt).getFile());

            if (Track.checkTracks(track, this.trackList))
                trackList.add(track);
            else
                allTrackUploadStatus(false);
        }

        // get dir
        for (int cpt = 0; cpt < rootDir.getChildrenDir().size(); cpt++) {
            setAllTracksToUpload(rootDir.getChildrenDir().get(cpt));
        }
    }

    public void setMessageUpload(boolean status) {

        resultUploadLabel.setStyle(status ? "-fx-text-fill:  green" : "-fx-text-fill:  red");
    }

    // @TODO use this function :)
    public void displayMessageUploadList() {

        if (allTrackUploadStatus == 1) {
            setMessageUpload(true);
            message.setValue("Arborescence upload");
        } else if (allTrackUploadStatus == 0) {
            setMessageUpload(true);
            message.setValue("\"Arborescence upload (Certaines musiques ne peuvent pas être partagé)");
        } else setMessageUpload(false);
    }

    public void allTrackUploadStatus(boolean value) {

        if (!value && allTrackUploadStatus == -1) return;
        allTrackUploadStatus = (value && this.allTrackUploadStatus != 0) ? 1 : 0;
    }

    public void updatePlayer() {

        String audioLabel = playerWindow.getController().getNameTrackLabel().getText() + " - " + playerWindow.getController().getArtistTrackLabel().getText();
        WindowView.getInstance().getController().getCurrentTrackLabel().setText(audioLabel);
        currentDurationLabel.setText(updateCurrentTime(playerManager.getCurrentTrackPlayer().getCurrentTime()));
    }

    private void loadFonts() {

        digital_font = Font.loadFont(AudioPlayerPictures.getFontTTF(), 10);
    }

    /******************************************************************************************
     * Add event on buttons for gestion of icons / iconsHover
     */
    private void addEventOnBtn() {

        playBtn.addEventHandler(MouseEvent.MOUSE_ENTERED,
                event -> playBtn.setBackground((playStatus) ? pauseIconHover : playIconHover)
        );
        playBtn.addEventHandler(MouseEvent.MOUSE_EXITED,
                event -> playBtn.setBackground((playStatus) ? pauseIcon : playIcon)
        );

        uploadLocalFilesButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
                event -> uploadLocalFilesButton.setBackground((uploadLocalFilesButtonStatus) ? uploadRacineEnableHover : uploadRacineDisableHover)
        );
        uploadLocalFilesButton.addEventHandler(MouseEvent.MOUSE_EXITED,
                event -> uploadLocalFilesButton.setBackground((uploadLocalFilesButtonStatus) ? uploadRacineEnable : uploadRacineDisable)
        );

        openPlayerBtn.addEventHandler(MouseEvent.MOUSE_ENTERED,
                event -> openPlayerBtn.setBackground((openPlayerStatus) ? openPlayerIconEnableHover : openPlayerIconHover)
        );
        openPlayerBtn.addEventHandler(MouseEvent.MOUSE_EXITED,
                event -> openPlayerBtn.setBackground((openPlayerStatus) ? openPlayerIconEnable : openPlayerIcon)
        );
    }

    // @TODO use this function :)
    public void setIconUploadBtn(boolean uploadLocalFilesButtonStatus) {

        this.uploadLocalFilesButtonStatus = uploadLocalFilesButtonStatus;
        uploadLocalFilesButton.setBackground((uploadLocalFilesButtonStatus) ? uploadRacineDisableHover : uploadRacineEnableHover);
    }

    public void setIconPlayBtn(boolean playStatus) {

        this.playStatus = playStatus;
        playBtn.setBackground((playStatus) ? playIcon : playIconHover);
    }

    private void loadIcons() {

        BackgroundImage p1 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerPlayPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage p1h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerPlayHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage p2 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerPausePictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage p2h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerPauseHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage o1 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerOpenFolderPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage o1h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerOpenFolderHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage o2 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerOpenFolderEnablePictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage o2h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerOpenFolderEnableHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage u1 = new BackgroundImage(AudioPlayerPictures.getFolderViewUploadRacineDisable(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage u1h = new BackgroundImage(AudioPlayerPictures.getFolderViewUploadRacineDisableHover(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage u2 = new BackgroundImage(AudioPlayerPictures.getFolderViewUploadRacineEnable(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage u2h = new BackgroundImage(AudioPlayerPictures.getFolderViewUploadRacineEnableHover(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        this.uploadRacineDisable = new Background(u1);
        this.uploadRacineDisableHover = new Background(u1h);
        this.uploadRacineEnable = new Background(u2);
        this.uploadRacineEnableHover = new Background(u2h);

        this.playIcon = new Background(p1);
        this.playIconHover = new Background(p1h);
        this.pauseIcon = new Background(p2);
        this.pauseIconHover = new Background(p2h);

        this.openPlayerIcon = new Background(o1);
        this.openPlayerIconHover = new Background(o1h);
        this.openPlayerIconEnable = new Background(o2);
        this.openPlayerIconEnableHover = new Background(o2h);
    }
}
