package fr.orchestra.controller.login;


import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ToolBar;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;


public class WindowLog {

    private static Parent root;
    private static Scene scene;
    private static Stage primaryStage;
    /**
     * Instance unique pré-initialisée
     */
    private static WindowLog INSTANCE = new WindowLog();
    private double xOffset = 0;
    private double yOffset = 0;

    /**
     * Point d'accès pour l'instance unique du singleton
     */
    public final static WindowLog getInstance() {
        return INSTANCE;
    }

    /***************************************************************************/

    public void setMovableWindowBy(ToolBar toolBar) {
        toolBar.setOnMousePressed(this::getPosWindow);
        toolBar.setOnMouseDragged(this::setPosWindow);
    }

    private void getPosWindow(MouseEvent mouseEvent) {
        xOffset = primaryStage.getX() - mouseEvent.getScreenX();
        yOffset = primaryStage.getY() - mouseEvent.getScreenY();
    }

    private void setPosWindow(MouseEvent mouseEvent) {
        primaryStage.setY(mouseEvent.getScreenY() + yOffset);
        primaryStage.setX(mouseEvent.getScreenX() + xOffset);
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        WindowLog.scene = scene;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setPrimaryStage(Stage primaryStage) {
        WindowLog.primaryStage = primaryStage;
    }

    public void setRoot(Parent root) {
        WindowLog.root = root;
    }

    public void showWindow() {
        primaryStage.show();
    }

}
