package fr.orchestra.controller.login;


import fr.orchestra.api.UserServices;
import fr.orchestra.api.interfaces.ConstApi;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.mediaplayer.PlayerWindow;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static fr.orchestra.utils.properties.ReadProperties.readProperLog;
import static fr.orchestra.utils.properties.ReadProperties.readProperMemorizeLog;
import static fr.orchestra.utils.properties.WriteProperties.writeProperLog;
import static java.net.URI.create;


public class LogController implements Initializable, ConstApi {

    @FXML
    CheckBox memorizeLog;
    @FXML
    Label errorLabel;
    @FXML
    BorderPane borderPane;
    @FXML
    ToolBar toolBar;
    @FXML
    PasswordField pwdInput;
    @FXML
    TextField emailInput;
    @FXML
    Button close;
    @FXML
    Button minimize;
    @FXML
    Button maximize;

    private WindowLog windowLog = WindowLog.getInstance();
    private WindowView windowView = WindowView.getInstance();
    private boolean windowMinimize = false;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        windowLog.setMovableWindowBy(toolBar);

        // auto connexion if checkbox "remember me" checked
        if (readProperMemorizeLog()) {

            String[] log = readProperLog();
            emailInput.setText(log[0]);
            pwdInput.setText(log[1]);
            memorizeLog.setSelected(true);
        }
    }

    @FXML
    private void login() {

        if (!UserServices.getAuthOp().auth(emailInput.getText(), pwdInput.getText())) {
            errorLabel.setText("Pseudo ou mot de passe incorrect");
            pwdInput.clear();
        }
        else {
            connexion();
            // write login password and bool stayConnexion for auto connexion
            writeProperLog(emailInput.getText(), pwdInput.getText(), memorizeLog.isSelected());
            if (!memorizeLog.isSelected()) pwdInput.clear();
        }
    }

    private void connexion() {

        PlayerWindow playerWindow = null;

        try {
            playerWindow = new PlayerWindow(windowLog.getPrimaryStage());
        } catch (IOException e) {
            e.printStackTrace();
        }

        windowView.LoadWindowView(playerWindow);
        windowLog.getPrimaryStage().hide();
        errorLabel.setText("");
    }

    @FXML
    private void subscribeOnWeb() throws IOException {

        Desktop.getDesktop().browse(create(SUBSCRIBE));
    }

    @FXML
    private void resetPwdOnWeb() throws IOException {

        Desktop.getDesktop().browse(create(RESET_PWD));
    }

    @FXML
    private void close() {

        writeProperLog(emailInput.getText(), pwdInput.getText(), memorizeLog.isSelected());

        Platform.exit();
    }

    @FXML
    private void minimize() {

        windowLog.getPrimaryStage().setIconified(true);
        windowMinimize = !windowMinimize;
    }


    @FXML
    private void maximize() {

        windowLog.getPrimaryStage().setMaximized(!windowLog.getPrimaryStage().isMaximized());
    }
}
