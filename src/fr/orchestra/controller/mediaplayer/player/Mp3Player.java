package fr.orchestra.controller.mediaplayer.player;


import fr.orchestra.plugin.templates.TrackPlayer;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.media.Media;
import javafx.scene.media.MediaException;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import javafx.util.Duration;

import java.io.File;


/**
 * AudioPlayer mp3 / wav / mp4 implements interface TrackPlayer
 */
public class Mp3Player implements TrackPlayer {

    private MediaPlayer mediaPlayer;
    private Runnable endOfMediaAction;
    //private Track track;

    @Override
    public Status getStatus() {
        return mediaPlayer == null ? Status.UNKNOWN : mediaPlayer.getStatus();
    }

    @Override
    public Media getMedia() {
        return this.mediaPlayer.getMedia();
    }

    @Override
    public void seek(Duration seekTime) {
        mediaPlayer.seek(seekTime);
    }

    @Override
    public void play() {
        mediaPlayer.play();
    }

    @Override
    public void pause() {
        mediaPlayer.pause();
    }

    @Override
    public void stop() {
        mediaPlayer.stop();
    }

    @Override
    public Duration getStopTime() {
        return mediaPlayer.getStopTime();
    }

    @Override
    public double getDoubleCurrentTime() {
        return (mediaPlayer.getCurrentTime().toSeconds());
    }

    @Override
    public double getDoubleCurrentTimeMin() {
        return (mediaPlayer.getCurrentTime().toMinutes());
    }

    @Override
    public Duration getCurrentTime() {
        return mediaPlayer.getCurrentTime();
    }

    @Override
    public ReadOnlyObjectProperty<Status> statusProperty() {
        return mediaPlayer.statusProperty();
    }

    @Override
    public ReadOnlyObjectProperty<Duration> currentTimeProperty() {
        return mediaPlayer.currentTimeProperty();
    }

    @Override
    public void setOnEndOfMedia(Runnable value) {

        endOfMediaAction = value;
        if (mediaPlayer != null)
            mediaPlayer.setOnEndOfMedia(value);
    }

    /**
     * VOLUME
     */
    @Override
    public void setVolume(double value) {

        if (mediaPlayer != null) {
            if (value < 0)
                mediaPlayer.setVolume(0.0);
            else
                mediaPlayer.setVolume(value);
        }
    }

    @Override
    public DoubleProperty volumeProperty() {

        if (mediaPlayer == null)
            return new SimpleDoubleProperty(1.0);
        else
            return mediaPlayer.volumeProperty();
    }

    /**
     * TRACK
     */

    @Override
    public void setTrack(String trackPath) throws MediaException {

        if (mediaPlayer != null) mediaPlayer.dispose();

        /* WARNING : Some system require dependency to read some format (MP3, ACC)
         *  |--> See more at http://www.oracle.com/technetwork/java/javase/downloads/index.html
         * */
        Media media = new Media((new File(trackPath)).toURI().toString());
        mediaPlayer = new MediaPlayer(media);
    }

    @Override
    public Duration getTotalDuration() {
        return mediaPlayer.getTotalDuration();
    }

    @Override
    public void setOnReady(Runnable runnable) {
        mediaPlayer.setOnReady(runnable);
    }

    @Override
    public String[] getTrackFormat() {
        return new String[]{"mp3", "mp4"/*, "wav"*/};
    }
}
