package fr.orchestra.controller.mediaplayer.player;


import fr.orchestra.controller.folderview.ViewController;
import fr.orchestra.controller.mediaplayer.PlayerController;
import fr.orchestra.model.Track;
import fr.orchestra.plugin.templates.TrackPlayer;
import fr.orchestra.utils.format.TrackFormat;
import fr.orchestra.utils.metadata.MetadataFactory;
import fr.orchestra.utils.metadata.MetadataMp3;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.media.MediaException;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.io.IOException;
import java.util.*;

import static fr.orchestra.utils.Utils.updateCurrentTime;
import static javafx.scene.media.MediaPlayer.Status.*;


/**
 * Singleton audio player
 * <p>
 * lecture de playlist :
 * <p>
 * playerQueue --> playlist que joue le player (représente la playlist gauche dans le folder)
 * rightPlaylist --> playlist que joue le player (représente la playlist droite dans le folder)
 * |
 * --> il faut que ce soit une liste observalble car beacoup de changement possible
 * avec le drag and drop (ajout, supréssion)
 * <p>
 * DisplayPlaylist --> playlist qui est afficher actuellment dans le folder (que pour la partie gauche)
 * <p>
 * ##	ouverture de dossier,
 * <p>
 * --> on affiche dans la partie gauche et on initialise DisplayPlaylist avec des new Track()
 * et un incrément int croissant (trackNumber)
 * <p>
 * --> on initialise playerQueue avec les Tracks si null
 * <p>
 * <p>
 * ##	si click sur un track aléatoire dans la partie gauche,
 * <p>
 * --> si DisplayPlaylist et playerQueue son égales, (voir comment on gere ça, si on rajoute une ligne avec un boolemn par exemple)
 * <p>
 * - on récupere l'index dans DisplayPlaylist et on joue le track dans le lecteur avec playerQueue
 * - on change la valeur de trackNumber dans le lecteur audio pour qui joue la suivante
 * <p>
 * sinon
 * <p>
 * - on clear() la playerQueue et on l'initialise avec la DisplayPlaylist actuelle
 * - on récupere l'index dans DisplayPlaylist et on joue le track dans le lecteur
 * - on change la valeur de trackNumber dans le lecteur audio pour qui joue la suivante
 * <p>
 * ## 	si drag&drop dans la partie droite
 * <p>
 * --> si premier,
 * <p>
 * - on initialise rightPlaylist
 * <p>
 * sinon
 * <p>
 * - c'est du rightPlaylist.add() pour les suivants
 * <p>
 * --> si delete
 * <p>
 * - c'est rightPlaylist.remove()
 * <p>
 * --> button clearRightPlaylist
 * - on rightPlaylist.clear()
 */

public class PlayerManager {

    private final static boolean PLAY = true;
    private final static boolean PAUSE = false;
    /**
     * Instance unique pré-initialisée
     */
    private static PlayerManager INSTANCE = new PlayerManager();
    private TrackPlayer currentTrackPlayer;
    private List<String> currentTrackFormatAllowed = new ArrayList<>();
    private List<TrackPlayer> trackPlayerList = new ArrayList<>();
    private Map<TrackPlayer, List<String>> trackPlayerListFormat = new HashMap<>();
    private PlayerController playerController;
    private ViewController folderController;
    private List<Track> playerQueue;
    private Optional<Track> currentTrack;
    private int trackNumber = 0;
    private boolean playingRandom;
    private boolean playingRepeat;
    private boolean playingLoop;
    private boolean isInitialized = false;
    private ToggleButton playBtn_player;
    private ToggleButton playBtn_folder;
    private Slider volumeSlider;
    private Slider seekSlider_player;
    private Slider seekSlider_folder;
    private ProgressBar progressBar_player;
    private ProgressBar progressBar_folder;
    private Label currentDurationLabel_player;
    private Label currentDurationLabel_folder;
    private Label totalDurationLabel_player;
    private Label totalDurationLabel_folder;

    private PlayerManager() {

        Mp3Player mp3Player = new Mp3Player();
        addTrackPlayer(mp3Player);
        MetadataFactory.getInstance().addMetadataReader(mp3Player.getTrackFormat(), new MetadataMp3());

        /**
         * This must be launch by introspection (PLUGIN)
         *
         WavPlayer wavPlayer = new WavPlayer();
         addTrackPlayer(wavPlayer);
         MetadataFactory.getInstance().addMetadataReader(wavPlayer.getTrackFormat(), new MetadataWav());*/

        playingRandom = false;
        playingRepeat = false;
        playingLoop = false;

        this.currentTrack = Optional.empty();
        this.playerQueue = new ArrayList<>();
    }

    /**
     * Point d'accès pour l'instance unique du singleton
     */
    public final static PlayerManager getInstance() {
        return INSTANCE;
    }

    public void addTrackPlayer(TrackPlayer trackPlayer) {
        if (trackPlayerList.contains(trackPlayer)) return;

        trackPlayerListFormat.put(trackPlayer, TrackFormat.getInstance().storeFormatsRegex(trackPlayer.getTrackFormat()));

        trackPlayerList.add(trackPlayer);
    }

    public void removeTrackPlayer(TrackPlayer trackPlayer) {
        if (!trackPlayerList.contains(trackPlayer)) return;

        trackPlayerListFormat.remove(trackPlayer);
        trackPlayerList.remove(trackPlayer);
        if (currentTrackPlayer.equals(trackPlayer)) currentTrackPlayer = null;

        // Remove format allowed
        TrackFormat.getInstance().removeFormatsRegex(trackPlayer.getTrackFormat());
    }

    public TrackPlayer getCurrentTrackPlayer() {
        return currentTrackPlayer;
    }

    public MediaPlayer.Status getPlayerStatus() {
        return currentTrackPlayer.getStatus();
    }

    public List<Track> getPlayerQueue() {
        return playerQueue;
    }

    public Optional<Track> getCurrentTrack() {
        return currentTrack;
    }

    /**
     * Initialise la Track gere la queue du lecteur en fonction des ses parametre :
     * - loop
     * - repeat
     * - random
     * - next
     * - previous
     * - trackNumber
     */
    private void setCurrentTrack(boolean playingRandom) {

        if (playingRandom) {

            int lastTrackNumber = trackNumber;
            trackNumber = (int) (Math.random() * playerQueue.size());

            /* -1 -> enleve l'incrementation de nextBtn */
            while (trackNumber == lastTrackNumber - 1) trackNumber = (int) (Math.random() * playerQueue.size());

            setPlayer(playerQueue.get(trackNumber));
        } else if (trackNumber <= 0) {
            setPlayer(playerQueue.get(0));
            trackNumber = 0;
        } else if (trackNumber >= playerQueue.size()) {

            if (playingLoop) {
                setPlayer(playerQueue.get(0));
                trackNumber = 0;
            } else {
                /* enleve l'incrémentation du playerController.nextBtn si fin de playlist && loop = false */
                trackNumber--;
            }
        } else setPlayer(playerQueue.get(trackNumber));
    }

    public int getTrackNumber() {
        return trackNumber;
    }

    public void setTrackNumber(int trackNumber) {
        this.trackNumber = trackNumber;
    }

    public void seek(Duration seekTime) {
        currentTrackPlayer.seek(seekTime);
    }

    public void setPlayingRepeat(boolean repeat) {
        this.playingRepeat = repeat;
    }

    public void setPlayingRandom(boolean random) {
        this.playingRandom = random;
    }

    public void setPlayingLoop(boolean playingLoop) {
        this.playingLoop = playingLoop;
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    /**
     * Initialise Les controllers du lecteur audio indépendant  et du folder  + leurs composants pour
     * une meilleur lisibilité du code
     */
    public void setFolderController(ViewController folderController) {

        this.folderController = folderController;
        this.playBtn_folder = playerController.getPlayBtn();
        this.seekSlider_folder = folderController.getSeekSlider();
        this.progressBar_folder = folderController.getProgressBar();
        this.currentDurationLabel_folder = folderController.getCurrentDurationLabel();
        this.totalDurationLabel_folder = folderController.getTotalDurationLabel();
    }

    public void setPlayerController(PlayerController playerController) {

        this.playerController = playerController;
        this.playBtn_player = playerController.getPlayBtn();
        this.seekSlider_player = playerController.getSeekSlider();
        this.progressBar_player = playerController.getProgressBar();
        this.currentDurationLabel_player = playerController.getCurrentDurationLabel();
        this.totalDurationLabel_player = playerController.getTotalDurationLabel();
        this.volumeSlider = playerController.getVolumeSlider();
    }

    /**
     * Vérifie si la Track est jouable avant d'initialiser la Track courrant et update le player
     */
    private boolean isPlayable(Track track) {

        boolean isPlayable = false;
        try {
            isPlayable = track.isPlayable();
        } catch (IOException exception) {
            String fullPath = track.getFileFolder();
        }

        return isPlayable;
    }

    public void playByRepertoryClick() {

        runTrack();
    }

    public void play() {
        if (!playerQueue.isEmpty()) {
            if (currentTrackPlayer.getStatus() != MediaPlayer.Status.PLAYING) {

                if (getCurrentTrack().isPresent())
                    resume();
                else
                    runTrack();
            } else
                pause();
        }
    }

    /**
     * Joue la Track et gère la fin du média avec la récursive
     */
    public void runTrack() {

        if (currentTrackPlayer == null ||
                currentTrackPlayer.getStatus().equals(STOPPED) ||
                currentTrackPlayer.getStatus().equals(PAUSED) ||
                currentTrackPlayer.getStatus().equals(UNKNOWN) ||
                currentTrackPlayer.getStatus().equals(PLAYING)) {

            if (!playerQueue.isEmpty()) {

                setCurrentTrack(playingRandom);
                playerController.setIconPlayBtn(PLAY);
                folderController.setIconPlayBtn(PLAY);
                currentTrackPlayer.play();


                currentTrackPlayer.setOnEndOfMedia(() -> {
                    currentTrackPlayer.stop();
                    if (!playingRepeat)
                        trackNumber++;
                    // Recursive
                    runTrack();
                });

            }
        } else stop();
    }

    /**
     * Initialise le le player, le track courrant et update l'affichage via le currentTrack
     */
    public void setPlayer(Track track) {

        isInitialized = true;

        if (setTrackPlayer(track) && isPlayable(track)) {
            try {
                String path = track.getFileFolder();
                path = path.replace("\\", "/");
                currentTrackPlayer.setTrack(path);
                currentTrack = Optional.of(track);
                playerController.updatePlayer(track);
                folderController.updatePlayer();
                updateTime();
                updateSeekSlider();
                AddEventOnSeekSlider();

            } catch (MediaException exception) {
                currentTrack = Optional.empty();
                exception.printStackTrace();
            }
        }
    }

    private void updateTime() {

        currentTrackPlayer.currentTimeProperty().addListener(ov -> updateSeekSlider());
        currentTrackPlayer.setOnReady(() -> {

            playerController.getTotalDurationLabel().setText(updateCurrentTime(currentTrackPlayer.getTotalDuration()));
            folderController.getTotalDurationLabel().setText(updateCurrentTime(currentTrackPlayer.getTotalDuration()));

            updateSeekSlider();
        });
    }

    private void updateSeekSlider() {

        Platform.runLater(() -> {

            Duration currentTime = currentTrackPlayer.getCurrentTime();
            currentDurationLabel_player.setText(updateCurrentTime(currentTrackPlayer.getCurrentTime()));
            currentDurationLabel_folder.setText(updateCurrentTime(currentTrackPlayer.getCurrentTime()));

            seekSlider_player.setDisable(currentTrackPlayer.getTotalDuration().isUnknown());
            seekSlider_folder.setDisable(currentTrackPlayer.getTotalDuration().isUnknown());

            if (currentTrackPlayer.getTotalDuration().greaterThan(Duration.ZERO)
                    && !seekSlider_folder.isDisabled()
                    && !seekSlider_player.isDisabled()
                    && !seekSlider_player.isValueChanging()
                    && !seekSlider_folder.isValueChanging()) {

                seekSlider_player.setValue(currentTime.divide(currentTrackPlayer.getTotalDuration()).toMillis() * 100.0);
                seekSlider_folder.setValue(currentTime.divide(currentTrackPlayer.getTotalDuration()).toMillis() * 100.0);
                progressBar_player.setProgress(currentTime.divide(currentTrackPlayer.getTotalDuration()).toMillis());
                progressBar_folder.setProgress(currentTime.divide(currentTrackPlayer.getTotalDuration()).toMillis());
            }
        });
    }

    private void AddEventOnSeekSlider() {

        seekSlider_player.valueProperty().addListener(ov -> {
            if (seekSlider_player.isValueChanging() || seekSlider_player.isPressed()) {
                // multiply duration by percentage calculated by slider position
                currentTrackPlayer.seek(currentTrackPlayer.getTotalDuration().multiply(seekSlider_player.getValue() / 100.0));
                eventSlider();
            }
        });

        seekSlider_folder.valueProperty().addListener(ov -> {
            if (seekSlider_folder.isValueChanging() || seekSlider_folder.isPressed()) {
                // multiply duration by percentage calculated by slider position
                currentTrackPlayer.seek(currentTrackPlayer.getTotalDuration().multiply(seekSlider_folder.getValue() / 100.0));
                eventSlider();
            }
        });
    }

    private void eventSlider() {

        progressBar_player.setProgress(currentTrackPlayer.getCurrentTime().divide(currentTrackPlayer.getTotalDuration()).toMillis());
        progressBar_folder.setProgress(currentTrackPlayer.getCurrentTime().divide(currentTrackPlayer.getTotalDuration()).toMillis());
    }

    public void stop() {

        currentTrackPlayer.stop();
        playerController.setIconPlayBtn(PAUSE);
        folderController.setIconPlayBtn(PAUSE);
        currentTrack = Optional.empty();
    }

    public void pause() {

        if (currentTrackPlayer.getStatus().equals(PLAYING)) {
            currentTrackPlayer.pause();
            playerController.setIconPlayBtn(PAUSE);
            folderController.setIconPlayBtn(PAUSE);
        }
    }

    public void resume() {

        currentTrackPlayer.play();
        playerController.setIconPlayBtn(PLAY);
        folderController.setIconPlayBtn(PLAY);
    }

    public void resetPlaylist() {

        playerQueue.clear();
    }

    public void reload() {

        seek(Duration.millis(0));
        runTrack();
    }

    /**
     * This function select the good TrackPlayer, to read a sound.
     *
     * @param track The track who must be read
     **/
    private boolean setTrackPlayer(Track track) {

        // Check if last track has the same format and if the current TrackReader allow the format of the current

        String[] tabTmp = null;
        if (currentTrackPlayer != null) tabTmp = new String[currentTrackFormatAllowed.size()];

        /*
         *  JAVA do not raise EXCEPTION from an invalid CAST
         *  |--> Example: (String[]) currentTrackFormatAllowed.toArray();
         **/
        if (currentTrackPlayer != null && currentTrack.isPresent() && TrackFormat.fileFormatMatch(track.getFileFormat(), currentTrackFormatAllowed.toArray(tabTmp))) {
            return true;
        } else {
            // Check for each TrackPlayer (but not the current, cause it was checked before), if they can read the Track
            for (TrackPlayer aTrackPlayerList : trackPlayerList) {

                // If the currentTrackPlayer is the same of the TrackPlayer tested
                if (currentTrackPlayer != null && currentTrackPlayer.equals(aTrackPlayerList)) continue;

                List<String> regexFormats = trackPlayerListFormat.get(aTrackPlayerList);

                for (String regexFormat : regexFormats) {

                    if (TrackFormat.fileFormatMatch(track.getFileFormat(), regexFormat)) {

                        // If the currentTrackPlayer is not as the new, we must stop it cause the last sound may still played
                        // and so we will hear the last one with the new one.
                        if (currentTrackPlayer != null) stop();

                        currentTrackPlayer = aTrackPlayerList;
                        currentTrackFormatAllowed = regexFormats;
                        return true;
                    }
                }
            }
        }
        return false;
    }
}