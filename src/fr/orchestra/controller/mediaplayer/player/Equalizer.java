package fr.orchestra.controller.mediaplayer.player;

//import com.adobe.xmp.impl.Utils;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Stop;

public class Equalizer extends VBox {
    int maxValue;
    int barCount;
    double lastWidth = 0.0;
    double lastHeight = 0.0;

    public Equalizer(int maxValue, int barCount) {
        this.maxValue = maxValue;
        this.barCount = barCount;

        setSpacing(2.0);

        setPadding(new Insets(5));
        setStyle("-fx-background-color: red;");

        Stop[] stop = new Stop[5];
        stop[0] = new Stop(0.2, Color.RED);
        stop[1] = new Stop(0.4, Color.ORANGE);
        stop[2] = new Stop(0.6, Color.YELLOW);
        stop[3] = new Stop(0.8, Color.LIGHTGREEN);
        stop[4] = new Stop(0.9, Color.GREEN);


    }

    public void setValue(double value) {
        int barsLit = Math.min(
                barCount, (int) Math.round(value / maxValue * barCount));
        ObservableList lists = getChildren();

    }
}
