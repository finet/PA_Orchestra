package fr.orchestra.controller.mediaplayer;

import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.utils.annotation.ResourceFile;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class PlayerWindow {

    @ResourceFile("fxml.mediaPlayer")
    private static URL mediaPlayerFXML;
    @ResourceFile("pictures.logo")
    private static InputStream logoPictures;
    @ResourceFile("css.player")
    private static String playerCSS;
    private Parent childRoot;
    private Scene scene;
    private Stage stage;
    private PlayerController controller;
    private boolean winStatus = false;

    public PlayerWindow(Stage primaryStage) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(mediaPlayerFXML);
        this.childRoot = fxmlLoader.load();
        this.controller = fxmlLoader.getController();

        scene = new Scene(childRoot);
        stage = new Stage();
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);

        stage.getIcons().add(new Image(logoPictures));
        /** initialise la stage du lecteur audio dans son controller */
        controller.setStage(stage);
        controller.setPrimaryStage(primaryStage);

        /** Charge css for window design */
        scene.getStylesheets().add(playerCSS);
        //stage.show();
    }

    public Stage getStage() {
        return stage;
    }

    public PlayerController getController() {
        return controller;
    }

    public void setWinStatus(boolean status) {
        this.winStatus = status;
    }

    public void openWindow() {

        if (winStatus) {
            stage.hide();
            WindowView.getInstance().setWinStatus(true);
            winStatus = false;
        } else {
            stage.show();
            WindowView.getInstance().setWinStatus(false);
            winStatus = true;
        }
    }
}
