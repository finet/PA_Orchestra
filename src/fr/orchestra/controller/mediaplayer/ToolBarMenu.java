package fr.orchestra.controller.mediaplayer;

import fr.orchestra.controller.folderview.WindowView;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.input.MouseEvent;

public class ToolBarMenu {

    private static double xOffset = 0;
    private static double yOffset = 0;
    private PlayerController playerController;


    public ToolBarMenu(PlayerController playerController, Button exitBtn, Button reduceBtn) {

        this.playerController = playerController;
        exitBtn.setOnAction(event -> {
            WindowView.getInstance().exit();
        });
        reduceBtn.setCancelButton(true);
        reduceBtn.setOnAction(event -> this.playerController.getStage().setIconified(true));
        setMovableWindowBy(playerController.getToolbar());
    }

    public void setMovableWindowBy(ToolBar toolBar) {
        toolBar.setOnMousePressed(this::getPosWindow);
        toolBar.setOnMouseDragged(this::setPosWindow);
    }

    private void getPosWindow(MouseEvent mouseEvent) {
        xOffset = playerController.getStage().getX() - mouseEvent.getScreenX();
        yOffset = playerController.getStage().getY() - mouseEvent.getScreenY();
    }

    private void setPosWindow(MouseEvent mouseEvent) {
        playerController.getStage().setY(mouseEvent.getScreenY() + yOffset);
        playerController.getStage().setX(mouseEvent.getScreenX() + xOffset);
    }


}
