package fr.orchestra.controller.mediaplayer;

import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.folderview.view.right.WaitingList;
import fr.orchestra.controller.login.WindowLog;
import fr.orchestra.controller.mediaplayer.player.PlayerManager;
import fr.orchestra.model.Track;
import fr.orchestra.view.AudioPlayerPictures;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

import static fr.orchestra.utils.Utils.updateCurrentTime;
import static fr.orchestra.utils.properties.ReadProperties.readProperVolumeLevel;
import static fr.orchestra.utils.properties.WriteProperties.writeProperVolumeLevel;

/**
 * TODO :
 * -  Fix affichage currentTime qui se décale
 */

public class PlayerController implements Initializable {

    private Background playIcon;
    private Background playIconHover;
    private Background pauseIcon;
    private Background pauseIconHover;

    private Background loopIcon;
    private Background loopIconHover;
    private Background loopIconEnable;
    private Background loopIconEnableHover;

    private Background repeatIcon;
    private Background repeatIconHover;
    private Background repeatIconEnable;
    private Background repeatIconEnableHover;

    private Background randomIcon;
    private Background randomIconHover;
    private Background randomIconEnable;
    private Background randomIconEnableHover;

    private Background openFolderIcon;
    private Background openFolderIconHover;
    private Background openFolderIconEnable;
    private Background openFolderIconEnableHover;

    private Background logoutIcon;
    private Background logoutIconHover;
    private Background logoutIconEnable;
    private Background logoutIconEnableHover;

    private Background volumeIconEnable;
    private Background volumeIconEnableHover;
    private Background volumeIconDisable;
    private Background volumeIconDisableHover;

    private Font digital_font;

    private boolean play_status = false;
    private boolean loop_status = false;
    private boolean repeat_status = false;
    private boolean random_status = false;

    private boolean openFolder_status = false;
    private boolean logout_status = false;
    private boolean volume_status = true;

    private double volumeLevel = readProperVolumeLevel();

    private PlayerManager playerManager;
    private Stage PrimaryStage;
    private Stage stageFolder;
    private Stage stage;

    @FXML
    private Button exitBtn;

    @FXML
    private Button reduceBtn;

    @FXML
    private ToggleButton playBtn;

    @FXML
    private Button volumeBtn;

    @FXML
    private Button loopBtn;

    @FXML
    private Button logoutBtn;

    @FXML
    private Button repeatBtn;

    @FXML
    private Button randomBtn;

    @FXML
    private Label nameTrackLabel;

    @FXML
    private Label artistTrackLabel;

    @FXML
    private Label extraTrackLabel; // album - genre - year

    @FXML
    private Label totalDurationLabel;

    @FXML
    private Label currentDurationLabel;

    @FXML
    private Slider volumeSlider;

    @FXML
    private Slider seekSlider;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private ProgressBar volumeProgressBar;

    @FXML
    private ToggleButton openFolderBtn;

    @FXML
    private ToolBar toolBar;

    public ToolBar getToolbar() {
        return this.toolBar;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setStageFolder(Stage stageFolder) {
        this.stageFolder = stageFolder;
    }

    public void setPrimaryStage(Stage PrimaryStage) {
        this.PrimaryStage = PrimaryStage;
    }

    public Label getTotalDurationLabel() {
        return totalDurationLabel;
    }

    public Label getCurrentDurationLabel() {
        return currentDurationLabel;
    }

    public Slider getSeekSlider() {
        return seekSlider;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public Slider getVolumeSlider() {
        return volumeSlider;
    }

    public ToggleButton getPlayBtn() {
        return playBtn;
    }

    public double getVolumeLevel() {
        return volumeLevel;
    }

    public Label getNameTrackLabel() {
        return nameTrackLabel;
    }

    public Label getArtistTrackLabel() {
        return artistTrackLabel;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        new ToolBarMenu(this, exitBtn, reduceBtn);

        playerManager = PlayerManager.getInstance();
        playerManager.setPlayerController(this);

        loadIcons();
        loadFonts();
        InitVolume();
        addEventOnBtn();

        currentDurationLabel.setFont(digital_font);
        playBtn.setBackground(playIcon);
        loopBtn.setBackground(loopIcon);
        repeatBtn.setBackground(repeatIcon);
        randomBtn.setBackground(randomIcon);
        openFolderBtn.setBackground(openFolderIcon);
        logoutBtn.setBackground(logoutIcon);
    }

    @FXML
    private void openFolder() {

        WindowView.getInstance().openWindow();
        stage.hide();
    }

    @FXML
    public void logout() {

        /** Si le lecteur audio à déja été instancié **/
        if (playerManager.isInitialized()) {
            stop();
            playerManager.resetPlaylist();
        }

        WindowLog.getInstance().showWindow();

        /** Delete instance of User **/
        WindowView.getInstance().setUser(null);

        stage.close();
        stageFolder.close();

        /** Persistance **/
        writeProperVolumeLevel(volumeLevel);

        /** Remove WaitingList **/
        if (WaitingList.getInstance() != null) WaitingList.getInstance().removeINSTANCE();
    }

    /*****************************************************************************************
     * @Job : Gestion of playing Track
     */
    @FXML
    public void play() {

        playerManager.play();
    }

    private void stop() {

        playerManager.stop();
    }

    /*****************************************************************************************
     * @job: Gestion of AudioPlayer properties
     */
    @FXML
    private void repeat() {

        setIconRepeatBtn(repeat_status);
        repeat_status = !repeat_status;

        WindowView.getInstance().getController().setRepeat_status(repeat_status);
        playerManager.setPlayingRepeat(repeat_status);
    }

    @FXML
    private void random() {

        setIconRandomBtn(random_status);
        random_status = !random_status;

        playerManager.setPlayingRandom(random_status);
    }

    @FXML
    private void loop() {

        setIconLoopBtn(loop_status);
        loop_status = !loop_status;

        playerManager.setPlayingLoop(loop_status);
    }

    @FXML
    private void next() {

        if (repeat_status)
            playerManager.reload();
        else {
            playerManager.setTrackNumber(playerManager.getTrackNumber() + 1);
            playerManager.runTrack();
        }
    }

    @FXML
    private void previous() {

        if (repeat_status)
            playerManager.reload();
        else {
            playerManager.setTrackNumber(playerManager.getTrackNumber() - 1);
            playerManager.runTrack();
        }
    }

    @FXML
    private void volume() {


        if (volume_status) {

            volumeLevel = volumeSlider.getValue();
            volumeBtn.setBackground(volumeIconDisableHover);
            volume_status = false;
            playerManager.getCurrentTrackPlayer().setVolume(0);
            volumeSlider.setValue(0);
            volumeProgressBar.setProgress(0);
        } else {
            volumeBtn.setBackground(volumeIconEnableHover);
            volume_status = true;
            if (volumeLevel >= 0)
                playerManager.getCurrentTrackPlayer().setVolume(volumeLevel / 100);
            volumeSlider.setValue(volumeLevel);
        }
    }

    /******************************************************************************************
     * @Job : Initialise le volume (partie graphique et lecteur audio) et attach un event pour la gestion du volume
     */
    private void InitVolume() {

        if (volumeLevel > 0) {
            volumeProgressBar.setProgress(volumeLevel / 100);
            //playerManager.getCurrentTrackPlayer().setVolume(volumeLevel/ 100);
            volumeSlider.setValue(volumeLevel);
            volumeBtn.setBackground(volumeIconEnable);
        } else {
            volumeProgressBar.setProgress(volumeSlider.getValue() / 100);
            //playerManager.getCurrentTrackPlayer().setVolume(volumeSlider.getValue() / 100);
            volumeSlider.setValue(0);
            volumeBtn.setBackground(volumeIconDisable);
        }

        /** add listener to Slider volume **/
        volumeSlider.valueProperty().addListener((Observable) -> {
            playerManager.getCurrentTrackPlayer().setVolume(volumeSlider.getValue() / 100);
            volumeProgressBar.setProgress(volumeSlider.getValue() / 100);
            volumeLevel = volumeSlider.getValue();

            if (volumeSlider.getValue() > 0) {
                volumeBtn.setBackground(volumeIconEnable);
                volume_status = true;
            } else {
                volumeBtn.setBackground(volumeIconDisable);
                volume_status = false;
            }

        });
    }

    /******************************************************************************************
     * @Job : Update player display and add listenner on progress bar and volume
     * @param currentTrack
     */
    public void updatePlayer(Track currentTrack) {

        nameTrackLabel.textProperty().bind(currentTrack.nameProperty());
        artistTrackLabel.textProperty().bind(currentTrack.artistProperty());
        extraTrackLabel.setText(currentTrack.getExtraInfo());
        currentDurationLabel.setText(updateCurrentTime(playerManager.getCurrentTrackPlayer().getCurrentTime()));
    }

    /******************************************************************************************
     * @Job : Set dynamically Icon & Css on audioplayer buttons
     * @param play_status
     */
    public void setIconPlayBtn(boolean play_status) {

        this.play_status = play_status;

        if (!play_status)
            playBtn.setBackground(playIconHover);
        else
            playBtn.setBackground(pauseIconHover);
    }

    public void setIconLoopBtn(boolean loop_status) {

        this.loop_status = loop_status;

        if (!loop_status)
            loopBtn.setBackground(loopIconEnableHover);
        else
            loopBtn.setBackground(loopIconHover);
    }

    public void setIconRepeatBtn(boolean repeat_status) {

        this.repeat_status = repeat_status;

        if (!repeat_status)
            repeatBtn.setBackground(repeatIconEnableHover);
        else
            repeatBtn.setBackground(repeatIconHover);

    }

    public void setIconRandomBtn(boolean random_status) {

        this.random_status = random_status;

        if (!random_status)
            randomBtn.setBackground(randomIconEnableHover);
        else
            randomBtn.setBackground(randomIconHover);

    }

    /******************************************************************************************
     * Add event on buttons for gestion of icons / iconsHover
     */
    private void addEventOnBtn() {

        playBtn.addEventHandler(MouseEvent.MOUSE_ENTERED,
                event -> {
                    if (!play_status)
                        playBtn.setBackground(playIconHover);
                    else
                        playBtn.setBackground(pauseIconHover);
                });

        playBtn.addEventHandler(MouseEvent.MOUSE_EXITED,
                event -> {
                    if (!play_status)
                        playBtn.setBackground(playIcon);
                    else
                        playBtn.setBackground(pauseIcon);
                });

        loopBtn.addEventHandler(MouseEvent.MOUSE_ENTERED,
                event -> {
                    if (!loop_status)
                        loopBtn.setBackground(loopIconHover);
                    else
                        loopBtn.setBackground(loopIconEnableHover);
                });

        loopBtn.addEventHandler(MouseEvent.MOUSE_EXITED,
                event -> {
                    if (!loop_status)
                        loopBtn.setBackground(loopIcon);
                    else
                        loopBtn.setBackground(loopIconEnable);
                });

        repeatBtn.addEventHandler(MouseEvent.MOUSE_ENTERED,
                event -> {
                    if (!repeat_status)
                        repeatBtn.setBackground(repeatIconHover);
                    else
                        repeatBtn.setBackground(repeatIconEnableHover);
                });

        repeatBtn.addEventHandler(MouseEvent.MOUSE_EXITED,
                event -> {
                    if (!repeat_status)
                        repeatBtn.setBackground(repeatIcon);
                    else
                        repeatBtn.setBackground(repeatIconEnable);
                });

        randomBtn.addEventHandler(MouseEvent.MOUSE_ENTERED,
                event -> {
                    if (!random_status)
                        randomBtn.setBackground(randomIconHover);
                    else
                        randomBtn.setBackground(randomIconEnableHover);
                });

        randomBtn.addEventHandler(MouseEvent.MOUSE_EXITED,
                event -> {
                    if (!random_status)
                        randomBtn.setBackground(randomIcon);
                    else
                        randomBtn.setBackground(randomIconEnable);
                });

        openFolderBtn.addEventHandler(MouseEvent.MOUSE_ENTERED,
                event -> {
                    if (!openFolder_status)
                        openFolderBtn.setBackground(openFolderIconHover);
                    else
                        openFolderBtn.setBackground(openFolderIconEnableHover);
                });

        openFolderBtn.addEventHandler(MouseEvent.MOUSE_EXITED,
                event -> {
                    if (!openFolder_status)
                        openFolderBtn.setBackground(openFolderIcon);
                    else
                        openFolderBtn.setBackground(openFolderIconEnable);
                });

        logoutBtn.addEventHandler(MouseEvent.MOUSE_ENTERED,
                event -> {
                    if (!logout_status)
                        logoutBtn.setBackground(logoutIconHover);
                    else
                        logoutBtn.setBackground(logoutIconEnableHover);
                });

        logoutBtn.addEventHandler(MouseEvent.MOUSE_EXITED,
                event -> {
                    if (!logout_status)
                        logoutBtn.setBackground(logoutIcon);
                    else
                        logoutBtn.setBackground(logoutIconEnable);
                });

        volumeBtn.addEventHandler(MouseEvent.MOUSE_ENTERED,
                event -> {
                    if (volume_status)
                        volumeBtn.setBackground(volumeIconEnableHover);
                    else
                        volumeBtn.setBackground(volumeIconDisableHover);
                });

        volumeBtn.addEventHandler(MouseEvent.MOUSE_EXITED,
                event -> {
                    if (volume_status)
                        volumeBtn.setBackground(volumeIconEnable);
                    else
                        volumeBtn.setBackground(volumeIconDisable);
                });
    }

    /******************************************************************************************
     * Load icons for play button play / playHover / pauseIcon / pause_hover AND load fonts
     */

    private void loadFonts() {

        digital_font = Font.loadFont(AudioPlayerPictures.getFontTTF(), 42);
    }

    private void loadIcons() {

        BackgroundImage p1 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerPlayPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage p1h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerPlayHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage p2 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerPausePictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage p2h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerPauseHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage l1 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerLoopPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage l1h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerLoopHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage l2 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerLoopEnablePictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage l2h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerLoopEnableHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage r1 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerRepeatPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage r1h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerRepeatHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage r2 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerRepeatEnablePictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage r2h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerRepeatEnableHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage ra1 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerRandomPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage ra1h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerRandomHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage ra2 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerRandomEnablePictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage ra2h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerRandomEnableHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage o1 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerOpenFolderPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage o1h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerOpenFolderHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage o2 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerOpenFolderEnablePictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage o2h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerOpenFolderEnableHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage lo1 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerLogoutPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage lo1h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerLogoutHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage lo2 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerLogoutEnablePictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage lo2h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerLogoutEnableHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage v1 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerVolumeEnablePictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage v1h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerVolumeEnableHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage v2 = new BackgroundImage(AudioPlayerPictures.getAudioPlayerVolumeDisablePictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        BackgroundImage v2h = new BackgroundImage(AudioPlayerPictures.getAudioPlayerVolumeDisableHoverPictures(),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);

        this.playIcon = new Background(p1);
        this.playIconHover = new Background(p1h);
        this.pauseIcon = new Background(p2);
        this.pauseIconHover = new Background(p2h);

        this.loopIcon = new Background(l1);
        this.loopIconHover = new Background(l1h);
        this.loopIconEnable = new Background(l2);
        this.loopIconEnableHover = new Background(l2h);

        this.repeatIcon = new Background(r1);
        this.repeatIconHover = new Background(r1h);
        this.repeatIconEnable = new Background(r2);
        this.repeatIconEnableHover = new Background(r2h);

        this.randomIcon = new Background(ra1);
        this.randomIconHover = new Background(ra1h);
        this.randomIconEnable = new Background(ra2);
        this.randomIconEnableHover = new Background(ra2h);

        this.openFolderIcon = new Background(o1);
        this.openFolderIconHover = new Background(o1h);
        this.openFolderIconEnable = new Background(o2);
        this.openFolderIconEnableHover = new Background(o2h);

        this.logoutIcon = new Background(lo1);
        this.logoutIconHover = new Background(lo1h);
        this.logoutIconEnable = new Background(lo2);
        this.logoutIconEnableHover = new Background(lo2h);

        this.volumeIconEnable = new Background(v1);
        this.volumeIconEnableHover = new Background(v1h);
        this.volumeIconDisable = new Background(v2);
        this.volumeIconDisableHover = new Background(v2h);

    }
}