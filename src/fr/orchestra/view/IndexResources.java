package fr.orchestra.view;


import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class IndexResources {

    private final static String DIR_RESOURCES_JAR = "resources";
    private static IndexResources INSTANCE = new IndexResources();
    private final Map<String, Map<String, URL>> mapDirResources = new HashMap<>();
    private File resourcesFilePath = new File(getResourceFilePath());
    private String key;

    {
        for (File subFile : Objects.requireNonNull(resourcesFilePath.listFiles())) {

            Map dirMap = new HashMap<>();
            key = "";
            if (resourcesFilePath.isDirectory()) {

                key = subFile.getName();
                mapDirResources.put(key, dirMap);
            } else {
                mapDirResources.put(key, dirMap);
            }
            loadFile(dirMap, subFile, key);
        }
    }

    private IndexResources() {
        INSTANCE = this;
    }

    public static IndexResources getINSTANCE() {
        return INSTANCE;
    }

    private String getResourceFilePath() {

        String path = getClass().getProtectionDomain().getCodeSource().getLocation().getFile();

        if (path.endsWith(".jar")) {
            path = path.substring(0, path.lastIndexOf("/") + 1) + DIR_RESOURCES_JAR;
        } else {
            path = IndexResources.class.getProtectionDomain().getCodeSource().getLocation().getPath()
                    + IndexResources.class.getPackage().getName().replaceAll("\\.", "/");
        }
        return path;
    }

    public URL getFileResource(String nameFile) {

        String nameType = nameFile.substring(0, nameFile.indexOf('.'));
        nameFile = nameFile.substring(nameFile.indexOf('.') + 1);

        return mapDirResources.get(nameType).get(nameFile);
    }

    private void loadFile(Map<String, URL> map, File file, String dirParent) {

        if (file.isDirectory()) {

            for (File tmpFile : Objects.requireNonNull(file.listFiles())) {
                loadFile(map, tmpFile, dirParent);
            }
        } else {
            if (!file.getName().endsWith(".class")) {
                int fileTypeSize = file.getName().length() - file.getName().lastIndexOf(".");

                // Remove path of the indexResources.class and the folder of resource type. At the end, the file extension is removed too.
                String tmp = file.getPath().substring(resourcesFilePath.getPath().length() + 1 + key.length() + 1, file.getPath().length() - fileTypeSize);
                try {
                    map.put(tmp.replaceAll("[\\\\/]", "\\."), file.toURI().toURL());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}