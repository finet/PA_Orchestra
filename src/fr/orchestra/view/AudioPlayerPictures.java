package fr.orchestra.view;

import fr.orchestra.utils.annotation.ResourceFile;
import javafx.scene.image.Image;

public class AudioPlayerPictures {

    @ResourceFile("fonts.digital-7")
    private static String fontTTF;

    @ResourceFile("pictures.audioplayer.play")
    private static Image audioPlayerPlayPictures;

    @ResourceFile("pictures.audioplayer.playHover")
    private static Image audioPlayerPlayHoverPictures;

    @ResourceFile("pictures.audioplayer.pause")
    private static Image audioPlayerPausePictures;

    @ResourceFile("pictures.audioplayer.pauseHover")
    private static Image audioPlayerPauseHoverPictures;

    @ResourceFile("pictures.audioplayer.loop")
    private static Image audioPlayerLoopPictures;

    @ResourceFile("pictures.audioplayer.loopHover")
    private static Image audioPlayerLoopHoverPictures;

    @ResourceFile("pictures.audioplayer.loopEnable")
    private static Image audioPlayerLoopEnablePictures;

    @ResourceFile("pictures.audioplayer.loopEnableHover")
    private static Image audioPlayerLoopEnableHoverPictures;

    @ResourceFile("pictures.audioplayer.repeat")
    private static Image audioPlayerRepeatPictures;

    @ResourceFile("pictures.audioplayer.repeatHover")
    private static Image audioPlayerRepeatHoverPictures;

    @ResourceFile("pictures.audioplayer.repeatEnable")
    private static Image audioPlayerRepeatEnablePictures;

    @ResourceFile("pictures.audioplayer.repeatEnableHover")
    private static Image audioPlayerRepeatEnableHoverPictures;

    @ResourceFile("pictures.audioplayer.random")
    private static Image audioPlayerRandomPictures;

    @ResourceFile("pictures.audioplayer.randomHover")
    private static Image audioPlayerRandomHoverPictures;

    @ResourceFile("pictures.audioplayer.randomEnable")
    private static Image audioPlayerRandomEnablePictures;

    @ResourceFile("pictures.audioplayer.randomEnableHover")
    private static Image audioPlayerRandomEnableHoverPictures;

    @ResourceFile("pictures.audioplayer.openFolder")
    private static Image audioPlayerOpenFolderPictures;

    @ResourceFile("pictures.audioplayer.openFolderHover")
    private static Image audioPlayerOpenFolderHoverPictures;

    @ResourceFile("pictures.audioplayer.openFolderEnable")
    private static Image audioPlayerOpenFolderEnablePictures;

    @ResourceFile("pictures.audioplayer.openFolderEnableHover")
    private static Image audioPlayerOpenFolderEnableHoverPictures;

    @ResourceFile("pictures.audioplayer.logout")
    private static Image audioPlayerLogoutPictures;

    @ResourceFile("pictures.audioplayer.logoutHover")
    private static Image audioPlayerLogoutHoverPictures;

    @ResourceFile("pictures.audioplayer.logoutEnable")
    private static Image audioPlayerLogoutEnablePictures;

    @ResourceFile("pictures.audioplayer.logoutEnableHover")
    private static Image audioPlayerLogoutEnableHoverPictures;

    @ResourceFile("pictures.audioplayer.volumeEnable")
    private static Image audioPlayerVolumeEnablePictures;

    @ResourceFile("pictures.audioplayer.volumeEnableHover")
    private static Image audioPlayerVolumeEnableHoverPictures;

    @ResourceFile("pictures.audioplayer.volumeDisable")
    private static Image audioPlayerVolumeDisablePictures;

    @ResourceFile("pictures.audioplayer.volumeDisableHover")
    private static Image audioPlayerVolumeDisableHoverPictures;

    @ResourceFile("pictures.folderview.uploadRacineDisable")
    private static Image folderViewUploadRacineDisable;

    @ResourceFile("pictures.folderview.uploadRacineDisableHover")
    private static Image folderViewUploadRacineDisableHover;

    @ResourceFile("pictures.folderview.uploadRacineEnable")
    private static Image folderViewUploadRacineEnable;

    @ResourceFile("pictures.folderview.uploadRacineEnableHover")
    private static Image folderViewUploadRacineEnableHover;


    public static String getFontTTF() {
        return fontTTF;
    }

    public static Image getFolderViewUploadRacineDisable() {
        return folderViewUploadRacineDisable;
    }

    public static Image getFolderViewUploadRacineDisableHover() {
        return folderViewUploadRacineDisableHover;
    }

    public static Image getFolderViewUploadRacineEnable() {
        return folderViewUploadRacineEnable;
    }

    public static Image getFolderViewUploadRacineEnableHover() {
        return folderViewUploadRacineEnableHover;
    }

    public static Image getAudioPlayerPlayPictures() {
        return audioPlayerPlayPictures;
    }

    public static Image getAudioPlayerPlayHoverPictures() {
        return audioPlayerPlayHoverPictures;
    }

    public static Image getAudioPlayerPausePictures() {
        return audioPlayerPausePictures;
    }

    public static Image getAudioPlayerPauseHoverPictures() {
        return audioPlayerPauseHoverPictures;
    }

    public static Image getAudioPlayerLoopPictures() {
        return audioPlayerLoopPictures;
    }

    public static Image getAudioPlayerLoopHoverPictures() {
        return audioPlayerLoopHoverPictures;
    }

    public static Image getAudioPlayerLoopEnablePictures() {
        return audioPlayerLoopEnablePictures;
    }

    public static Image getAudioPlayerLoopEnableHoverPictures() {
        return audioPlayerLoopEnableHoverPictures;
    }

    public static Image getAudioPlayerRepeatPictures() {
        return audioPlayerRepeatPictures;
    }

    public static Image getAudioPlayerRepeatHoverPictures() {
        return audioPlayerRepeatHoverPictures;
    }

    public static Image getAudioPlayerRepeatEnablePictures() {
        return audioPlayerRepeatEnablePictures;
    }

    public static Image getAudioPlayerRepeatEnableHoverPictures() {
        return audioPlayerRepeatEnableHoverPictures;
    }

    public static Image getAudioPlayerRandomPictures() {
        return audioPlayerRandomPictures;
    }

    public static Image getAudioPlayerRandomHoverPictures() {
        return audioPlayerRandomHoverPictures;
    }

    public static Image getAudioPlayerRandomEnablePictures() {
        return audioPlayerRandomEnablePictures;
    }

    public static Image getAudioPlayerRandomEnableHoverPictures() {
        return audioPlayerRandomEnableHoverPictures;
    }

    public static Image getAudioPlayerOpenFolderPictures() {
        return audioPlayerOpenFolderPictures;
    }

    public static Image getAudioPlayerOpenFolderHoverPictures() {
        return audioPlayerOpenFolderHoverPictures;
    }

    public static Image getAudioPlayerOpenFolderEnablePictures() {
        return audioPlayerOpenFolderEnablePictures;
    }

    public static Image getAudioPlayerOpenFolderEnableHoverPictures() { return audioPlayerOpenFolderEnableHoverPictures; }

    public static Image getAudioPlayerLogoutPictures() {
        return audioPlayerLogoutPictures;
    }

    public static Image getAudioPlayerLogoutHoverPictures() {
        return audioPlayerLogoutHoverPictures;
    }

    public static Image getAudioPlayerLogoutEnablePictures() {
        return audioPlayerLogoutEnablePictures;
    }

    public static Image getAudioPlayerLogoutEnableHoverPictures() {
        return audioPlayerLogoutEnableHoverPictures;
    }

    public static Image getAudioPlayerVolumeEnablePictures() {
        return audioPlayerVolumeEnablePictures;
    }

    public static Image getAudioPlayerVolumeEnableHoverPictures() {
        return audioPlayerVolumeEnableHoverPictures;
    }

    public static Image getAudioPlayerVolumeDisablePictures() {
        return audioPlayerVolumeDisablePictures;
    }

    public static Image getAudioPlayerVolumeDisableHoverPictures() {
        return audioPlayerVolumeDisableHoverPictures;
    }
}
