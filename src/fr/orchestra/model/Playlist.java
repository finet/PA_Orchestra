package fr.orchestra.model;


import fr.orchestra.utils.system.ConfSys;
import javafx.scene.image.Image;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Playlist implements Serializable, ConfSys {

    private String UserPseudo;
    private transient Image imagePlaylist;
    private String playlistName;
    private Date createdDate;
    private Date updatedDate;
    private boolean shared;
    private int id;

    private List<Track> trackList = new ArrayList<>();

    public Playlist(String UserPseudo, Image imagePlaylist, String playlistName, boolean shared) {

        this.UserPseudo = UserPseudo;
        this.imagePlaylist = imagePlaylist;
        this.playlistName = playlistName;
        this.createdDate = new Date();
        this.updatedDate = new Date();
        this.shared = shared;
    }

    public Playlist(String UserPseudo, String imagePlaylistUrl, String playlistName, boolean shared) {

        this(UserPseudo, new Image(imagePlaylistUrl), playlistName, shared);
    }

    public static void writeSerializedPlaylist(Playlist playlist, Path path) {

        final Playlist SerializerPlaylist = new Playlist(playlist.getUserPseudo(),
                playlist.getImagePlaylistUrl(),
                playlist.getPlaylistName(),
                true);
        ObjectOutputStream oos = null;

        try {
            final FileOutputStream fos = new FileOutputStream(String.valueOf(path) + FILE_SEPARATOR + playlist.getPlaylistName());
            oos = new ObjectOutputStream(fos);
            oos.writeObject(SerializerPlaylist);
            oos.flush();

        } catch (final java.io.IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (oos != null) {
                    oos.flush();
                    oos.close();
                }
            } catch (final IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static Playlist readSerializedPlaylist(String path) {

        Playlist playlist = null;
        ObjectInputStream ois = null;

        try {
            ois = new ObjectInputStream(new FileInputStream(path));
            playlist = (Playlist) ois.readObject();
        } catch (final IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {

            try {
                if (ois != null) {
                    ois.close();
                }
            } catch (final IOException ex) {
                ex.printStackTrace();
            }
        }

        // load image
        assert playlist != null;
        playlist.setImagePlaylist(new Image(playlist.getImagePlaylistUrl()));
        return playlist;
    }

    public void addTrack(Track track) {
        this.trackList.add(track);
    }

    public void addTrack(List<Track> track) {
        this.trackList.addAll(track);
    }

    public String getImagePlaylistUrl() {
        return imagePlaylist.getUrl();
    }

    public void setImagePlaylistByUrl(String imagePlaylistUrl) {
        this.imagePlaylist = new Image(imagePlaylistUrl);
    }

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.updatedDate = new Date();
        this.shared = shared;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserPseudo() {
        return UserPseudo;
    }

    public Image getImagePlaylist() {
        return imagePlaylist;
    }

    public void setImagePlaylist(Image imagePlaylist) {
        this.updatedDate.setTime(System.currentTimeMillis());
        this.imagePlaylist = imagePlaylist;
    }

    public String getPlaylistName() {
        return playlistName;
    }

    public void setPlaylistName(String playlistName) {
        this.updatedDate.setTime(System.currentTimeMillis());
        this.playlistName = playlistName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public Track[] getTrackList() {
        return trackList.toArray(new Track[0]);
    }

    public Track getTrack(int index) {
        return trackList.get(index);
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {

        oos.defaultWriteObject();
        oos.writeUTF(imagePlaylist.getUrl());
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {

        ois.defaultReadObject();
        imagePlaylist = new Image(ois.readUTF());
    }

    @Override
    public boolean equals(Object obj) {

        return (obj instanceof String) ? obj.equals(this.playlistName) : super.equals(obj);
    }

    @Override
    public String toString() {
        return "Playlist{" +
                "UserPseudo='" + UserPseudo + '\'' +
                "\n, imagePlaylist=" + imagePlaylist +
                "\n, imagePlaylistUrl=" + imagePlaylist.getUrl() +
                "\n, playlistName='" + playlistName + '\'' +
                "\n, createdDate=" + createdDate +
                "\n, updatedDate=" + updatedDate +
                "\n, shared=" + shared +
                "\nTrackList" + trackList.toString() +
                '}';
    }
}
