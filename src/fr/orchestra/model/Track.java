package fr.orchestra.model;

import fr.orchestra.plugin.templates.MetadataGetter;
import fr.orchestra.utils.format.TrackFormat;
import fr.orchestra.utils.metadata.MetadataFactory;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class Track implements Serializable {

    private static final long serialVersionUID = 1250012881342729535L;

    private int trackId;
    private String fileFolder = ""; // file folder absolute path
    private String fileFormat = "";
    private String name = "";
    private String artist = "";
    private String album = "";
    private String genre = "";
    private String extraInfo = ""; // for concat album - genre - year
    private double totalTime = 0.0;
    private String src_tracks = "";

    /**
     * listener
     **/
    private transient StringProperty nameProperty;
    private transient StringProperty artistProperty;

    public Track() {
    }

    public Track(int trackId, double totalTime, String fileFolder, String fileFormat,
                 String name, String artist, String album, String genre) {

        this.trackId = trackId;
        this.fileFolder = fileFolder;
        this.fileFormat = fileFormat;
        this.name = name;
        this.artist = artist;
        this.album = album;
        this.genre = genre;
        this.totalTime = totalTime;

        setExtraInfos();

        nameProperty = new SimpleStringProperty(this, "name", name);
        nameProperty.addListener((observable, oldString, newString) -> setName(newString));
        artistProperty = new SimpleStringProperty(this, "artist", artist);
        artistProperty.addListener((observable, oldString, newString) -> setArtist(newString));
    }

    public static Track setTrack(File fileAudio) {

        // First, select the good Metadata extractor (depend of the kind of file format)
        MetadataGetter metadata = MetadataFactory.getInstance().getMetadataReader(fileAudio);
        metadata.initMetadataExtractor(fileAudio);

        return new Track(
                0,
                metadata.getDoubleValueDurationMilliSec(),
                fileAudio.getPath(),
                TrackFormat.getFormat(fileAudio),
                metadata.getTitle(),
                metadata.getArtist(),
                metadata.getAlbum(),
                metadata.getGenre()
        );
    }

    public static boolean checkTracks(Track track, List<Track> trackList) {
        return !trackList.contains(track) &&
                Track.checkTrack(track).equals("");
    }

    // @TODO Must throw some exception (better than return)
    public static String checkTrack(Track track) {
        int MAX_MEMORY_SIZE = 1024 * 1024 * 100;
        String[] extendsFileArray = {"mp3", "mp4"};

        File file = new File(track.getFileFolder());
        String extension = FilenameUtils.getExtension(track.getFileFolder());
        long size = file.length();

        if (track.getArtist().equals(""))
            return "Récupération de l'artist impossible";
        if (track.getName().equals(""))
            return "Récupération du nom de la musique impossible";
        if (track.getTotalTime() == 0.0)
            return "Récupération de la durée de la musique impossible";
        if (!Arrays.asList(extendsFileArray).contains(extension))
            return "Format non autorisé (mp3 ou mp4)";
        if (size > MAX_MEMORY_SIZE)
            return "Taille de la musiques trop élevé (100 MO max)";
        return "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim().replaceAll("\\s+", " ")
                .replaceAll(" (?i)(remix)", " Remix")
                .replaceAll("(?i)(remix) (?i)(by) ", "Remix by ")
                .replaceAll("(?i)(ft)(\\.|\\s) ", "ft ")
                .replaceAll("(?i)(feat)(\\.|\\s) ", "feat ")
                .replaceAll("(?i)(featuring) ", "featuring ")
                .replaceAll("(?i)(with) ", "with ");
        nameProperty.setValue(this.name);
    }

    public String getFileFolder() {
        return fileFolder;
    }

    public void setFileFolder(String fileFolder) {
        this.fileFolder = fileFolder;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
        artistProperty.setValue(this.artist);
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public double getTotalTime() {
        return totalTime;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public int getTrackId() {
        return trackId;
    }

    public String getSrcTracks() {
        return src_tracks;
    }

    public void setSrcTracks(String src_tracks) {
        this.src_tracks = src_tracks;
    }

    public StringProperty nameProperty() {
        return nameProperty;
    }

    public StringProperty artistProperty() {
        return artistProperty;
    }

    public String getFileFormat() {
        return fileFormat;
    }

    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }

    public void setExtraInfos() {

        String finalString = album + " - " + genre;
        this.extraInfo = finalString;
    }

    public boolean isPlayable() throws IOException {

        boolean playable = true;

        File file = new File(fileFolder);
        if (!file.exists()) {
            throw new IOException("File not found: " + fileFolder);
        }
        return playable;
    }

    // function writeObject, use to serialize an object
    private void writeObject(ObjectOutputStream oos) throws IOException {

        oos.defaultWriteObject();
        oos.writeUTF(nameProperty().getValueSafe());
        oos.writeUTF(artistProperty().getValueSafe());
    }

    // function readObject, use to rebuild an object serialized
    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {

        ois.defaultReadObject();

        nameProperty = new SimpleStringProperty(this, "name", ois.readUTF());
        nameProperty.addListener((observable, oldString, newString) -> setName(newString));
        artistProperty = new SimpleStringProperty(this, "artist", ois.readUTF());
        artistProperty.addListener((observable, oldString, newString) -> setArtist(newString));
    }

    @Override
    public boolean equals(Object o) {

        boolean equals = false;

        if (o instanceof Track) {
            Track oTrack = (Track) o;
            if (oTrack.getFileFolder().equalsIgnoreCase(fileFolder))
                equals = true;
        }

        return equals;
    }

    @Override
    public String toString() {

        return trackId + "|" + totalTime + "|" + name + "|" + artist + "|" + genre + "|" + album + "|";
    }
}
