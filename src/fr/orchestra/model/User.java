package fr.orchestra.model;


import java.util.HashMap;
import java.util.Map;


public class User {

    private int idUser;
    private String login;
    private String password;
    private String accessToken;

    private Map<String, Playlist> playlistList = new HashMap<>();

    public User(int idUser, String login, String password, String accessToken) {

        this.idUser = idUser;
        this.login = login;
        this.password = password;
        this.accessToken = accessToken;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Playlist getPlaylist(String name) {
        return playlistList.get(name);
    }

    public void addPlaylist(Playlist playlist) {
        playlistList.put(playlist.getPlaylistName(), playlist);
    }

    public boolean havePlaylist(String playlistName) {
        return playlistList.containsKey(playlistName);
    }

    public boolean havePlaylist(Playlist playlist) {
        return playlistList.containsValue(playlist);
    }

    @Override
    public String toString() {

        return idUser + " | " + login + " | " + password + " | Token : " + accessToken + "\n"
                + "List of Playlist : " + playlistList;
    }
}
