package fr.orchestra;


import fr.orchestra.api.UserServices;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.controller.mediaplayer.PlayerWindow;
import fr.orchestra.utils.system.ConfSys;
import javafx.stage.Stage;

import javax.management.InstanceAlreadyExistsException;
import java.io.IOException;
import java.net.ConnectException;
import java.util.Arrays;


public class OrchestraDevLauncher extends OrchestraLauncher implements ConfSys {

    private static boolean dbConnect = true;
    private final static String USER_NAME = getEnvVar("USER_NAME");
    private final static String USER_PSWD = getEnvVar("USER_PSWD");
    private final static String DB_NAME = getEnvVar("DB_NAME");
    private final static String DB_PORT = getEnvVar("DB_PORT");
    private static boolean debugTrace = false;

    public static void main(String[] args) {
        checkJavaVersion();

        System.out.println("Args Main Function " + Arrays.toString(args));
        System.out.println("Java Runtime Version Num [" + JAVA_VERSION_NUM + "]");
        System.out.println("System File Separator '" + FILE_SEPARATOR + "'");
        System.out.println("Launcher Class #> " + OrchestraDevLauncher.class.getName());

        applyArgMain(args);

        launch(args);
    }

    private static String getEnvVar(String envVarName) {

        String envVarValue = System.getenv(envVarName);

        switch (envVarName) {

            case "USER_NAME":
                return (envVarValue.equals("")) ? "SaulGoodMan" : envVarValue;

            case "USER_PSWD":
                return (envVarValue.equals("")) ? "NamDoogLaus" : envVarValue;

            case "DB_NAME":
                return envVarValue;

            case "DB_PORT":
                if (System.getenv("DB_NAME").equals("")) {
                    dbConnect = false;
                    return "";
                }
                return (!System.getenv("DB_NAME").equals("") && envVarValue.equals("")) ? "http://localhost:3306/" : envVarValue;

            default:
                System.err.println("[WARNING] This Environment Variable must use \"System.getenv(String)\", no need this function");
                return envVarValue;
        }
    }

    private static void applyArgMain(String[] argsMain) {

        for (String arg : argsMain) {
            switch (arg) {

                case "DEBUG":
                    debugTrace = true;
                    break;

                case "NO_LOGIN":
                    dbConnect = false;
                    break;

                default:
                    System.err.println("[WARNING] Argument \"" + arg + "\" unknown");

            }
        }
    }

    @Override
    public void init() throws Exception {
        super.init();
        System.out.println("---- ENVIRONMENT'S VARIABLES ----");
        System.out.println("USER_NAME={" + USER_NAME + "}");
        System.out.println("USER_PSWD={" + USER_PSWD + "}");
        System.out.println("DB_NAME={" + DB_NAME + "}");
        System.out.println("DB_PORT={" + DB_PORT + "}");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("MyOrchestra");
        devLogin(primaryStage);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.out.println("End of Process --> OrchestraDevLauncher::stop()");
    }

    private void devLogin(Stage primaryStage) throws ConnectException, InstanceAlreadyExistsException {

        if (dbConnect) {

            UserServices.createInstance(UserServices.Mode.ONLINE);

            if (!UserServices.getAuthOp().auth(USER_NAME, USER_PSWD)) {
                throw new ConnectException("Invalid connection to the DB with " + this);
            }
        }
        else {
            UserServices.createInstance(UserServices.Mode.OFFLINE);

            UserServices.getAuthOp().auth(USER_NAME, USER_PSWD);
        }

        PlayerWindow playerWindow = null;

        try {
            playerWindow = new PlayerWindow(primaryStage);
        } catch (IOException e) {
            e.printStackTrace();
        }

        WindowView.getInstance().LoadWindowView(playerWindow);
    }

    @Override
    public String toString() {
        return "USER_NAME=" + USER_NAME + "," +
                "\nUSER_PSWD=" + USER_PSWD + "," +
                "\nDB_NAME=" + DB_NAME + "," +
                "\nDB_PORT=" + DB_PORT;
    }
}