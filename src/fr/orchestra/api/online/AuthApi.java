package fr.orchestra.api.online;

import fr.orchestra.api.interfaces.AuthOperation;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.model.User;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.HashMap;
import java.util.Map;


/**
 * Call API about authentication on loginView
 */
public class AuthApi implements AuthOperation {

    /**
     * POST : check email/pwd --> authentificate user
     **/
    @Override
    public boolean auth(String pseudo, String pwd) {

        Map<String, String> map = new HashMap<>();
        map.put("login", pseudo);
        map.put("password", pwd);

        JSONObject body = new JSONObject(map);

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(AUTH);
        httpPost.setHeader("Content-type", "application/json");

        try {

            StringEntity stringEntity = new StringEntity(body.toString());
            httpPost.setEntity(stringEntity);
            HttpResponse httpResponse = httpClient.execute(httpPost);

            if (httpResponse.getStatusLine().getStatusCode() == 200) {

                String content = EntityUtils.toString(httpResponse.getEntity());
                JSONParser parser = new JSONParser();
                JSONObject jsonRes = (JSONObject) parser.parse(content);


                /* initialise User and send his instance on WindowView for upload and download */
                JSONObject jsonObj = (JSONObject) jsonRes.get("user");
                int idUser = Integer.valueOf(jsonObj.get("id").toString());
                User user = new User(idUser, pseudo, pwd, jsonRes.get("token").toString());
                WindowView.getInstance().setUser(user);
                return true;
            }
            else return false;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
