package fr.orchestra.api.online;


import com.github.openjson.JSONObject;
import fr.orchestra.api.interfaces.UserOperation;
import fr.orchestra.controller.folderview.WindowView;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Call API about User for display author of playlist or change password on loginView
 */
public class UserApi implements UserOperation {

    /**
     * Get user by id
     *
     * @param id id of the user
     * @return The name of the user who have the same id as passed in parameter
     * @throws IOException
     */
    @Override
    public String getUserById(int id) throws IOException {

        StringBuilder res = new StringBuilder();
        String userName = "";
        String inputLine;
        String url = GET_USER_BY_ID + String.valueOf(id);

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        request.setHeader("x-access-token", WindowView.getInstance().getUser().getAccessToken());
        HttpResponse response = client.execute(request);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        while ((inputLine = rd.readLine()) != null)
            res.append(inputLine);
        rd.close();

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            com.github.openjson.JSONObject jsonObj = new JSONObject(res.toString());
            userName = jsonObj.get("login").toString();
        }
        return userName;
    }
}