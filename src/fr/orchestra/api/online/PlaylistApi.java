package fr.orchestra.api.online;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import fr.orchestra.api.interfaces.PlaylistOperations;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.model.Playlist;
import fr.orchestra.model.User;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class PlaylistApi implements PlaylistOperations {

    /**
     * Search on server Track by title, artist and album
     *
     * @param playlist playlist to be create
     * @param user     user who create this playlist
     * @return true if the new playlist has been created, false if not
     * @throws HttpResponseException if the response code do not correspond to 201 (successful created) : HttpStatus.SC_CREATED
     */
    @Override
    public boolean createNewPlaylist(Playlist playlist, User user) throws HttpResponseException, JSONException {

        HttpResponse httpResponse;
        int httpCode = HttpStatus.SC_NO_CONTENT;

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(CREATE_PLAYLIST);
        httpPost.setHeader("x-access-token", user.getAccessToken());

        File file = new File(playlist.getImagePlaylist().getUrl().substring(5));
        FileBody fileBody = new FileBody(file, ContentType.DEFAULT_BINARY);

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();

        builder.addPart("image", fileBody);
        builder.addTextBody("name", playlist.getPlaylistName());
        builder.addTextBody("user_id", Integer.toString(user.getIdUser()));

        HttpEntity entity = builder.build();

        String content;
        JSONObject jsonRes = null;

        try {
            httpPost.setEntity(entity);
            httpResponse = httpClient.execute(httpPost);

            httpCode = httpResponse.getStatusLine().getStatusCode();

            if (httpCode == HttpStatus.SC_CREATED) {
                user.addPlaylist(playlist);
                return true;
            }

            content = EntityUtils.toString(httpResponse.getEntity());
            JSONParser parser = new JSONParser();
            jsonRes = (JSONObject) parser.parse(content);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }

        assert jsonRes != null;
        throw new HttpResponseException(httpCode, jsonRes.get("message").toString());
    }

    @Override
    public JSONObject createNewTrackToPlaylist(int trackId, int playlistId, User user) throws JSONException {

        JSONObject body = new JSONObject();
        body.put("track_id", trackId);

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPut httpPut = new HttpPut(PATH_API + "playlists/" + String.valueOf(playlistId) + "/addTrack");
        httpPut.setHeader("Content-type", "application/json");
        httpPut.setHeader("x-access-token", user.getAccessToken());

        try {
            StringEntity stringEntity = new StringEntity(body.toString());
            httpPut.setEntity(stringEntity);
            HttpResponse httpResponse = httpClient.execute(httpPut);

            String content = EntityUtils.toString(httpResponse.getEntity());
            JSONParser parser = new JSONParser();
            return (JSONObject) parser.parse(content);

        } catch (IOException | ParseException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public JSONObject updateVisibilityPlaylist(String name, String visibility, User user) throws JSONException {

        JSONObject body = new JSONObject();
        body.put("visibilty", visibility);

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPut httpPut = new HttpPut(PATH_API + "playlists/visibilty/" + name);
        httpPut.setHeader("Content-type", "application/json");
        httpPut.setHeader("x-access-token", user.getAccessToken());

        try {
            StringEntity stringEntity = new StringEntity(body.toString());
            httpPut.setEntity(stringEntity);
            HttpResponse httpResponse = httpClient.execute(httpPut);

            String content = EntityUtils.toString(httpResponse.getEntity());
            JSONParser parser = new JSONParser();
            return (JSONObject) parser.parse(content);

        } catch (IOException | ParseException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public JSONObject updatePasswordPlaylist(String name, String password, User user) throws JSONException{

        JSONObject body = new JSONObject();
        body.put("password", password);

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPut httpPut = new HttpPut(PATH_API + "playlists/password/" + name);
        httpPut.setHeader("Content-type", "application/json");
        httpPut.setHeader("x-access-token", user.getAccessToken());

        try {
            StringEntity stringEntity = new StringEntity(body.toString());
            httpPut.setEntity(stringEntity);
            HttpResponse httpResponse = httpClient.execute(httpPut);

            String content = EntityUtils.toString(httpResponse.getEntity());
            JSONParser parser = new JSONParser();
            return (JSONObject) parser.parse(content);

        } catch (IOException | ParseException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    // @TODO : Take care of "visibility" and "visibilty" --> patch this on the api
    @Override
    public JSONObject checkAuthorizationForPlaylist(String playlistName, User user) {

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(PATH_API + "playlists/checkAuthorization/" + playlistName);
        httpGet.setHeader("Content-type", "application/json");
        httpGet.setHeader("x-access-token", user.getAccessToken());

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);

            String content = EntityUtils.toString(httpResponse.getEntity());
            JSONParser parser = new JSONParser();
            return (JSONObject) parser.parse(content);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public JSONObject getPlaylist(Playlist playlist, User user) {

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(ROOT_PLAYLIST + playlist.getId());
        httpGet.setHeader("Content-type", "application/json");
        httpGet.setHeader("x-access-token", user.getAccessToken());

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);

            String content = EntityUtils.toString(httpResponse.getEntity());
            JSONParser parser = new JSONParser();
            return (JSONObject) parser.parse(content);

        } catch (IOException | ParseException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public JSONObject deletePlaylist(Playlist playlist, User user) throws IOException, ParseException, JSONException{

        JSONObject playlistToUpload = getPlaylistByName(playlist.getPlaylistName());


        if (playlistToUpload.get("error").toString().equals("false")) {

            JSONObject playlistJsonObject = new JSONObject((Map) playlistToUpload.get("playlist"));
            int idPlaylist = Integer.parseInt(playlistJsonObject.get("id").toString());

            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpDelete httpDelete = new HttpDelete(DELETE_PLAYLIST + idPlaylist);
            httpDelete.setHeader("Content-type", "application/json");
            httpDelete.setHeader("x-access-token", user.getAccessToken());

            HttpResponse httpResponse = httpClient.execute(httpDelete);

            String content = EntityUtils.toString(httpResponse.getEntity());
            JSONParser parser = new JSONParser();
            return (JSONObject) parser.parse(content);
        } else return playlistToUpload;
    }

    @Override
    public JSONObject getPlaylistByName(String name) throws IOException, ParseException {

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(ROOT_PLAYLIST + "?name=" + name);
        httpGet.setHeader("Content-type", "application/json");
        httpGet.setHeader("x-access-token", WindowView.getInstance().getUser().getAccessToken());

        HttpResponse httpResponse = httpClient.execute(httpGet);

        String content = EntityUtils.toString(httpResponse.getEntity());

        JSONParser parser = new JSONParser();
        return (JSONObject) parser.parse(content);
    }

    @Override
    public byte[] download(Playlist playlist) {

        HttpClient httpClient = HttpClientBuilder.create().build();

        String ext = FilenameUtils.getExtension(playlist.getImagePlaylist().getUrl());

        HttpGet httpGet = new HttpGet(DOWNLOAD_IMAGE_PLAYLIST + playlist.getPlaylistName() + "." + ext);
        httpGet.setHeader("Content-type", "application/json");
        httpGet.setHeader("x-access-token", WindowView.getInstance().getUser().getAccessToken());

        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);

            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                return new byte[0];
            }

            return EntityUtils.toByteArray(httpResponse.getEntity());

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public JSONObject auth(String name, String password, User user) throws JSONException {

        JSONObject body = new JSONObject();
        body.put("name", name);
        body.put("password", password);

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(PATH_API + "playlists/verify");
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setHeader("x-access-token", user.getAccessToken());

        try {
            StringEntity stringEntity = new StringEntity(body.toString());
            httpPost.setEntity(stringEntity);
            HttpResponse httpResponse = httpClient.execute(httpPost);

            String content = EntityUtils.toString(httpResponse.getEntity());
            JSONParser parser = new JSONParser();
            return (JSONObject) parser.parse(content);

        } catch (IOException | ParseException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * Search on server playlist by name
     *
     * @param searchInput
     * @return
     * @throws IOException
     */
    @Override
    public List<Playlist> getPlaylistResearch(String searchInput) throws JSONException, IOException {

        StringBuilder res = new StringBuilder();
        String inputLine;
        List<Playlist> playlistList = new ArrayList<>();
        String url = RESEARCH_BY_PLAYLIST + searchInput;

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        request.setHeader("Content-type", "application/json");
        request.setHeader("x-access-token", WindowView.getInstance().getUser().getAccessToken());
        HttpResponse response = client.execute(request);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        while ((inputLine = rd.readLine()) != null) res.append(inputLine);
        rd.close();

        JSONArray jsonArr = new JSONArray(res.toString());

        /* If no result found send back a null playlistList */
        if (jsonArr.isNull(0)) return playlistList;

        for (int i = 0; i < jsonArr.length(); i++) {
            JSONObject jsonObj = jsonArr.getJSONObject(i);

            Playlist playlist = new Playlist(jsonObj.get("user_id").toString(),
                    jsonObj.get("src_image").toString(),
                    jsonObj.get("name").toString(),
                    true);
            playlist.setId(Integer.valueOf(jsonObj.get("id").toString()));

            playlistList.add(playlist);
        }
        return playlistList;
    }

    @Override
    public void deleteLinkBetweenPlaylistAndTrack(int idPlaylist, int idTrack, User user) throws IOException {

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpDelete httpDelete = new HttpDelete(ROOT_PLAYLIST + idPlaylist + "/delete/" + idTrack);
        httpDelete.setHeader("x-access-token", user.getAccessToken());
        httpClient.execute(httpDelete);
    }
}
