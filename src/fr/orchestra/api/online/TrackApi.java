package fr.orchestra.api.online;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import fr.orchestra.api.interfaces.TrackOperations;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.model.Track;
import fr.orchestra.model.User;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Call API about Track for search method, download & upload audio file
 */
public class TrackApi implements TrackOperations {

    /**
     * Search on server Track by title, artist and album
     *
     * @param track music add to the user lib
     * @param user  user who download the track
     * @return JSONObject : json-object of the DL track
     * @throws RuntimeException if the parse of the content retrieve by http request doesn't work
     */
    @Override
    public JSONObject createNewTracks(Track track, User user) throws RuntimeException {

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(UPLOAD_TRACK);
        httpPost.setHeader("x-access-token", user.getAccessToken());

        File file = new File(track.getFileFolder());
        FileBody fileBody = new FileBody(file, ContentType.DEFAULT_BINARY);

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();

        builder.addPart("file", fileBody);
        builder.addTextBody("title", track.getName());
        builder.addTextBody("duration", Double.toString(track.getTotalTime()));
        builder.addTextBody("genre", track.getGenre());
        builder.addTextBody("artist", track.getArtist());
        builder.addTextBody("user_id", Integer.toString(user.getIdUser()));

        HttpEntity entity = builder.build();

        httpPost.setEntity(entity);
        HttpResponse httpResponse;
        String content = null;
        try {
            httpResponse = httpClient.execute(httpPost);
            content = EntityUtils.toString(httpResponse.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONParser parser = new JSONParser();
        try {
            return (JSONObject) parser.parse(content);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new RuntimeException("Parse exception when invoke TrackApi.createNewTracks(" + track + ", " + user + ")");
        }
    }

    @Override
    public JSONObject getTrack(Track track, User user) {
        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpGet httpGet = new HttpGet(PATH_API + "tracks/" + track.getTrackId());
            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("x-access-token", user.getAccessToken());

            HttpResponse httpResponse = httpClient.execute(httpGet);

            String content = EntityUtils.toString(httpResponse.getEntity());
            JSONParser parser = new JSONParser();
            return (JSONObject) parser.parse(content);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public byte[] download(Track track) {

        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpGet httpGet = new HttpGet(DOWNLOAD_TRACK + track.getSrcTracks() + "/" + track.getTrackId());
            httpGet.setHeader("Content-type", "application/json");
            httpGet.setHeader("x-access-token", WindowView.getInstance().getUser().getAccessToken());

            HttpResponse httpResponse = httpClient.execute(httpGet);

            if (httpResponse.getStatusLine().getStatusCode() != 200)
                return new byte[0];

            return EntityUtils.toByteArray(httpResponse.getEntity());

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Search on server Track by title, artist and album
     *
     * @param researchBy
     * @param searchInput
     * @return
     * @throws IOException
     * @throws JSONException
     */
    @Override
    public List<Track> getTrackResearch(int researchBy, String searchInput) throws IOException, JSONException {

        StringBuilder res = new StringBuilder();
        URL url = null;
        List<Track> trackList = new ArrayList<>();

        switch (researchBy) {
            case 1:
                url = new URL(RESEARCH_BY_TITLE + searchInput);
                break;
            case 2:
                url = new URL(RESEARCH_BY_ARTIST + searchInput);
                break;
            case 3:
                url = new URL(RESEARCH_BY_ALBUM + searchInput);
                break;
        }

        assert url != null;
        URLConnection yc = url.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null)
            res.append(inputLine);
        in.close();

        JSONArray jsonArr = new JSONArray(res.toString());

        /* si aucun résultat on renvoie la trackList null */
        if (jsonArr.isNull(0)) return trackList;

        for (int i = 0; i < jsonArr.length(); i++) {
            JSONObject jsonObj = jsonArr.getJSONObject(i);
            trackList.add(new Track(Integer.valueOf(jsonObj.get("id").toString()),
                    Double.valueOf(jsonObj.get("duration").toString()),
                    "", "",
                    jsonObj.get("title").toString(),
                    jsonObj.get("artist").toString(),
                    jsonObj.get("album").toString(),
                    jsonObj.get("genre").toString()));
        }
        return trackList;
    }

    @Override
    public JSONObject getTrackBySrcTracks(String src_tracks) throws IOException, ParseException {

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(ROOT_TRACK + "/getall/?src_tracks=" + src_tracks);
        httpGet.setHeader("Content-type", "application/json");
        httpGet.setHeader("x-access-token", WindowView.getInstance().getUser().getAccessToken());

        HttpResponse httpResponse = httpClient.execute(httpGet);
        String content = EntityUtils.toString(httpResponse.getEntity());
        JSONParser parser = new JSONParser();
        return (JSONObject) parser.parse(content);
    }
}
