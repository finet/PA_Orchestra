package fr.orchestra.api.offline;


import org.json.JSONException;
import org.json.JSONObject;
import fr.orchestra.api.interfaces.PlaylistOperations;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.model.Playlist;
import fr.orchestra.model.User;
import org.apache.http.client.HttpResponseException;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class PlaylistLocal implements PlaylistOperations {

    private WindowView wdv = WindowView.getInstance();

    /**
     * Search localy Track by title, artist and album
     *
     * @param playlist playlist to be create
     * @param user     user who create this playlist
     * @return true if the new playlist has been created, false if not
     * @throws HttpResponseException if the response code do not correspond to 201 (successful created) : HttpStatus.SC_CREATED
     */
    @Override
    public boolean createNewPlaylist(Playlist playlist, User user) throws HttpResponseException {

        if(user == wdv.getUser()) {
            user.addPlaylist(playlist);
            return true;
        }

        return false;
    }

    @Override
    public JSONObject createNewTrackToPlaylist(int trackId, int playlistId, User user) throws JSONException {

        JSONObject jsonAddLinkTrackPlaylist = new JSONObject();
        jsonAddLinkTrackPlaylist.put("error", "false");

        return jsonAddLinkTrackPlaylist;
    }

    @Override
    public JSONObject updateVisibilityPlaylist(String name, String visibility, User user) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("error", "true");
        jsonObject.put("message", "Impossible server hors ligne");

        return jsonObject;
    }

    @Override
    public JSONObject updatePasswordPlaylist(String name, String password, User user) {

        return null;
    }

    @Override
    public JSONObject checkAuthorizationForPlaylist(String playlistName, User user) throws JSONException {
        JSONObject jsonAuthorization = new JSONObject();

        User currentUser = wdv.getUser();

        System.out.println("USER=" + currentUser);
        System.out.println("USER_PLAYLIST=" + user);
        System.out.println("HASPLAYLIST=" + currentUser.havePlaylist(playlistName));

        if(user == currentUser && user.havePlaylist(playlistName)) {
            jsonAuthorization.put("error", "false");
            jsonAuthorization.put("message", "own playlist");

            JSONObject jsonPlaylist = new JSONObject();
            jsonPlaylist.put("visibility", "0");

            jsonAuthorization.put("playlist", jsonPlaylist);
        }
        else jsonAuthorization.put("error", "true");

        return jsonAuthorization;
    }

    //@TODO : complete this function
    @Override
    public JSONObject getPlaylist(Playlist playlist, User user) throws JSONException {

        JSONObject jsonObject = new JSONObject();

        if(user == wdv.getUser()) {

            if(user.havePlaylist(playlist)) {
                jsonObject.put("visibility", 1);
                jsonObject.put("password", user.getPassword());
            }
            else {
                jsonObject.put("visibilty", 0);
            }
        }
        return jsonObject;
    }

    @Override
    public JSONObject deletePlaylist(Playlist playlist, User user) throws IOException, ParseException, JSONException {

        JSONObject jsonObject = new JSONObject();

        if(user == wdv.getUser() && user.havePlaylist(playlist)) {
            jsonObject.put("error", "false");
        }
        else jsonObject.put("error", "true");

        return jsonObject;
    }

    @Override
    public JSONObject getPlaylistByName(String playlistName) throws IOException, JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("playlist", wdv.getUser().getPlaylist(playlistName));

        return jsonObject;
    }

    @Override
    public byte[] download(Playlist playlist) {

        throw new IllegalCallerException("Mode OFFLINE, it's not event legal to call this function");
    }

    @Override
    public JSONObject auth(String name, String password, User user) {

        return null;
    }

    /**
     * Search on server playlist by name
     *
     * @param searchInput
     * @return
     * @throws IOException
     */
    @Override
    public List<Playlist> getPlaylistResearch(String searchInput) throws IOException {

        return new ArrayList<>();
    }

    @Override
    public void deleteLinkBetweenPlaylistAndTrack(int idPlaylist, int idTrack, User user) throws IOException { }
}
