package fr.orchestra.api.offline;

import fr.orchestra.api.interfaces.AuthOperation;
import fr.orchestra.controller.folderview.WindowView;
import fr.orchestra.model.User;

public class AuthLocal implements AuthOperation {

    @Override
    public boolean auth(String pseudo, String pwd) {

        User user = new User(-1, pseudo, pwd, "");
        WindowView.getInstance().setUser(user);
        return true;
    }
}