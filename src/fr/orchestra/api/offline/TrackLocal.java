package fr.orchestra.api.offline;

import fr.orchestra.api.interfaces.TrackOperations;
import fr.orchestra.model.Track;
import fr.orchestra.model.User;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class TrackLocal implements TrackOperations {
    @Override
    public JSONObject createNewTracks(Track track, User user) throws RuntimeException {

        throw new RuntimeException("The application shale not use this function in OFFLINE mode");
    }

    @Override
    public JSONObject getTrack(Track track, User user) {
        return null;
    }

    @Override
    public byte[] download(Track track) {

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        ObjectOutputStream objectStream;

        try {
            objectStream = new ObjectOutputStream(byteStream);
            objectStream.writeObject(track);
        } catch (IOException e) {
            e.printStackTrace();
            return new byte[0];
        }

        return byteStream.toByteArray();
    }

    @Override
    public List<Track> getTrackResearch(int researchBy, String searchInput) throws IOException, JSONException {

        return new ArrayList<>();
    }

    @Override
    public JSONObject getTrackBySrcTracks(String src_tracks) throws IOException, ParseException {
        return null;
    }
}
