package fr.orchestra.api.interfaces;


import org.json.JSONObject;
import fr.orchestra.model.Track;
import fr.orchestra.model.User;
import org.json.JSONException;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.List;


public interface TrackOperations extends ConstApi {

    JSONObject createNewTracks(Track track, User user) throws RuntimeException;

    JSONObject getTrack(Track track, User user);


    byte[] download(Track track);

    List<Track> getTrackResearch(int researchBy, String searchInput) throws IOException, JSONException;

    JSONObject getTrackBySrcTracks(String src_tracks) throws IOException, ParseException;

}
