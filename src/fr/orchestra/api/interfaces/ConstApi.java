package fr.orchestra.api.interfaces;


/**
 * This interface define all routes to the Orchestra api.
 */
public interface ConstApi {

    /**
     * path of Api & WebSite
     **/
    // String PATH_API = "http://ns378195.ip-5-196-95.eu:8081/";
    String PATH_API = "http://localhost:8081/";
    // String PATH_WEB = "http://ns378195.ip-5-196-95.eu:4201/";
    String PATH_WEB = "http://localhost:4200/";

    /** URL ON WEB **/

    /**
     * path for subscribe on webSite
     **/
    String SUBSCRIBE = PATH_WEB + "/auth/signup";

    /**
     * path for reset password on webSite
     **/
    String RESET_PWD = PATH_WEB + "/auth/reset-pwd";

    /**
     * path for manage an Account on webSite
     **/
    String MY_ACCOUNT = PATH_WEB + "/myAccount";


    /**************************************************************/
    /** URL ON API **/
    /**************************************************************/

    /**
     * Auth
     **/
    String AUTH = PATH_API + "authentificate";

    /**
     * logout
     **/
    String LOGOUT = PATH_API + "logout";


    /***************************/
    /********   USER   *********/
    /***************************/
    String GET_USER_BY_ID = PATH_API + "users/";

    /***************************/
    /********** TRACK **********/
    /***************************/

    /* get track */
    String ROOT_TRACK = PATH_API + "tracks/";

    /**
     * getAll
     **/
    String GETALL_TRACK = PATH_API + "tracks";

    /* upload */
    String UPLOAD_TRACK = PATH_API + "tracks/upload";

    /**
     * Research by title
     **/
    String RESEARCH_BY_TITLE = PATH_API + "tracks/researchTitle/";

    /**
     * Research by title
     **/
    String RESEARCH_BY_ARTIST = PATH_API + "tracks/researchArtist/";

    /**
     * Research by title
     **/
    String RESEARCH_BY_ALBUM = PATH_API + "tracks/researchAlbum/";

    /**
     * Donwload track
     **/
    String DOWNLOAD_TRACK = PATH_API + "tracks/download/";


    /***************************/
    /******** PLAYLIST *********/
    /***************************/

    /* upload */
    String CREATE_PLAYLIST = PATH_API + "playlists/create/upload";

    /**
     * Research by title
     **/
    String RESEARCH_BY_PLAYLIST = PATH_API + "playlists/research/";

    /* get playlist */
    String ROOT_PLAYLIST = PATH_API + "playlists/";

    /**
     * Donwload image playlist
     **/
    String DOWNLOAD_IMAGE_PLAYLIST = PATH_API + "playlists/downloadImage/";

    /**
     * Delete playlist
     **/
    String DELETE_PLAYLIST = PATH_API + "playlists/delete/";
}
