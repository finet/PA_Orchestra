package fr.orchestra.api.interfaces;


public interface AuthOperation extends ConstApi {

    boolean auth(String pseudo, String pwd);
}
