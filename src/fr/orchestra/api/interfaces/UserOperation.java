package fr.orchestra.api.interfaces;


import java.io.IOException;

public interface UserOperation extends ConstApi {

    String getUserById(int id) throws IOException;
}
