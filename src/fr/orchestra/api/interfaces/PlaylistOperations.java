package fr.orchestra.api.interfaces;


import org.json.JSONException;
import org.json.JSONObject;
import fr.orchestra.model.Playlist;
import fr.orchestra.model.User;
import org.apache.http.client.HttpResponseException;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.List;


public interface PlaylistOperations extends ConstApi {

    boolean createNewPlaylist(Playlist playlist, User user) throws HttpResponseException, JSONException;

    JSONObject createNewTrackToPlaylist(int trackId, int playlistId, User user) throws JSONException;

    JSONObject updateVisibilityPlaylist(String name, String visibility, User user) throws JSONException;

    JSONObject updatePasswordPlaylist(String name, String password, User user) throws JSONException;

    JSONObject checkAuthorizationForPlaylist(String playlistName, User user) throws JSONException;

    JSONObject getPlaylist(Playlist playlist, User user) throws JSONException;

    JSONObject deletePlaylist(Playlist playlist, User user) throws IOException, ParseException, JSONException;

    JSONObject getPlaylistByName(String name) throws IOException, ParseException, JSONException;

    byte[] download(Playlist playlist);

    JSONObject auth(String name, String password, User user) throws JSONException;

    List<Playlist> getPlaylistResearch(String searchInput) throws IOException, JSONException;

    void deleteLinkBetweenPlaylistAndTrack(int idPlaylist, int idTrack, User user) throws IOException;
}
