package fr.orchestra.api;

import fr.orchestra.api.offline.AuthLocal;
import fr.orchestra.api.offline.PlaylistLocal;
import fr.orchestra.api.offline.TrackLocal;
import fr.orchestra.api.offline.UserLocal;
import fr.orchestra.api.online.AuthApi;
import fr.orchestra.api.online.PlaylistApi;
import fr.orchestra.api.online.TrackApi;
import fr.orchestra.api.online.UserApi;
import fr.orchestra.api.interfaces.AuthOperation;
import fr.orchestra.api.interfaces.PlaylistOperations;
import fr.orchestra.api.interfaces.TrackOperations;
import fr.orchestra.api.interfaces.UserOperation;

import javax.management.InstanceAlreadyExistsException;

public class UserServices {

    private static AuthOperation authOp;
    private static PlaylistOperations playlistOps;
    private static TrackOperations trackOps;
    private static UserOperation userOp;

    public enum Mode {
        ONLINE,
        OFFLINE,
        UNKNOWN
    }

    private static UserServices INSTANCE = null;

    private static Mode connectionStatus = Mode.UNKNOWN;
    public static Mode getConnectionStatus() {

        return connectionStatus;
    }

    public static synchronized void createInstance(Mode mode) throws InstanceAlreadyExistsException {
        if(INSTANCE == null) INSTANCE = new UserServices(mode);
        else throw new InstanceAlreadyExistsException("Connection status already done --> " + connectionStatus);
    }

    private UserServices(Mode mode) {

        connectionStatus = mode;
        switch (mode) {
            case ONLINE :
                System.out.println("ONLINE");
                authOp = new AuthApi();
                playlistOps = new PlaylistApi();
                trackOps = new TrackApi();
                userOp = new UserApi();

            case OFFLINE :
                System.out.println("OFFLINE");
                authOp = new AuthLocal();
                playlistOps = new PlaylistLocal();
                trackOps = new TrackLocal();
                userOp = new UserLocal();
        }
    }

    public static AuthOperation getAuthOp() {
        return authOp;
    }

    public static PlaylistOperations getPlaylistOps() {
        return playlistOps;
    }

    public static TrackOperations getTrackOps() {
        return trackOps;
    }

    public static UserOperation getUserOp() {
        return userOp;
    }
}
